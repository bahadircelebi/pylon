<?php

class PylonMysql
{
    private static $instance = null;
    public static function getInstance()
    {
        if (!self::$instance)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }
    private function __clone(){}
    // [/Singleton]

    public $cmd = null;

    private function __construct()
    {
        global $Config;

        $this->cmd = new MysqliDb($Config["system"]["mysql_host"], $Config["system"]["mysql_user"], $Config["system"]["mysql_pass"], $Config["system"]["mysql_database"]);

    }

    //
    // crud operations go here.
    //
}


