<?php
/**
 * Created by PhpStorm.
 * User: guadin
 * Date: 01.07.2014
 * Time: 01:09
 */

abstract class BaseModel
{

    protected $db;
    protected $cache;
    protected $config;
    protected $caption;

    public function __construct()
    {

        global $Config;
        global $Caption;

        $this->config = $Config;
        $this->caption = $Caption;

        if ($this->config["system"]["use_mysql"] == true) {

            $this->db = Application::initDB();
        }

        if ($this->config["system"]["use_cache"] == true) {

            $this->cache = Application::initCache();
            $this->cache = $this->cache->cache;
        }

    }

}