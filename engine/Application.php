<?php

/**
 *  Created by Bahadır Çelebi
 *
 */

class Application
{
    private $config;
    private $cache = null;
    private $db = null;
    private $module;
    private $router;


    function __construct() {

        global $Config;
        global $Router;


        $this->config = $Config;
        $this->router = $Router;

        $this->ConfigureApplication();
    }

//  Run Pylon Framework and Route
    public function Run()
    {
        
        try {
            $_SESSION["_token"] = $this->config["system"]["csr_protection"];

            $key = isset($_COOKIE["user_tmp_key"]) ? $_COOKIE["user_tmp_key"] : md5(date("Ymdhis").rand(1,10000));

            if(!isset($_COOKIE["user_tmp_key"])){
                setcookie("user_tmp_key", $key);
            }

            if(!isset($_SESSION["view_column"])){
                setcookie("view_column", "box");
            }
            // if any router exit call it ...
            $this->RunRoutuer();

            $urlData = isset($_SERVER["REQUEST_URI"]) ? explode("/", $_SERVER["REQUEST_URI"]) : null;
            array_shift($urlData);

            $cnt = count($urlData);
            $param["module"] = "default";

            if(isset($_SERVER["REQUEST_URI"])){
                if($_SERVER["REQUEST_URI"]== "/"){
                    $cnt = 0;
                }

            }

            switch ($cnt){

                # Home Page
                case 0 :
                    $this->module = "default";
                    $controller = "IndexController";
                    $action = "Index";
                    $param["parameter"] = null;


                break;

                # Module Index Page
                case 1 :

                    $this->module = $urlData[0];
                    $param["module"] = $urlData[0];
                    $controller = "IndexController";
                    $action = "Index";
                    $param["parameter"] = null;

                    break;

                # Module Index Page
                case 2 :

                    $Controller  = ucfirst(strtolower($urlData[1]));
                    $Action  = "Index";

                    $this->module = $urlData[0];
                    $param["module"] = $urlData[0];
                    $controller = $Controller . "Controller";
                    $action = $Action;
                    $param["parameter"] = null;

                    break;

                # Module Index Page
                case 3 :

                    $Controller  = ucfirst(strtolower($urlData[1]));
                    $Action  = ucfirst(strtolower($urlData[2]));

                    $this->module = $urlData[0];
                    $param["module"] = $urlData[0];
                    $controller = $Controller . "Controller";
                    $action = $Action;
                    $param["parameter"] = null;

                    break;

                default :


                    $allParam = array_slice($urlData, 2);

                    $Controller  = ucfirst(strtolower($urlData[1]));
                    $Action  = ucfirst(strtolower($urlData[2]));

                    $this->module = $urlData[0];
                    $param["module"] = $urlData[0];
                    $controller = $Controller . "Controller";
                    $action = $Action;
                    $param["parameter"] = $allParam;

                break;
            }

            if(strstr($param["module"], "?lang")){
                $param["module"] = "default";
            }

            $ClassName = $param["module"] . "space" . "\\" . $controller;
            $app = new $ClassName;


            if((method_exists($app, $action))){

                $app->$action($param);
            }


        } catch (Exception $e) {

            echo $e->getMessage();

        }
    }

    private function RunRoutuer(){

        if($this->router != null){

            $Controller = $this->router["controller"]."Controller";
            $Action = $this->router["action"];
            $Param = $this->router["param"];
            $Module = $this->router["module"];

            $ClassName = $Module . "space" . "\\" . $Controller;

            $app = new $ClassName;

            if((method_exists($app, $Action))){
                $app->$Action($Param);
                exit;
            }
        }
    }

    public static function RenderView($view, $module, $data = null, $return = false)
    {

        if($return == false){
            require( __DIR__ . "/../modules/$module/view/$view.tpl.php");
        }else{

            ob_start();
            require( __DIR__ . "/../modules/$module/view/$view.tpl.php");
            $text = ob_get_contents();
            ob_end_clean();

            return $text;

        }
    }

    public static function RenderLayout($layout, $view, $module, $data = null)
    {
        $data["PYLON_VIEW"] = $view != NULL ? Application::renderView($view, $module, $data, true) : null;

        return Application::renderView("template/$layout",$module, $data);
    }

   private function ConfigureApplication(){


    if($this->config["system"]["use_mysql"] == true){

        $this->db =
          new MysqliDb(
            $this->config["system"]["mysql_host"],
            $this->config["system"]["mysql_user"],
            $this->config["system"]["mysql_pass"],
            $this->config["system"]["mysql_database"]
        );
    }

    if($this->config["system"]["use_cache"] == true){

        switch ($this->config["system"]["cache_driver"]){

            case "file" :
                $this->cache = new Cache();
                break;

            case "redis" :

                $client = new \Redis();
                $client->connect('127.0.0.1', 6379);

                $this->cache = new RedisCachePool($client);

                break;

        }
    }

   }

    public static function initDB(){
        return PylonMysql::getInstance();
    }

    public static function initCache(){

        global $config;

        if($config["cache"]["status"] == true){
            if($config["system"]["cache_driver"] == "redis"){
                return PylonRedis::getInstance();
            }
        }else{
            return null;
        }
    }

}