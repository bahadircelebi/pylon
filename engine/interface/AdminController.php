<?php
/**
 * Created by PhpStorm.
 * User: guadin
 * Date: 06.04.2014
 * Time: 23:00
 */

interface AdminController {

    public function Index($param=null);
    public function NotFound();

    public function AddForm();
    public function EditForm($id);

    public function Save();
    public function Update();
    public function Delete($id);

    public function ShowList();

}