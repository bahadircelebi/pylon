<?php

/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 14/07/16
 * Time: 11:34
 */

interface AdminManager {

    public function Add($param=array());
    public function Get($id);
    public function Delete($id);
    public function Edit($id);
    public function Update();
    public function Check($field, $value);
    public function GetList();

}