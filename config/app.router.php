<?php
/**
 * Created by webmobilyazilim.com
 * User: Bahadır Çelebi
 * Date: 18/08/2017
 * Time: 19:56
 */

global $Router;

$klein = new \Klein\Klein();

$klein->with("/admin", function() use ($klein) {

    $klein->respond('GET', '/index', function ($request) {
        AdminRouteController("default", "Admin", "Index", $request, true);
    });

    $klein->respond('POST', '/index', function ($request) {
        AdminRouteController("default", "Admin", "Index", $request, true);
    });

    $klein->respond('GET', '/member', function ($request) {
        AdminRouteController("default", "Admin", "Member", $request, true);
    });

    $klein->respond('POST', '/member-authority', function ($request) {
        AdminRouteController("default", "Admin", "MemberAuthority", $request, true);
    });

    $klein->respond('GET', '/publication', function ($request) {
        AdminRouteController("default", "Admin", "Publication", $request, true);
    });

    $klein->respond('GET', '/publication-detail/[i:id]', function ($request) {
        AdminRouteController("default", "Admin", "PublicationDetail", $request->id, true);
    });

    $klein->respond('GET', '/member-detail/[i:id]', function ($request) {
        AdminRouteController("default", "Admin", "MemberDetail", $request->id, true);
    });

    $klein->respond('GET', '/user', function ($request) {
        AdminRouteController("default", "Admin", "User", $request, true);
    });

    $klein->respond('GET', '/user-detail/[i:id]', function ($request) {
        AdminRouteController("default", "Admin", "UserDetail", $request->id, true);
    });

    $klein->respond('POST', '/user-detail/[i:id]', function ($request) {
        AdminRouteController("default", "Admin", "UserDetail", $request->id, true);
    });

    $klein->respond('GET', '/category', function ($request) {
        AdminRouteController("default", "Admin", "Category", $request, true);
    });

    $klein->respond('POST', '/category', function ($request) {
        AdminRouteController("default", "Admin", "CategorySave", $request, true);
    });

    $klein->respond('GET', '/update-category/[i:id]', function ($request) {
        AdminRouteController("default", "Admin", "CategoryUpdate", $request->id, true);
    });

    $klein->respond('POST', '/update-category/[i:id]', function ($request) {
        AdminRouteController("default", "Admin", "CategoryUpdate", $request->id, true);
    });

    $klein->respond('POST', '/category-delete', function ($request) {
        AdminRouteController("default", "Admin", "CategoryDelete", $request, true);
    });

    $klein->respond('POST', '/sub-category-ajax', function ($request) {
        AdminRouteController("default", "Admin", "SubCategoryAjax", $request, true);
    });

    $klein->respond('GET', '/sub-category/[i:id]', function ($request) {
        AdminRouteController("default", "Admin", "SubCategory", $request->id, true);
    });

    $klein->respond('POST', '/sub-category/[i:id]', function ($request) {
        AdminRouteController("default", "Admin", "SubCategorySave", $request->id, true);
    });

    $klein->respond('POST', '/option-ajax', function ($request) {
        AdminRouteController("default", "Admin", "OptionAjax", $request, true);
    });

    $klein->respond('GET', '/option', function ($request) {
        AdminRouteController("default", "Admin", "Option", $request, true);
    });

    $klein->respond('POST', '/option', function ($request) {
        AdminRouteController("default", "Admin", "OptionSave", $request, true);
    });

    $klein->respond('GET', '/update-option/[i:id]', function ($request) {
        AdminRouteController("default", "Admin", "OptionUpdate", $request->id, true);
    });

    $klein->respond('POST', '/update-option/[i:id]', function ($request) {
        AdminRouteController("default", "Admin", "OptionUpdate", $request->id, true);
    });

	$klein->respond('POST', '/option-delete', function ($request) {
        AdminRouteController("default", "Admin", "OptionDelete", $request, true);
    });

    $klein->respond('GET', '/option-detail/[i:id]', function ($request) {
        AdminRouteController("default", "Admin", "OptionDetail", $request->id, true);
    });

    $klein->respond('POST', '/option-detail/[i:id]', function ($request) {
        AdminRouteController("default", "Admin", "OptionDetailSave", $request->id, true);
    });

	$klein->respond('GET', '/update-option-detail/[i:id]', function ($request) {
        AdminRouteController("default", "Admin", "OptionDetailUpdate", $request->id, true);
    });

    $klein->respond('POST', '/update-option-detail/[i:id]', function ($request) {
        AdminRouteController("default", "Admin", "OptionDetailUpdate", $request->id, true);
    });

	$klein->respond('POST', '/option-detail-delete', function ($request) {
        AdminRouteController("default", "Admin", "OptionDetailDelete", $request, true);
    });

    $klein->respond('GET', '/company', function ($request) {
        AdminRouteController("default", "Admin", "Company", $request, true);
    });

    $klein->respond('GET', '/homepage', function ($request) {
        AdminRouteController("default", "Admin", "Homepage", $request, true);
    });

    $klein->respond('POST', '/featured', function ($request) {
        AdminRouteController("default", "Admin", "FeaturedSave", $request, true);
    });

    $klein->respond('POST', '/homepage', function ($request) {
        AdminRouteController("default", "Admin", "HomepageSave", $request, true);
    });

	$klein->respond('POST', '/buygosell', function ($request) {
        AdminRouteController("default", "Admin", "BuygosellSave", $request, true);
    });

    $klein->respond('POST', '/company', function ($request) {
        AdminRouteController("default", "Admin", "CompanySave", $request, true);
    });

    $klein->respond('GET', '/login', function ($request) {
        AdminRouteController("default", "Admin", "LoginForm", $request);
    });

    $klein->respond('POST', '/login', function ($request) {
        AdminRouteController("default", "Admin", "Login", $request);
    });

    $klein->respond('POST', '/register', function ($request) {
        AdminRouteController("default", "Admin", "AdminRegister", $request);
    });

    $klein->respond('GET', '/logout', function ($request) {
        AdminRouteController("default", "Admin", "Logout", $request);
    });

    $klein->respond('GET', '/cargo-report', function ($request) {
        AdminRouteController("default", "Admin", "CargoReport", $request, true);
    });

    $klein->respond('GET', '/confirmation-report', function ($request) {
        AdminRouteController("default", "Admin", "ConfirmationReport", $request, true);
    });

    $klein->respond('GET', '/finish-report', function ($request) {
        AdminRouteController("default", "Admin", "FinishReport", $request, true);
    });

    $klein->respond('GET', '/payment-report', function ($request) {
        AdminRouteController("default", "Admin", "PaymentReport", $request, true);
    });

    $klein->respond('POST', '/payment-confirmation', function ($request) {
        AdminRouteController("default", "Admin", "PaymentConfirmation", $request, true);
    });

    $klein->respond('GET', '/mailing', function ($request) {
        AdminRouteController("default", "Admin", "Mailing", $request, true);
    });

    $klein->respond('POST', '/mailing', function ($request) {
        AdminRouteController("default", "Admin", "MailingSave", $request, true);
    });

    $klein->respond('POST', '/send-mailing', function ($request) {
        AdminRouteController("default", "Admin", "MailingSend", $request, true);
    });

    $klein->respond('GET', '/message-detail/[i:id]', function ($request) {
        AdminRouteController("default", "Admin", "MessageDetail", $request->id, true);
    });

    $klein->respond('POST', '/slider-delete', function ($request) {
        AdminRouteController("default", "Admin", "SliderDelete", $request, true);
    });

    $klein->respond('GET', '/big-slider', function ($request) {
        AdminRouteController("default", "Admin", "Homepage", $request, true);
    });

    $klein->respond('POST', '/big-slider', function ($request) {
        AdminRouteController("default", "Admin", "BigSliderSave", $request, true);
    });

    $klein->respond('POST', '/publication-update/[i:id]', function ($request) {
        AdminRouteController("default", "Admin", "UpdatePublication", $request->id, true);
    });

	 $klein->respond('POST', '/publication-photo-add', function ($request) {
        AdminRouteController("default", "Admin", "AddPublicationPhoto", $request, true);
    });

    $klein->respond('POST', '/publication-photo-delete', function ($request) {
        AdminRouteController("default", "Admin", "DeletePublicationPhoto", $request, true);
    });

});

$klein->with("/user", function() use ($klein){

    $klein->respond('GET', '/login', function ($request) {
        RouteController("default", "User", "LoginForm", $request);
    });

    $klein->respond('POST', '/login', function ($request) {
        RouteController("default", "User", "LoginAction", $request);
    });

    $klein->respond('GET', '/logout', function ($request) {
        RouteController("default", "User", "Logout", $request);
    });

    $klein->respond('GET', '/register', function ($request) {
        RouteController("default", "User", "RegisterForm", $request);
    });

    $klein->respond('POST', '/register', function ($request) {
        RouteController("default", "User", "RegisterAction", $request);
    });

    $klein->respond('POST', '/get-state', function ($request) {
        RouteController("default", "User", "GetState", $request);
    });

    $klein->respond('POST', '/get-city', function ($request) {
        RouteController("default", "User", "GetCity", $request);
    });

	$klein->respond('GET', '/recovery', function ($request) {
        RouteController("default", "User", "Recovery", $request);
    });

	$klein->respond('POST', '/recovery', function ($request) {
        RouteController("default", "User", "RecoverySend", $request);
    });

    $klein->respond('GET', '/recoverychange', function ($request) {
        RouteController("default", "User", "RecoveryChange", $request);
    });

    $klein->respond('POST', '/recoverychange', function ($request) {
        RouteController("default", "User", "RecoveryChange", $request);
    });

    $klein->respond('POST', '/column', function ($request) {
        RouteController("default", "User", "UserViewColumn", $request);
    });

    $klein->respond('POST', '/add-message/[i:id]', function ($request) {
        RouteController("default", "User", "AddMessage", $request->id);
    });

    $klein->respond('POST', '/add-complaint/[i:id]', function ($request) {
        RouteController("default", "User", "AddComplaint", $request->id);
    });

    $klein->respond('GET', '/activate', function ($request) {
        RouteController("default", "User", "AccountActivate", $request);
    });

});

$klein->with("/profile", function() use ($klein){

    $klein->respond('POST', '/update', function ($request) {
        RouteController("default", "User", "MyProfileAction", $request, true);
    });

	$klein->respond('GET', '/password-change', function ($request) {
        RouteController("default", "User", "PasswordChangeForm", $request, true);
    });

	$klein->respond('POST', '/password-change', function ($request) {
        RouteController("default", "User", "SavePasswordChange", $request, true);
    });

    $klein->respond('GET', '/bank', function ($request) {
        RouteController("default", "User", "BankForm", $request, true);
    });

    $klein->respond('POST', '/bank', function ($request) {
        RouteController("default", "User", "BankFormAction", $request, true);
    });

    $klein->respond('POST', '/delete-bank', function ($request) {
        RouteController("default", "User", "BankDeleteAction", $request, true);
    });

    $klein->respond('GET', '/address', function ($request) {
        RouteController("default", "User", "AddressForm", $request, true);
    });

    $klein->respond('POST', '/address', function ($request) {
        RouteController("default", "User", "AddressFormAction", $request, true);
    });

	$klein->respond('GET', '/favorite', function ($request) {
        RouteController("default", "User", "GetFavorite", $request, true);
    });

    $klein->respond('POST', '/delete-favorite', function ($request) {
        RouteController("default", "User", "DeleteFavorite", $request, true);
    });

    $klein->respond('GET', '/delete-favorite', function ($request) {
        RouteController("default", "User", "DeleteFavorite", $request, true);
    });

    $klein->respond('GET', '/buy', function ($request) {
        RouteController("default", "User", "GetBuy", $request, true);
    });

    $klein->respond('POST', '/buy', function ($request) {
        RouteController("default", "User", "GetBuy", $request, true);
    });

    $klein->respond('POST', '/order-point', function ($request) {
        RouteController("default", "User", "OrderPoint", $request, true);
    });

    $klein->respond('GET', '/sell', function ($request) {
        RouteController("default", "User", "GetSell", $request, true);
    });

    $klein->respond('POST', '/sell', function ($request) {
        RouteController("default", "User", "GetSell", $request, true);
    });

    $klein->respond('GET', '/message', function ($request) {
        RouteController("default", "User", "GetMessage", $request, true);
    });

	$klein->respond('POST', '/add-messages/[i:id]', function ($request) {
        RouteController("default", "User", "AddMessages", $request->id, true);
    });

    $klein->respond('GET', '/message-detail/[i:id]', function ($request) {
        RouteController("default", "User", "GetMessageDetail", $request->id, true);
    });

    $klein->respond('GET', '/manage', function ($request) {
        RouteController("default", "User", "GetManage", $request, true);
    });

    $klein->respond('GET', '/manage-detail/[i:id]', function ($request) {
        RouteController("default", "User", "GetManageDetail", $request->id, true);
    });

});

$klein->with("/product", function() use ($klein){

    $klein->respond('GET', '/detail/[i:id]', function ($request) {
        RouteController("default", "Product", "Detail", $request->id);
    });

    $klein->respond('POST', '/detail', function ($request) {
        RouteController("default", "Product", "Detail", $request);
    });

    $klein->respond('GET', '/upload', function ($request) {
        RouteController("default", "Product", "Upload", $request, true);
    });

	  $klein->respond('POST', '/upload', function ($request) {
        RouteController("default", "Product", "SendUpload", $request, true);
    });

    $klein->respond('POST', '/upload-product', function ($request) {
        RouteController("default", "Product", "Upload", $request, true);
    });

    $klein->respond('POST', '/sub-category', function ($request) {
        RouteController("default", "Product", "SubCategory", $request, true);
    });

    $klein->respond('POST', '/option', function ($request) {
        RouteController("default", "Product", "Option", $request, true);
    });

   	$klein->respond('GET', '/seller', function ($request) {
        RouteController("default", "Product", "Seller", $request);
    });

	  $klein->respond('GET', '/buyer', function ($request) {
        RouteController("default", "Product", "Buyer", $request);
    });

    $klein->respond('POST', '/add-favorite', function ($request) {
        RouteController("default", "Product", "AddFavorite", $request);
    });

    $klein->respond('GET', '/search/?', function ($request) {
        RouteController("default", "Product", "Search", $request);
    });

    $klein->respond('GET', '/auto-search', function ($request) {
        RouteController("default", "Product", "AutoSearch", $request);
    });

    $klein->respond('GET', '/confirm/[i:id]', function ($request) {
        RouteController("default", "Product", "Confirm", $request->id);
    });

    $klein->respond('POST', '/publication-point', function ($request) {
        RouteController("default", "Product", "PublicationPoint", $request, true);
    });

    $klein->respond('POST', '/publication-update/[i:id]', function ($request) {
        RouteController("default", "Product", "UpdatePublication", $request->id, true);
    });

    $klein->respond('POST', '/publication-photo-add', function ($request) {
        RouteController("default", "Product", "AddPublicationPhoto", $request, true);
    });

    $klein->respond('POST', '/publication-photo-delete', function ($request) {
        RouteController("default", "Product", "DeletePublicationPhoto", $request, true);
    });

});

$klein->with("/basket", function() use ($klein){

    $klein->respond('POST', '/add', function ($request) {
        RouteController("default", "Basket", "AddToBasket", $request);
    });

    $klein->respond('POST', '/delete-item', function ($request) {
        RouteController("default", "Basket", "DeleteFromBasket", $request);
    });

    $klein->respond('GET', '/total-product', function ($request) {
        RouteController("default", "Basket", "TotalProduct", $request);
    });

    $klein->respond('GET', '/shopping-cart', function ($request) {
        RouteController("default", "Basket", "Step1", $request);
    });

    $klein->respond('GET', '/delivery-payment', function ($request) {
        RouteController("default", "Basket", "Step2", $request);
    });

    $klein->respond('GET', '/complete-order/[i:id]', function ($request) {
        RouteController("default", "Basket", "Step3", $request->id);
    });

	$klein->respond('POST', '/address', function ($request) {
        RouteController("default", "Basket", "AddAddress", $request);
    });

    $klein->respond('POST', '/empty-basket', function ($request) {
        RouteController("default", "Basket", "BasketEmpty", $request);
    });

    $klein->respond('POST', '/change-quantity', function ($request) {
        RouteController("default", "Basket", "ChangeQuantity", $request);
    });

    $klein->respond('POST', '/save-order', function ($request) {
        RouteController("default", "Basket", "SaveOrder", $request);
    });

});

$klein->with("/order", function() use ($klein){

    $klein->respond('POST', '/cargo', function ($request) {
        RouteController("default", "User", "Cargo", $request);
    });

});

$klein->respond('GET', '/test/[:name]', function ($request) {
    RouteController("default", "Index", "Test", $request);
});

$klein->respond('GET', '/category', function ($request) {
    RouteController("default", "Category", "Index", $request);
});

$klein->respond('GET', '/best', function ($request) {
    RouteController("default", "Product", "BestPublication", $request);
});

$klein->respond('GET', '/profile', function ($request) {
    RouteController("default", "User", "MyProfile", $request, true);
});

$klein->respond('GET', '/product', function ($request) {
    RouteController("default", "Product", "Index", $request);
});

$klein->respond('GET', '/about', function ($request) {
    RouteController("default", "Index", "About", $request);
});

$klein->respond('GET', '/contact', function ($request) {
    RouteController("default", "Index", "Contact", $request);
});

$klein->respond('GET', '/delivery', function ($request) {
    RouteController("default", "Index", "Delivery", $request);
});

$klein->respond('GET', '/legal-warning', function ($request) {
    RouteController("default", "Index", "LegalWarning", $request);
});

$klein->respond('GET', '/empty', function ($request) {
    RouteController("default", "Index", "EmptyPage", $request);
});




$klein->with("/api/v1", function() use ($klein){

    $klein->respond('POST', '/login', function ($request) {
        RouteController("api", "Index", "Login", $request);
    });

    $klein->respond('POST', '/register', function ($request) {
        RouteController("api", "Index", "Register", $request);
    });


    $klein->respond('POST', '/remember', function ($request) {
        RouteController("api", "Index", "RememberPassword", $request);
    });

    $klein->respond('POST', '/change-pwd', function ($request) {
        RouteController("api", "Index", "ChangePassword", $request);
    });

    $klein->respond('POST', '/getUser', function ($request) {
        RouteController("api", "Index", "GetUser", $request);
    });
  
    $klein->respond('POST', '/updateUser', function ($request) {
        RouteController("api", "Index", "UpdateUser", $request);
    });
  
    $klein->respond('POST', '/updateUserPhoto', function ($request) {
        RouteController("api", "Index", "UpdateUserPhotoBase64", $request);
    });


    $klein->respond('POST', '/country', function ($request) {
        RouteController("api", "Utility", "GetCountryList", $request);
    });

    $klein->respond('POST', '/state', function ($request) {
        RouteController("api", "Utility", "GetStateList", $request);
    });

    $klein->respond('POST', '/city', function ($request) {
        RouteController("api", "Utility", "GetCityList", $request);
    });

    $klein->respond('POST', '/category', function ($request) {
        RouteController("api", "Category", "GetCategoryList", $request);
    });

    $klein->respond('POST', '/detail', function ($request) {
        RouteController("api", "Product", "ProductDetail", $request);
    });

    $klein->respond('POST', '/category_detail', function ($request) {
        RouteController("api", "Category", "Index", $request);
    });

    $klein->respond('GET', '/search', function ($request) {
        RouteController("api", "Product", "Search", $request);
    });

   $klein->respond('GET', '/bestSellersProducts', function ($request) {
        RouteController("api", "Product", "GetBestSellersProduct", $request);
    });

   $klein->respond('GET', '/newArrivalProducts', function ($request) {
        RouteController("api", "Product", "GetNewlyAddedProduct", $request);
    });


    $klein->respond('POST', '/sub-category', function ($request) {
        RouteController("api", "Category", "GetCategoryList", $request);
    });

    $klein->respond('POST', '/buy-publication', function ($request) {
        RouteController("api", "Category", "GetCategoryList", $request);
    });

    $klein->respond('POST', '/addtobasket', function ($request) {
        RouteController("api", "Basket", "AddToBasket", $request);
    });

    $klein->respond('POST', '/mybasket', function ($request) {
        RouteController("api", "Basket", "GetMyBasket", $request);
    });

    $klein->respond('POST', '/delete-basket', function ($request) {
        RouteController("api", "Basket", "DeleteFromBasket", $request);
    });

    $klein->respond('POST', '/update-quantity', function ($request) {
        RouteController("api", "Basket", "UpdateQuantity", $request);
    });

    $klein->respond('POST', '/empty-basket', function ($request) {
        RouteController("api", "Basket", "EmptyBasket", $request);
    });

    $klein->respond('POST', '/order-detail', function ($request) {
        RouteController("api", "Basket", "OrderDetail", $request);
    });
  
    $klein->respond('POST', '/save-order', function ($request) {
        RouteController("api", "Basket", "saveOrder", $request);
    });
  
  
   // Buy Sell
  
  $klein->respond('POST', '/buy-list', function ($request) {
        RouteController("api", "Product", "GetBuy", $request);
    });
   $klein->respond('POST', '/sell-list', function ($request) {
        RouteController("api", "Product", "GetSell", $request);
    });
  
  
  // address
   $klein->respond('POST', '/add-address', function ($request) {
        RouteController("api", "Address", "addAddress", $request);
    });
   $klein->respond('POST', '/edit-address', function ($request) {
        RouteController("api", "Address", "editAddress", $request);
    });
   $klein->respond('POST', '/delete-address', function ($request) {
        RouteController("api", "Address", "deleteAddress", $request);
    });
   $klein->respond('POST', '/my-addresses', function ($request) {
        RouteController("api", "Address", "getAddresses", $request);
    });
  
  // Banks
  
  $klein->respond('POST', '/add-bank', function ($request) {
        RouteController("api", "Bankinfo", "AddBankinfo", $request);
    });
   $klein->respond('POST', '/edit-bank', function ($request) {
        RouteController("api", "Bankinfo", "editBankinfo", $request);
    });
   $klein->respond('POST', '/delete-bank', function ($request) {
        RouteController("api", "Bankinfo", "deleteBank", $request);
    });
   $klein->respond('POST', '/my-banks', function ($request) {
        RouteController("api", "Bankinfo", "getBanks", $request);
    });

  // sell and buy
  $klein->respond('POST', '/addProduct', function ($request) {
        RouteController("api", "Product", "addProduct", $request);
    });
  
});


$klein->dispatch();

function RouteController($module, $controller, $action, $param, $private=false){

    if($private){

        if(!isset($_SESSION["userInfo"]["user_id"])) {
        \Globalf::redirect("user/login");
        }
    }

    global $Router;

    $Router["module"] = $module;
    $Router["controller"] = $controller;
    $Router["action"] = $action;
    $Router["param"] = $param;
}

function AdminRouteController($module, $controller, $action, $param, $private=false){

    if($private){

        if(!isset($_SESSION["adminInfo"]["admin_id"])) {
            \Globalf::redirect("admin/login");
        }
    }

    global $Router;


    $Router["module"] = $module;
    $Router["controller"] = $controller;
    $Router["action"] = $action;
    $Router["param"] = $param;
}