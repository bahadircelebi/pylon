<?php
/**
 * Created by PhpStorm.
 * User: guadin
 * Date: 29.06.2014
 * Time: 15:47
 */

class Globalf {

    public static function p($data){

        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

    public static function redirect($page, $code=null){

        global $config;

        $url = "http://" . $_SERVER["HTTP_HOST"] . "/" . $page;
        if($code == null){
            header("Location: $url");
            exit;
        }else{
            header("Location: $url", $code);
            exit;
        }

    }

    public static function LogToFile($msg, $file=null){

        return "";

        global $config;

        if($file == null && isset($config["CONF_FILE"])){
            $file = $config["CONF_FILE"];
        }

        if(!file_exists($file)){

            $fh = fopen($file, 'w');
            fclose($fh);
            chmod($file,0755);
        }

        $newStr = date("H:i:s") . "  " . $msg . "\n";

        @file_put_contents($file, $newStr, FILE_APPEND);

    }


    static function GetContentByCURL($url, $timeout=10){

        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_HEADER, false);
        curl_setopt($c, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);

        $result = curl_exec($c);
        curl_close($c);


        return $result;

    }

    static function get_web_page( $url )
    {

        $user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

        $options = array(

            CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
            CURLOPT_POST           =>false,        //set to GET
            CURLOPT_USERAGENT      => $user_agent, //set user agent
            CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
            CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        );

        $ch      = curl_init( $url );
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;


        return $content;
    }

    static function GetLangFromIp_ipinfodb($ip){


        $url = "http://ip-api.com/php/$ip";

        $data = Globalf::GetContentByCURL($url);

        $coords = "";

        if($data != ""){


            if(isset($data["lan"]) && $data["lon"]){
                $lat = $data["lat"];
                $lng = $data["lon"];

                $coords = $lat.",".$lng;
            }

        }

        return $coords;
    }

    static function GetInfoFromIP(){
        $gps = Globalf::GetLatLngFromIp($_SERVER["REMOTE_ADDR"]);
        return Globalf::GetFormatedInfoFromGoogle($gps);
    }


    static function GetInfo_ipinfodb($ip){

        return "";

        $url = "http://ip-api.com/php/94.123.202.121/$ip";

        $data =  Globalf::GetContentByCURL($url);

        $dataInfo = null;

        if($data != ""){

            $data = explode(";", $data);

            if(isset($data["country"]) && $data["country"]){

                $dataInfo["postal_code"] = $data["zip"];
                $dataInfo["country"] = $data["country"];
                $dataInfo["countryCodeFor"] = $data["countryCode"];
                $dataInfo["countryCode"] = Globalf::CountrToLangCode($data["countryCode"]);

                $dataInfo["gps"] = $data["lat"].",".$data["lon"];
                $dataInfo["city"] = urlencode($data["city"]);
            }else{
                Globalf::LogToFile("GetInfo_ipinfodb hata", "ipinfodb_HATA");
            }

        }

        return $dataInfo;

    }

    static function GetNumber($str){
        return intval(filter_var($str, FILTER_SANITIZE_NUMBER_INT));
    }

    static function ClearString($text){
        return preg_replace('/\s+/', '', $text);
    }



    static function SetSEOUrl( $str, $options = array() ) {
        // Make sure string is in UTF-8 and strip invalid UTF-8 characters
        $str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());

        $defaults = array(
            'delimiter' => '-',
            'limit' => null,
            'lowercase' => true,
            'replacements' => array(),
            'transliterate' => false,
        );

        // Merge options
        $options = array_merge($defaults, $options);

        $char_map = array(
            // Latin
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
            'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
            'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
            'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
            'ß' => 'ss',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
            'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
            'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
            'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
            'ÿ' => 'y',
            // Latin symbols
            '©' => '(c)',
            // Greek
            'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
            'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
            'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
            'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
            'Ϋ' => 'Y',
            'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
            'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
            'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
            'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
            'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',
            // Turkish
            'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
            'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',
            // Russian
            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
            'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
            'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
            'Я' => 'Ya',
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
            'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
            'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
            'я' => 'ya',
            // Ukrainian
            'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
            'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',
            // Czech
            'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',
            'Ž' => 'Z',
            'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
            'ž' => 'z',
            // Polish
            'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',
            'Ż' => 'Z',
            'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
            'ż' => 'z',
            // Latvian
            'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',
            'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
            'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
            'š' => 's', 'ū' => 'u', 'ž' => 'z'
        );

        // Make custom replacements
        $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

        // Transliterate characters to ASCII
        //if ($options['transliterate']) {
            $str = str_replace(array_keys($char_map), $char_map, $str);
        //}

        // Replace non-alphanumeric characters with our delimiter
        $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

        // Remove duplicate delimiters
        $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

        // Truncate slug to max. characters
        $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

        // Remove delimiter from ends
        $str = trim($str, $options['delimiter']);

        return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
    }

    static function minify_output($buffer)
    {
        $search = array(
            '/\>[^\S ]+/s',
            '/[^\S ]+\</s',
            '/(\s)+/s'
        );
        $replace = array(
            '>',
            '<',
            '\\1'
        );
        if (preg_match("/\<html/i",$buffer) == 1 && preg_match("/\<\/html\>/i",$buffer) == 1) {
            $buffer = preg_replace($search, $replace, $buffer);
        }
        return $buffer;
    }

    static function FormatParam($param){
        $data = explode("-", $param);
        $lang = end($data);
        array_pop($data);
        return array( "lang"=>strtoupper($lang), "key"=>implode("-", $data ));
    }

    static function FormatLangParam($param){

        $data = explode("@@", $param);
        return array( "lang"=>strtolower($data[1]), "data"=>$data[0]);
    }

    static function Direct301($lang){

        header("HTTP/1.1 301 Moved Permanently");
        header("Location: http://sineman.tv/$lang");
        exit;
    }

    static function InstallLanguage($lang){

        global $config;

        $lang = strtoupper($lang);

        if(!in_array($lang, $config["lang"])){

           // Globalf::Direct301("en");
        }

        $lang = in_array($lang, $config["lang"]) ? $lang : "EN";

        $file = __DIR__ . "/../lang/" . $lang . ".php";


        include($file);
    }

    public static function GetSubText($string, $width){

        $your_desired_width = $width;
        $string = mb_substr($string, 0, $your_desired_width+1);

        if (strlen($string) > $your_desired_width)
        {
            $string = wordwrap($string, $your_desired_width);
            $i = strpos($string, "\n");
            if ($i) {
                $string = mb_substr($string, 0, $i)." ..";
            }
        }

        $string = str_replace("&nbsp;", " ", $string);

        return $string;

    }


    static function my_mb_ucfirst($str) {
        $fc = mb_strtoupper(mb_substr($str, 0, 1));
        return $fc.mb_substr($str, 1);
    }


    static function NotFound(){

        /*
         *  Bu dosyada anasayfaya yönlendirme kesinlikle yapılmamalı
         */

        header('Location: /404');

        exit;

    }

    static function FormatPhone($phone)
    {
        $phone = preg_replace("/[^0-9]/", "", $phone);

        if(strlen($phone) == 7)
            return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
        elseif(strlen($phone) == 10)
            return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
        else
            return $phone;
    }

    static function encrypt_decrypt($action, $string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = 'This is my secret key';
        $secret_iv = 'This is my secret iv';

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        }
        else if( $action == 'decrypt' ){
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }


    static function removeWhitespace($buffer)
    {
        return preg_replace('/\s+/', ' ', $buffer);
    }

    static function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    static function Encrypt($action, $string)
    {
        global $config;

        $output = false;
        $key    = hash("sha256", $config["system"]["secureKey"]);
        $iv     = substr(hash("sha256", "bitex"), 0, 16);

        if ($action == "encrypt")
        {
            $output = openssl_encrypt($string, "AES-256-CBC", $key, 0, $iv);
            $output = base64_encode($output);
        }
        else if($action == "decrypt")
        {
            $output = base64_decode($string);
            $output = openssl_decrypt($output, "AES-256-CBC", $key, 0, $iv);
        }
        return $output;
    }
	
	public static function SendMail($to, $subject, $message){

        $headers = "From: " . "info@webmobilyazilim.com" . "\r\n";
        $headers .= "Reply-To: ". "info@webmobilyazilim.com". "\r\n";
        $headers .= "CC: info@webmobilyazilim.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

        //mail($to, $message, $subject,  $headers);
        //return "";

        $mail = new \PHPMailer\PHPMailer\PHPMailer();
        $mail->isSMTP();                                      // Set mailer to use SMTP

        $mail->SMTPAuth = true; // There was a syntax error here (SMPTAuth)
        $mail->SMTPSecure = 'ssl';
        $mail->Host = "smtp-pulse.com";
        $mail->Mailer = "smtp";
        $mail->Port = 465;
        $mail->Username = "bahadir@webmobilyazilim.com";
        $mail->Password = "Aj7HkKPNk8c";
        $mail->CharSet = 'UTF-8';

        //$mail->SMTPDebug = 2;                                // Enable verbose debug output

        $mail->setFrom('info@enisan.com.tr', "Buygosell");
		
		$address = explode(',',$to);
		//var_dump($address);
        $mail->addAddress($to, '');     // Add a recipient

        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name         // Set email format to HTML

        $mail->Subject = $subject;
        $mail->Body    = $message;


        $mail->isHTML(true);

        if(!$mail->send()) {
             return 'mail_not_sent';
            // echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            return 'mail_sent';
        }

    }

}