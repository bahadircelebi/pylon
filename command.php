<?php
/**
    PYLON CREATER FILE
*/

if (isset($argv[1]) && $argv[2]) {

    $Command = $argv[1];


    switch($Command){

        case "module" :
                    $module = $argv[2];
                    RecursiveCopy( __DIR__ . "/engine/template/core-module", __DIR__."/modules/$module");
            break;

        case "controller" :

            $module = $argv[2];

            $ControllerClassName = ucfirst(strtolower($argv[3]))."Controller";

            $Class = <<<STR
<?php            
Class {$ControllerClassName} implements FrontController {

    public function Index(\$param=null){
    
    }

    public function NotFound(){
    
    }

}
?>

STR;

            $dest = __DIR__ . "/modules/$module/controller/{$ControllerClassName}".".php";
            file_put_contents($dest, $Class);

            break;

        case "acontroller" :

            $module = $argv[2];

            $ControllerClassName = ucfirst(strtolower($argv[3]))."Controller";

            $Class = <<<STR
<?php            
Class {$ControllerClassName} implements AdminController {

    public function Index(\$param=null){
    }
    
    public function NotFound(){
    }

    public function AddForm(){}
    public function EditForm(\$id){}

    public function Save(){}
    public function Update(){}
    public function Delete(\$id){}

    public function ShowList(){}

}
?>
STR;

            $dest = __DIR__ . "/modules/$module/controller/{$ControllerClassName}".".php";
            file_put_contents($dest, $Class);

            break;
    }
}

function RecursiveCopy($source, $dest){

    mkdir($dest, 0755);

    foreach (
        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS),
            \RecursiveIteratorIterator::SELF_FIRST) as $item
    ) {
        if ($item->isDir()) {
            mkdir($dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
        } else {
            copy($item, $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
        }
    }
}
