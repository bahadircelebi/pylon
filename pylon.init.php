<?php
@require_once __DIR__ . "/vendor/autoload.php";
@require_once __DIR__ . "/exlib/vendor/autoload.php";

require_once __DIR__ . "/exlib/Globalf.php";
require_once __DIR__ . "/config/app.config.php";
require_once __DIR__ . "/config/app.router.php";
require_once __DIR__ . "/engine/Application.php";


if(isset($_GET["lang"])){

    if($_GET["lang"] == "en" ||  $_GET["lang"] == "tr" ){

        $_SESSION["lang"] = $_GET["lang"];
        require_once __DIR__ . "/config/lang/" . $_GET["lang"] . ".php";

    }else{
        if($Config["system"]["default_lang"] != ""){
            require_once __DIR__ . "/config/lang/".$Config["system"]["default_lang"].".php";
        }
    }
}
else if(isset($_SESSION["lang"])){

    require_once __DIR__ . "/config/lang/" . $_SESSION["lang"] . ".php";

}else{
    if($Config["system"]["default_lang"] != ""){
        require_once __DIR__ . "/config/lang/".$Config["system"]["default_lang"].".php";
    }

}

function __autoload($class_name) {

    $module = "*";

    if(strstr($class_name,'\\')){
        $tmpData = explode("\\", $class_name);
        $module = $tmpData[0];
        $module = str_replace("space", "", $module);
        $class_name = $tmpData[1];
    }

    LoadClass($class_name, __DIR__ ."/engine/interface");
    LoadClass($class_name, __DIR__ ."/engine/model");

    # Modules ...

    $directories = array_filter(glob(__DIR__  . '/modules/'.$module), 'is_dir');

    if(count($directories)>0){

        foreach ($directories as $directory){

            if(LoadClass($class_name, $directory."/model")){
                break;
            }

            if(LoadClass($class_name, $directory."/controller")){
                break;
            }
        }

    }

}

spl_autoload_register("__autoload");

function LoadClass($class_name, $dir){

    $ds = DIRECTORY_SEPARATOR;

    // replace namespace separator with directory separator (prolly not required)
    $className = str_replace('\\', $ds, $class_name);

    // get full name of file containing the required class
    $file = "{$dir}{$ds}{$className}.php";

    // get file if it is readable
    if (is_readable($file)){
        require_once $file;
        return true;

    }

}