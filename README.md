# Pylon Framework README

1- Install Framework
    sudo composer.phar require  klein/klein:"dev-master" guadin/pylon:"dev-master"

2- Install Libraries

go pylon/vendor/exlib
    sudo composer.phar update

3- Install Admin Static Files
    bower install adminbsb-materialdesign

4- Create Module

    sudo command.php module test

5- Create Normal Controller

    sudo command.php controller "module_name" "controller_name"

6- Create Admin Controller

    sudo command.php acontroller "module_name" "controller_name"


7- Create index.php with these codes

    require_once "vendor/guadin/pylon/pylon.init.php";

    $Application = new Application();
    $Application->Run();



Licence
    The Pylon framework is open-sourced software licensed under the MIT license.
