<?php
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 09/08/2017
 * Time: 05:27
 */
namespace apispace;

Class CategoryController extends \BaseModel implements \FrontController {


    public function Index($param = null)
    {
        $UserManager = new \defaultspace\UserManager();
        $CategoryManager = new \defaultspace\CategoryManager();
        $PublicationManager = new \defaultspace\PublicationManager();

        $categoryId = $_POST["cat_id"];

        if(isset($categoryId)):

            $data['categoryInfo'] = $CategoryManager->GetCategory($categoryId);
            $data['category'] = $CategoryManager->GetCategoryPublicationList();
            $data['subCategory'] = $CategoryManager->GetSubCategory($categoryId);
            $data['categoryOption'] = $CategoryManager->GetCategoryOptionList($categoryId);
            $data["publicationList"] = $CategoryManager->GetCategoryPublicationList($categoryId);
        else:
            $data['publicationList'] = $PublicationManager->GetHomePagePublicationList();
        endif;
        $data['country'] = $UserManager->GetCountry();
        $data['categoryMain'] = $CategoryManager->GetCategoryInfo();


        echo json_encode($data);
    }

    public function GetPublicationList(){
        
    }

    public function GetCategoryList(){

        $CatMngr = new \defaultspace\CategoryManager();

        $data = $CatMngr->GetCategoryInfo();

        echo json_encode($data);

    }

    public function GetSubCategoryList(){

        if(isset($_POST["category_id"])){

            $CatMngr = new \defaultspace\CategoryManager();

            $data = $CatMngr->GetSubCategory($_POST["category_id"]);
        }else{
            $data = "missing";
        }

        echo json_encode($data);

    }

    public function NotFound()
    {
        // TODO: Implement NotFound() method.
    }

}