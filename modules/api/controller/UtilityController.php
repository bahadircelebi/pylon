<?php
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 09/08/2017
 * Time: 05:27
 */
namespace apispace;

Class UtilityController extends \BaseModel  {

    public function GetCountryList(){

        $Mngr = new UserManager();

        $data = $Mngr->GetCountry();

        echo json_encode($data);
    }

    public function GetStateList()
    {

        if (isset($_POST["country_id"])) {

            $Mngr = new UserManager();

            $data = $Mngr->GetState($_POST["country_id"]);
        }else{
            $data = "missing";
        }

        echo json_encode($data);
    }


    public function GetCityList(){

        if(isset($_POST["state_id"])){

            $Mngr = new UserManager();

            $data =  $Mngr->GetCity($_POST["state_id"]);

        }else{
            $data = "missing";
        }

        echo json_encode($data);



    }

    public function NotFound()
    {
        // TODO: Implement NotFound() method.
    }

}