<?php
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 09/08/2017
 * Time: 05:27
 */
namespace apispace;

/*
status: true,
message: “”,
data: []
 */

Class IndexController extends \BaseModel  {

    public function Login(){

        $data = array();

        if(isset($_POST["user_mail"]) && $_POST["password"]){

            $Mngr = new UserManager();

            if($User = $Mngr->Login($_POST["user_mail"], $_POST["password"])){
                $data["data"] = $User;
                $data["status"] = true;
                $data["message"] = "";
                $data["message_tr"] = "";
            } else {
                $data["data"] = "";
                $data["status"] = false;
                $data["message"] = "There is no such user registered.";
                $data["message_tr"] = "Kayıtlı böyle bir kullanıcı yok.";
            }
        }else{
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "Not Exist";
            $data["message_tr"] = "Mevcut değil";
        }

        echo json_encode($data);

    }

    public function Register(){

        $data = null;

        if( isset($_POST["first_name"]) &&
            isset($_POST["last_name"]) &&
            isset($_POST["user_mail"]) &&
            isset($_POST["password"]) &&
            isset($_POST["repassword"]) &&
            isset($_POST["country_id"]) &&
            isset($_POST["state_id"]) &&
            isset($_POST["city_id"])
        )
        {
            $Mngr = new UserManager();

            $msg = $Mngr->Register(
                     $_POST["first_name"],
                     $_POST["last_name"],
                     $_POST["user_mail"],
                     $_POST["password"],
                     $_POST["repassword"],
                     $_POST["country_id"],
                     $_POST["state_id"],
                     $_POST["city_id"]
            );

            if(intval($msg)>0){
                $data["data"]["user_id"] = intval($msg);
                $data["status"] = true;
                $data["message"] = "";
                $data["message_tr"] = "";
            }else{
                $data["data"]= "";
                $data["status"] = false;
                $data["message"] = $msg;
                $data["message_tr"] = $msg;
            }



        }else{
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "Please enter the required fields!";
            $data["message_tr"] = "Lütfen gerekli alanları giriniz!";

        }

        echo json_encode($data);


    }

    public function RememberPassword(){

        if(isset($_POST["member_mail"])){

            $Mngr = new UserManager();

          if($Mngr->SendPassword()){
              $data["data"] = "";
              $data["status"] = true;
              $data["message"] = "The password has been sent to your e-mail address!";
              $data["message_tr"] = "Şifre mail adresinize gönderilmiştir!";
          } else {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "Email not found";
            $data["message_tr"] = "Email bulunamadı";
          }

        }else{
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "Please enter the required fields!";
            $data["message_tr"] = "Lütfen gerekli alanları giriniz!";
        }

        echo json_encode($data);
    }

    public function ChangePassword(){

        $Mngr = new UserManager();

        $msg = $Mngr->ChangePassword();

        $data["data"] = "";
        $data["status"] = false;
        $data["message"] = $msg;

        echo json_encode($data);


    }

  
   public function GetUser(){

        if(isset($_POST["user_id"])){

          $Mngr = new UserManager();

          if($User = $Mngr->GetUser($_POST["user_id"])){
                $data["data"] = $User;
                $data["status"] = true;
                $data["message"] = "";
                $data["message_tr"] = "";
            } else {
                $data["data"] = "";
                $data["status"] = false;
                $data["message"] = "There is no such user registered.";
                $data["message_tr"] = "Kayıtlı böyle bir kullanıcı yok.";
            }

        } else {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "user_id is required";
            $data["message_tr"] = "user_id gerekli";
        }

        echo json_encode($data);
    }
  
   public function UpdateUser() {
     $userId = $_POST["user_id"];
        if(isset($userId)){
          $Mngr = new UserManager();
          if($Mngr->UpdateUser($userId)){
                $data["data"] = $Mngr->GetUser($userId);
                $data["status"] = true;
                $data["message"] = "";
                $data["message_tr"] = "";
            } else {
                $data["data"] = "";
                $data["status"] = false;
                $data["message"] = "There is no such user registered.";
                $data["message_tr"] = "Kayıtlı böyle bir kullanıcı yok.";
            }
        } else {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "user_id is required";
            $data["message_tr"] = "user_id gerekli";
        }
        echo json_encode($data);
    }
  
   public function UpdateUserPhotoBase64() {
     $userId = $_POST["user_id"];
     $photoBase64 = $_POST["photo"];
     $encodedImgString = explode(',', $photoBase64, 2)[1];

     
     if(!isset($userId)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "user_id is required";
            $data["message_tr"] = "user_id gerekli";
     } else if(!isset($photoBase64)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "photo is required";
            $data["message_tr"] = "photo gerekli";
     } else if (base64_encode(base64_decode($encodedImgString, true)) != $encodedImgString) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "photo need base64";
            $data["message_tr"] = "fotoğraf base64 gerek";
     } else {   
       $Mngr = new UserManager();
       $user = $Mngr->GetUser($userId);
       
       $filename = md5($userId .$i. time());
       preg_match("/^data:image\/(.*);base64/i",$photoBase64, $match);
       $extension = strtolower($match[1]);
       
			 $decoded=base64_decode($encodedImgString);
			 $folder = $this->config["system"]["user_path"] . $userId;

       $fullPath = $folder . "/" . $filename . ".$extension";
       $imgPath = $this->config["system"]["user_path"].$user['user_photo'];

       if($user['user_photo']) {
         unlink($imgPath);
       }

       @mkdir($folder, 0755, true);
	 		 chmod($folder, 0777);
			 $res = file_put_contents($fullPath, $decoded);
		   $photoPath = $userId."/".$filename . ".$extension";

       if($Mngr->UpdateUserPhoto($userId, $photoPath)) {
                $data["data"] = $Mngr->GetUser($userId);
                $data["status"] = true;
                $data["message"] = "Photo Updated successfully";
                $data["message_tr"] = "Fotoğraf başarıyla güncellendi";
            } else {
                $data["data"] = "";
                $data["status"] = false;
                $data["message"] = "Photo not updated";
                $data["message_tr"] = "Fotoğraf güncellenmedi";
            }
        }
     echo json_encode($data);

    }
  
   public function UpdateUserPhoto() {
     $userId = $_POST["user_id"];
     if(!isset($userId)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "user_id is required";
            $data["message_tr"] = "user_id gerekli";
     } else {        
       $Mngr = new UserManager();
       $user = $Mngr->GetUser($userId);
       
       $filename = md5($userId .$i. time());
        $extension = strtolower(end(explode("/", $_FILES['photo']['type'])));
			 $folder = $this->config["system"]["user_path"] . $userId;

       $fullPath = $folder . "/" . $filename . ".$extension";
       $imgPath = $this->config["system"]["user_path"].$user['user_photo'];

       if($user['user_photo']) {
         unlink($imgPath);
       }

       @mkdir($folder, 0755, true);
	 		 chmod($folder, 0777);
       
  			$res = move_uploaded_file($_FILES['photo']['tmp_name'], $fullPath);
		   $photoPath = $userId."/".$filename . ".$extension";

           if($res && $Mngr->UpdateUserPhoto($userId, $photoPath)) {
                $data["data"] = $Mngr->GetUser($userId);
                $data["status"] = true;
                $data["message"] = "Photo Updated successfully";
                $data["message_tr"] = "Fotoğraf başarıyla güncellendi";
            } else {
                $data["data"] = "";
                $data["status"] = false;
                $data["message"] = "Photo not updated";
                $data["message_tr"] = "Fotoğraf güncellenmedi";
            }
     }
     
     echo json_encode($data);

    }

    public function NotFound()
    {
        // TODO: Implement NotFound() method.
    }

}