<?php
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 09/08/2017
 * Time: 05:27
 */
namespace apispace;

Class BasketController extends \BaseModel {


    public function AddToBasket(){

        $BasketMngr = new BasketManager();

        $data["data"] = "";
        if($BasketMngr->AddToBasket()){
            $data["status"] = true;
            $data["message"] = "Added to your basket";
            $data["message_tr"] = "Sepetinize eklendi";
        } else{
          $data["status"] = false;
            $data["message"] = "Something went wrong";
            $data["message_tr"] = "Bir şeyler yanlış gitti";
        }

        echo json_encode($data);

    }

    public function DeleteFromBasket(){

        $BasketMngr = new BasketManager();

        $data["data"] = "";
        if($BasketMngr->DeleteFromBasket()){
          $data["status"] = true;
          $data["message"] = "Product is deleted";
          $data["message_tr"] = "Ürün silindi";
        } else{
          $data["status"] = false;
          $data["message"] = "Something went wrong";
          $data["message_tr"] = "Bir şeyler yanlış gitti";
        }

        echo json_encode($data);

    }

    public function GetMyBasket(){

        $BasketMngr = new BasketManager();
        $data["publication_list"] = $BasketMngr->GetBasketPublictionList();

        echo json_encode($data);

    }

    public function UpdateQuantity(){

        $BasketMngr = new BasketManager();
        $BasketMngr->ChangeQuantity();

          $data["status"] = true;
          $data["message"] = "Added to your basket";
          $data["message_tr"] = "Sepetinize eklendi";
          echo json_encode($data);
    }

    public function EmptyBasket(){

        $BasketMngr = new BasketManager();
        $BasketMngr->BasketEmpty();

          $data["status"] = true;
          $data["message"] = "Your basket products are deleted";
          $data["message_tr"] = "Sepet ürünleriniz silinmiş";
        echo json_encode($data);

    }

    public function OrderDetail(){
        $orderId = $_POST["order_id"];
        $BasketMngr = new BasketManager();
        $data = $BasketMngr->GetCompleteOrderDetail($orderId);

        echo json_encode($data);

    }

    public function saveOrder() {
        $userId = $_POST['user_id'];
        $userDeliveryaddress = $_POST['user_delivery_address'];
        $paymentType = $_POST['payment_type'];

        if(!isset($userId)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "user_id is required";
            $data["message_tr"] = "user_id gerekli";
        }   else if(!isset($userDeliveryaddress)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "user_delivery_address is required";
            $data["message_tr"] = "user_delivery_address gerekli";
        }  else if(!isset($paymentType)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "payment_type is required";
            $data["message_tr"] = "payment_type gerekli";
        } else {
            $BasketMngr = new BasketManager();
            $UserMngr = new UserManager();
            $IndexManager = new IndexManager();

            $seller_id = 0;
            $totalPrice = 0;
            $Orderotal = array();
            $products = $BasketMngr->GetBasketProducts($userId);

            foreach($products as $product) {

                $productSellerId = $product['user_basket_seller_id'];
                $orderPublicationId = $product['user_basket_publication_id'];
                $bsketAmnt = $product['user_basket_amount'];
                $publicationPrice = $product['publication_price'];

                if($seller_id != $productSellerId) {
                    $insert = array();
                    $insert['order_buyer_id'] = $userId;
                    $insert['order_seller_id'] = $productSellerId;
                    $insert['order_code'] = "PN1234567890";
                    $insert['order_address_id'] = $userDeliveryaddress;
                    $insert['order_invoice_address_id'] = $userDeliveryaddress;
                    $insert['order_payment_type'] = $paymentType;
                    $insert['order_status'] = "P";
                    $insert['order_cargo_no'] = "0";

                    $lastId = $BasketMngr->SaveOrder($insert);


                    if ($lastId) {

                        $insert = array();
                        $insert['order_movement_type'] = "P";
                        $insert['order_movement_order_id'] = $lastId;
                        $BasketMngr->SaveMovement($insert);

                        $insert = array();
                        $insert['pending_payment_user_id'] = $userId;
                        $insert['pending_payment_order_id'] = $lastId;
                        $insert['pending_payment_price'] = 0;
                        $insert['pending_payment_currency'] = "USD";
                        $insert['pending_payment_accept_user'] = 0;
                        $insert['pending_payment_accept_ip'] = "";
                        $insert['pending_payment_status'] = "P";
                        $BasketMngr->SaveOrderPayment($insert);


                        $totalPrice = 0;

                        $buyer = $UserMngr->GetUser($productSellerId);
                        $HTML = <<<HTML
                        <p>Yeni bir siparişiniz var.</p>
                        <p>Profilinizde Satışlarım bölümünden inceleyebilirsiniz.</p>
                        <p>Buygosell'i tercih ettiğiniz için teşekkür ederiz.</p>                    
HTML;

                        $IndexManager->Mail($buyer['user_mail'], "Yeni Sipariş Bildirimi", $HTML);
                    }

                }

                if ($lastId) {

                    $insert = array();
                    $insert['order_detail_order_id'] = $lastId;
                    $insert['order_detail_user_id'] = $userId;
                    $insert['order_detail_seller_id'] = $productSellerId;
                    $insert['order_detail_publication_id'] = $orderPublicationId;
                    $insert['order_detail_publication_amount'] = $bsketAmnt;
                    $insert['order_detail_publication_price'] = $publicationPrice;
                    $BasketMngr->saveOrderDetails($insert);


                    $totalPrice += ($bsketAmnt*$publicationPrice); 
                    $Orderotal[$lastId] =+ $totalPrice;

                    $update = array();
                    $update['order_total_rate'] = $Orderotal[$lastId];
                    $BasketMngr->UpdateOrder($lastId, $update);

                    $update = array();
                    $update['pending_payment_price'] = $Orderotal[$lastId];
                    $BasketMngr->UpdateOrderPayment($lastId, $update);


                    $seller_id = $productSellerId;
                }

            }

            if ($lastId) {

                $BasketMngr->deleteBasket($userId);

                $latestOrder = $BasketMngr->GetCompleteOrder($lastId);
                $latestOrder["publications"] = $BasketMngr->GetCompleteOrderDetail($lastId);

                $data["data"] = $latestOrder;
                $data["status"] = true;
                $data["message"] = "Order saved";
                $data["message_tr"] = "Sipariş kaydedildi";
            } else {
                $data["data"] = array();
                $data["status"] = false;
                $data["message"] = "Order not saved";
                $data["message_tr"] = "Sipariş kaydedilmedi";
            }
        }
        echo json_encode($data);
    }



    public function NotFound()
    {
        // TODO: Implement NotFound() method.
    }

}