<?php
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 09/08/2017
 * Time: 05:27
 */
namespace apispace;

Class ProductController extends \BaseModel implements \FrontController {


    public function Index($param = null)
    {
        $data = array();

        $BasketManager = new \BasketManager();
        $UserManager = new \UserManager();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();


        \Application::RenderLayout("default", "product/product", "default", $data);
    }

    public function Detail($id)
    {

        $data = array();

        $UserManager = new \UserManager();
        $PublicationManager = new \PublicationManager();

        $data['publication'] = $PublicationManager->GetPublicationDetail($id);
        $data['publicationOption'] = $PublicationManager->GetPublicationOption($id);
        $data['publication_photo'] = $PublicationManager->GetPublicationPhoto($id);
        $data['from_country'] = $UserManager->GetCountrySelect($data['publication']['publication_from_country']);
        $data['to_country'] = $UserManager->GetCountrySelect($data['publication']['publication_to_country']);

        \Application::RenderLayout("default", "product/product.detail", "default", $data);

    }

    public function Confirm($id)
    {
        $data = array();

        $UserManager = new \UserManager();
        $PublicationManager = new \PublicationManager();

        $PublicationManager->PublicationConfirm($id);
        $data['publication'] = $PublicationManager->GetPublicationDetail($id);
        $data['publicationOption'] = $PublicationManager->GetPublicationOption($id);
        $data['publication_photo'] = $PublicationManager->GetPublicationPhoto($id);
        $data['from_country'] = $UserManager->GetCountrySelect($data['publication']['publication_from_country']);
        $data['to_country'] = $UserManager->GetCountrySelect($data['publication']['publication_to_country']);

        \Application::RenderLayout("default", "product/product.detail", "default", $data);
    }
	
	public function Seller()
	{
        $data = array();
		$PublicationManager = new \PublicationManager();

		$data["publicationList"] = $PublicationManager->GetLastSalePublicationList();

	    echo json_encode($data);

	}
	
	public function Buyer()
	{
        $data = array();
		$PublicationManager = new \PublicationManager();
		$data["publicationList"] = $PublicationManager->GetLastSalePublicationList();

        echo json_encode($data);


	}

    public function ProductDetail(){

        $id = $_POST['id'];

        $UserManager = new \defaultspace\UserManager();
        $PublicationManager = new \defaultspace\PublicationManager();

        $data['publication'] = $PublicationManager->GetPublicationDetail($id);
        $data['publicationOption'] = $PublicationManager->GetPublicationOption($id);
        $data['publication_photo'] = $PublicationManager->GetPublicationPhoto($id);
        $data['from_country'] = $UserManager->GetCountrySelect($data['publication']['publication_from_country']);
        $data['to_country'] = $UserManager->GetCountrySelect($data['publication']['publication_to_country']);
        $data['publication_point'] = $PublicationManager->GetPublicationPoint($id);
        $data['pid'] = $id;

        echo json_encode($data);
    }

  
  public function GetBestSellersProduct(){
        $data = array("message" => "");
        $PublicationManager = new \defaultspace\PublicationManager();

        $products = $PublicationManager->GetPopulerPublicationList();
        if ($products) {
            $data["data"] = $products;
            $data["status"] = true;
          $data["message"] = "";
             $data["message_tr"] = "";
        } else {
            $data["data"] = array();
            $data["status"] = false;
          $data["message"] = "";
             $data["message_tr"] = "";
        }
        echo json_encode($data);
    }
  
   public function GetNewlyAddedProduct(){
        $data = array("message" => "");
        $PublicationManager = new \defaultspace\PublicationManager();

        $products = $PublicationManager->GetNewPublicationList();
        if ($products) {
            $data["data"] = $products;
            $data["status"] = true;
          $data["message"] = "";
             $data["message_tr"] = "";
        } else {
            $data["data"] = array();
            $data["status"] = false;
            $data["message"] = "";
             $data["message_tr"] = "";
       }
        echo json_encode($data);
    }

    public function Search() {
        $data = array();
        $ApiManager = new \apispace\ApiManager();

        $data['search'] = $ApiManager->PublicationSearch();

        echo json_encode($data);
    }

  
  public function GetSell() {
         $userId = $_POST["user_id"];

          if(!isset($userId)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "user_id is required";
            $data["message_tr"] = "user_id gerekli";
          } else {
            $Mngr = new UserManager();
            $sellList = $Mngr->GetSell($userId);
                  if ($sellList) {
                    $data["data"] = $sellList;
                    $data["status"] = true;
                    $data["message"] = "";
                    $data["message_tr"] = "";
                  } else {
                    $data["data"] = array();
                    $data["status"] = false;
                    $data["message"] = "";
                    $data["message_tr"] = "";
                  }
         }
        echo json_encode($data);
  }
  
  
  public function GetBuy() {
         $userId = $_POST["user_id"];

          if(!isset($userId)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "user_id is required";
            $data["message_tr"] = "user_id gerekli";
          } else {
            $Mngr = new UserManager();
            $buyList = $Mngr->GetBuy($userId);
            $orders = array();
            foreach ($buyList as $order) {
                $order['publications'] = $Mngr->GetCompleteOrderDetail($order['order_id']);
                array_push($orders, $order);
            }

                  if ($buyList) {
                    $data["data"] = $orders;
                    $data["status"] = true;
                    $data["message"] = "";
                    $data["message_tr"] = "";
                  } else {
                    $data["data"] = array();
                    $data["status"] = false;
                    $data["message"] = "";
                    $data["message_tr"] = "";
                  }
         }
        echo json_encode($data);
  }
    public function NotFound()
    {
        // TODO: Implement NotFound() method.
    }
  
  public function addProduct() {
    $publicationTitle = $_POST['product_title'];
		$publicationDescription = $_POST['product_info'];
		$publicationCategoryId = $_POST['category'];
		$publicationUserId = $_POST["user_id"];
		$publicationFromCountry = $_POST['from_country'];
		$publicationToCountry = $_POST['to_country'];
		$publicationStartDate = $_POST['date'];
		$publicationEndDate = $_POST['date'];
		$publicationPrice = $_POST['product_price'];
		$publicationCurrency = $_POST['product_currency'];
		$publicationPiece = $_POST['product_piece'];
		$publicationPieceType = $_POST['product_piece_type'];
    
     if(!isset($publicationTitle)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "product_title is required";
            $data["message_tr"] = "product_title gerekli";
     }  else if(!isset($publicationDescription)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "product_info is required";
            $data["message_tr"] = "product_info gerekli";
     } else if(!isset($publicationCategoryId)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "category is required";
            $data["message_tr"] = "category gerekli";
     } else if(!isset($publicationUserId)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "user_id is required";
            $data["message_tr"] = "user_id gerekli";
     } else if(!isset($publicationFromCountry)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "from_country is required";
            $data["message_tr"] = "from_country gerekli";
     } else if(!isset($publicationToCountry)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "to_country is required";
            $data["message_tr"] = "to_country gerekli";
     } else if(!isset($publicationStartDate)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "date is required";
            $data["message_tr"] = "date gerekli";
     } else if(!isset($publicationPrice)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "product_price is required";
            $data["message_tr"] = "product_price gerekli";
     } else if(!isset($publicationCurrency)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "product_currency is required";
            $data["message_tr"] = "product_currency gerekli";
     } else if(!isset($publicationPiece)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "product_piece is required";
            $data["message_tr"] = "product_piece gerekli";
     } else if(!isset($publicationPieceType)) {
            $data["data"] = "";
            $data["status"] = false;
            $data["message"] = "product_piece_type gerekli";
            $data["message_tr"] = "product_piece_type gerekli";
     } else {
       $insert = array();
		$insert['publication_title'] = $publicationTitle;
		$insert['publication_description'] = $publicationDescription;
		$insert['publication_category_id'] = $publicationCategoryId;
		$insert['publication_user_id'] = $publicationUserId;
		$insert['publication_from_country'] = $publicationFromCountry;
		$insert['publication_to_country'] = $publicationToCountry;
		$insert['publication_start_date'] = $publicationStartDate;
		$insert['publication_end_date'] = $publicationEndDate;
		$insert['publication_price'] = $publicationPrice;
		$insert['publication_currency'] = $publicationCurrency;
		$insert['publication_piece'] = $publicationPiece;
		$insert['publication_piece_type'] = $publicationPieceType;
       
        $ApiManager = new \apispace\ApiManager();
      $insertedId = $ApiManager->addPublication($insert);   
       if($insertedId) {
         $count = count($_FILES['file']['name']);

         
        for($i=0; $i < $count; $i++){
            if(!empty($_FILES['file']['name'][$i])){

                $filename = md5($publicationUserId .$i. time());
              
                $array = explode('.', $_FILES['file']['name'][$i]);
                $extension = strtolower(end($array));

                $folder = $this->config["system"]["upload_path"] . $insertedId;

                @mkdir($folder, 0755, true);
	 		          chmod($folder, 0777);

                $fullPath = $folder . "/" . $filename . ".$extension";
                move_uploaded_file($_FILES['file']['tmp_name'][$i], $fullPath);

                $bigImage = $fullPath;
                $imgResizer = new \Eventviva\ImageResize($fullPath);
                $imgResizer->resizeFit($bigImage, 1000, 1435);
                $newPath = $folder . "/" . $filename . "-640.$extension";
                $this->PhotoResizeWidth($fullPath, $newPath, 640); //640px genişlik ürün resmi
                $newPath = $folder . "/" . $filename . "-150.$extension";
                $this->PhotoResizeWidth($fullPath, $newPath, 150); //150x150px küçük resim
                $this->PhotoCrop($newPath, 150, 150);

                $insert = array();
                $insert['publication_id'] = $insertedId;
                $insert['publication_photo_path'] = $insertedId."/".$filename;
                $insert['publication_photo_extension'] = $extension;
                $pubPhotoInsertedId = $ApiManager->addPublicationPhotos($insert);   
            }
        }
       }
       
       
       if ($count > 0) {
         if($pubPhotoInsertedId) {
           $data["data"] = "";
           $data["status"] = true;
          $data["message"] = "Successfully added";
          $data["message_tr"] = "Başarıyla Eklendi";
         } else {
           $data["data"] = "";
           $data["status"] = false;
           $data["message"] = "Not added";
           $data["message_tr"] = "Eklenmez";
         }
       } else {
         if($insertedId) {
           $data["data"] = "";
           $data["status"] = true;
          $data["message"] = "Successfully added";
          $data["message_tr"] = "Başarıyla Eklendi";
         } else {
           $data["data"] = $insert;
           $data["status"] = false;
           $data["message"] = "Product Not added";
           $data["message_tr"] = "Ürün eklenmedi";
         }
       }
     }
    echo json_encode($data);
  }
  

   public function PhotoResize($fullPath, $width, $height)
    {
        $imgResizer = new \Eventviva\ImageResize($fullPath);
        $imgResizer->resizeToBestFit($width, $height);
        $imgResizer->save($fullPath);
    }

    public function PhotoResizeWidth($fullPath, $newPath, $width)
    {
        $imgResizer = new \Eventviva\ImageResize($fullPath);
        $imgResizer->resizeToWidth($width);
        $imgResizer->save($newPath);
    }

    public function PhotoResizeHeight($fullPath, $newPath, $height)
    {
        $imgResizer = new \Eventviva\ImageResize($fullPath);
        $imgResizer->resizeToHeight($height);
        $imgResizer->save($newPath);
    }

    public function PhotoCrop($fullPath, $width, $height)
    {
        $imgResizer = new \Eventviva\ImageResize($fullPath);
        $imgResizer->crop($width, $height);
        $imgResizer->save($fullPath);
    }
}