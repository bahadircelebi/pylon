<?php
namespace apispace;
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 15.01.2018
 * Time: 15:42
 */

class UserManager extends \BaseModel
{


    /**
     * UserManager constructor.
     */


    public function GetUserInfo(){

        /*$User = new User($UserId);
        return $User;*/
        
        $this->db->cmd->where("user_id", $_SESSION['userInfo']['user_id']);
        return $this->db->cmd->getOne("user");
    }
  
   public function GetUser($userId){
        $this->db->cmd->where("user_id", $userId);
        return $this->db->cmd->getOne("user");
    }

    public function Register($first_name, $last_name, $user_email, $password, $repassword, $country_id, $state_id, $city_id){

        if($password != $repassword)
            return "password_not_match";

        if(strlen($password) < 6 || strlen($repassword) < 6)
            return "passwod_lower_6";

        if (!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
            return "wrong_mail";
        }

        $tmpkey = \Globalf::Encrypt("encrypt", $user_email . time());

        $insert = array();

        $insert["user_first_name"] = $first_name;
        $insert["user_last_name"] = $last_name;
        $insert["user_mail"] = $user_email;
        $insert["user_password"] = \Globalf::Encrypt("encrypt", $password);
        $insert["user_country"] = $country_id;
        $insert["user_state"] = $state_id;
        $insert["user_city"] = $city_id;
        $insert["user_register_date"] = date("Y-m-d H:i:s");
        $insert["user_tmp_key"] = $tmpkey;
        $insert["user_status"] = "A";


        $this->db->cmd->where("user_mail", $user_email);

        $data = $this->db->cmd->getOne("user", "user_id");

        if(isset($data["user_id"])){
            return "exist";
        }

        /*
        $link = $this->config["system"]["base_url"] . "user/activate?key=$tmpkey";

        $HTML = <<<HTML
                    <h1>Üyelik Aktivasyonu</h1>
                    Lünten aşağıdaki linke tıklayarak üyeliğinizi aktif ediniz. 
                    <a href="{$link}">Hesabımı Aktif Et</a>                    
HTML;
        */

        if($id = $this->db->cmd->insert("user", $insert)){

           // \Globalf::SendMail($user_email, "Üyelik Aktivasyonu", $HTML);

            return $id;
        }

        return "Error";

    }

    public function AccountActivate()
    {
        $update = array();
        $update["user_status"] = "A";

        $this->db->cmd->where("user_tmp_key", $_GET["key"]);
        if($this->db->cmd->update("user", $update)){
            return "success";
        }
    }

    public function Login($user_email, $password){

		$pass = \Globalf::Encrypt("encrypt", $password);

    $this->db->cmd->where("user_status", "A");
		$this->db->cmd->where("user_mail", $user_email);
        $this->db->cmd->where("user_password", $pass);

        $user = $this->db->cmd->getOne("user");

        if(isset($user["user_id"])){
            return $user;
        }

        return false;

    }

    //Kullanıcı bilgileri güncelleme
    public function UpdateProfile(){

		if(!empty($_FILES['file']['name'])){

			$filename = md5($_SESSION["userInfo"]["user_id"] .$i. time());

			$array = explode('.', $_FILES['file']['name']);
			$extension = strtolower(end($array));

			$folder = $this->config["system"]["user_path"] . $_SESSION["userInfo"]["user_id"];

			@mkdir($folder, 0755, true);
			chmod($folder, 0755);

			$fullPath = $folder . "/" . $filename . ".$extension";
			move_uploaded_file($_FILES['file']['tmp_name'], $fullPath);

			$bigImage = $fullPath;
			//$this->PhotoResizeWidth($bigImage, $bigImage, 1000);
			$PublicationManager = new \PublicationManager();
			$PublicationManager->PhotoCrop($bigImage, 250, 250);
		
		}

        $update = array();

        $update['user_first_name'] = $_POST['user_first_name'];
        $update['user_last_name'] = $_POST['user_last_name'];
        $update['username'] = $_POST['username'];
        $update['user_birthday'] = $_POST['user_birthday'];
        $update['user_gender'] = $_POST['user_gender'];
        $update['user_country'] = $_POST['user_country'];
        $update['user_state'] = $_POST['state'];
        $update['user_city'] = $_POST['city'];
		    $update['user_telephone'] = $_POST['user_telephone'];
		   $update['user_photo'] = $_SESSION["userInfo"]["user_id"]."/".$filename . ".$extension";

        $this->db->cmd->where("user_id", $_SESSION["userInfo"]["user_id"]);
        if($this->db->cmd->update("user", $update)){
            return "Bilgiler Güncellendi";
        }

    }
  
  
  public function UpdateUser($userId) {
        $update = array();

        $update['user_first_name'] = $_POST['user_first_name'];
        $update['user_last_name'] = $_POST['user_last_name'];
		    $update['user_telephone'] = $_POST['user_telephone'];
        $update['user_birthday'] = $_POST['user_birthday'];
        $update['user_country'] = $_POST['user_country'];
        $update['user_state'] = $_POST['user_state'];
        $update['user_city'] = $_POST['user_city'];
        $update['user_gender'] = $_POST['user_gender'];

        $this->db->cmd->where("user_id", $userId);
        if($this->db->cmd->update("user", $update)){
            return true;
        }
       return false;
  }
  
  public function UpdateUserPhoto($userId, $photo) {
        $update = array();
        $update['user_photo'] = $photo;
        $this->db->cmd->where("user_id", $userId);
        if($this->db->cmd->update("user", $update)){
            return true;
        }
       return false;
  }
	
	public function SendPassword()
    {

        if (isset($_POST["member_mail"]) && $_POST["member_mail"] != "") {

            $this->db->cmd->where("user_mail", $_POST["member_mail"]);
            $data = $this->db->cmd->getOne("user");

            if (isset($data["user_mail"])) {

                $tmpkey = \Globalf::Encrypt("encrypt",$data["user_id"] . $data["user_mail"] . time());

                $update = array();
                $update["user_tmp_key"] = $tmpkey;

                $this->db->cmd->where("user_id", $data["user_id"]);
                $this->db->cmd->update("user", $update);

                $link = $this->config["system"]["base_url"] . "user/recoverychange?key=$tmpkey";

                $HTML = <<<HTML
                    <h1>Password Recovery</h1>
                    Please click this link for change your password. 
                    <a href="{$link}">Click here to reset</a>                    
HTML;


                return \Globalf::SendMail($data["user_mail"], "Password Recovery", $HTML);

            } else {
                return false;
            }

        } else {
            return false;
        }
    }
	
	public function RecoveryChange(){

        if(isset($_POST['password']) && $_POST['password'] != "" && isset($_POST['password_retry']) && isset($_POST["key"])){
            if($_POST['password'] == $_POST['password_retry']){

                if (strlen($_POST["password"]) < 6) {
                    return "pwd_short";
                }

                $update = array();
                $update["user_password"] = \Globalf::Encrypt("encrypt", $_POST['password']);

                $this->db->cmd->where("user_tmp_key", $_POST["key"]);
                if($this->db->cmd->update("user", $update)){
                    return "pwd_chngd";
                }

            }else{
                return "repassword";
            }

        }else{
            return "missing";
        }


    }
	
	public function ChangePassword()
    {

        if (    isset($_POST["user_password"]) &&
                isset($_POST["user_password_new"]) &&
                isset($_POST["user_password_retype"]) &&
                isset($_POST["user_id"])
        ) {

            $password = \Globalf::Encrypt("encrypt", $_POST["user_password"]);

            $this->db->cmd->where("user_password", $password);

            $this->db->cmd->where("user_id", $_POST["user_id"]);

            $info = $this->db->cmd->getOne("user");

            if (!isset($info["user_id"])) {
                return "not_exist";
            }

            if ($_POST["user_password_new"] != $_POST["user_password_retype"]) {
                return "not_match";
            }

            if (strlen($_POST["user_password_new"]) < 6) {
                return "pwd_short";
            }

            $password = \Globalf::Encrypt("encrypt", $_POST["user_password_new"]);

            $update = array();
            $update["user_password"] = $password;

            $this->db->cmd->where("user_id", $_POST["user_id"]);
            $this->db->cmd->update("user", $update);

            return "password changed successfully";
        } else {

            return "missing";
        }
    }

    // Kullanıcıya yapılan yorumlar
    public function GetUserReviewList(){

    }

    // Kullanıcının İlanları
    public function GetUserPublication(){

    }

    // Login olan kullanıcının ilanlarını getir ...
    public function GetMyPublication(){

    }

    // Login olan kullanıcının sepetindeki ürünleri ...
    public function GetMyBasketList(){

    }

    // Login olan kullanıcının toplam siparişleri ...
    public function GetMyOrderList(){

    }
	
	// Kullanıcının adresleri
    public function GetMyAddress($userId){
		$this->db->cmd->where("user_address_user_id", $userId);
    $this->db->cmd->join("countries", "countries.id = uadd.user_address_country_id", "INNER");
    $this->db->cmd->join("states", "states.id = uadd.user_address_state_id", "LEFT");
    $this->db->cmd->join("cities", "cities.id = uadd.user_address_city_id", "LEFT");

        return $this->db->cmd->get("user_address as uadd",null, "uadd.*, countries.name as user_address_country_name, states.name as user_address_state_name, cities.name as user_address_city_name");	
    }

    public function GetBankInfo($userId)
    {
        $this->db->cmd->where("user_bank_user_id", $userId);
        return $this->db->cmd->get("user_bank");
    }
	
	public function GetFavorite()
    {
        $this->db->cmd->where("favorite_user_id", $_SESSION['userInfo']['user_id']);
		$this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication", "publication.publication_id = user_favorite.favorite_publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("user_favorite");
    }

    public function GetBuy($userId)
    {
        $this->db->cmd->where("user_order.order_buyer_id", $userId);
        $this->db->cmd->groupBy("user_order.order_id");
        $this->db->cmd->join("user_order_detail", "user_order_detail.order_detail_order_id = user_order.order_id", "INNER");
        $this->db->cmd->join("publication", "publication.publication_id = user_order_detail.order_detail_publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("user_order");
    }

    public function GetSell($userId)
    {
        $this->db->cmd->where("user_order_detail.order_detail_seller_id", $userId);
        $this->db->cmd->groupBy("user_order.order_id");
        $this->db->cmd->join("user_order_detail", "user_order_detail.order_detail_order_id = user_order.order_id", "INNER");
        $this->db->cmd->join("publication", "publication.publication_id = user_order_detail.order_detail_publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("user_order");
    }

    // Sepet Detay Özeti
    public function GetCompleteOrderDetail($id){

        $this->db->cmd->where("order_detail_order_id", $id);
        $this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication", "publication.publication_id = user_order_detail.order_detail_publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("user_order_detail");

    }

  
    public function SaveAddress($address)
    {
        if($this->db->cmd->insert("user_address", $address))
        {
            return true;
        } 
      return false;
    }
  
    public function UpdateAddress($address, $addressId) {
        $this->db->cmd->where("user_address_id", $addressId);
        if($this->db->cmd->update("user_address", $address)){
            return true;
        }
      return false;
    }
  
  public function RemoveAddress($userId, $addressId) {
         $this->db->cmd->where("user_address_user_id", $userId);
         $this->db->cmd->where("user_address_id", $addressId);
        if($this->db->cmd->delete("user_address")){
            return true;
        }
      return false;
  }

    public function SaveBank($bankInfo)
    {
        if($this->db->cmd->insert("user_bank", $bankInfo))
        {
            return true;
        }
      return false;
    }
  
  public function UpdateBank($bankInfo, $bankId) {
        $this->db->cmd->where("user_bank_id", $bankId);
        if($this->db->cmd->update("user_bank", $bankInfo)){
            return true;
        }
      return false;
    }

    public function DeleteBank($userId, $bankId)
    {
        $this->db->cmd->where("user_bank_id", $bankId);
        $this->db->cmd->where("user_bank_user_id", $userId);
        if($this->db->cmd->delete("user_bank")){
            return true;
        }
      return false;
    }

    public function GetCountry(){
        return $this->db->cmd->get("countries");
    }

    public function GetCountrySelect($id){
        $this->db->cmd->where("id", $id);
        return $this->db->cmd->getOne("countries");
    }

    public function GetState($id){
        $this->db->cmd->where("country_id", $id);
        return $this->db->cmd->get("states");
    }

    public function GetCity($id){
        $this->db->cmd->where("state_id", $id);
        return $this->db->cmd->get("cities");
    }

    public function GetUserId(){

        $UserId = isset($_SESSION["user"]["user_id"]) ? $_SESSION["user"]["user_id"] : $_SESSION["user_tmp_key"];

        $this->db->cmd->where("user_id=? or user_tmp_key = ?", array($UserId, $UserId));

        $res = $this->db->cmd->getOne("user", "user_id");

        return $res["user_id"];
    }

    public function GetMessage()
    {
        $this->db->cmd->where("user_message_seller_id", $_SESSION["userInfo"]["user_id"]);
        return $this->db->cmd->get("user_message");
    }

    public function GetMessageDetail($id)
    {
        $this->db->cmd->where("user_message_id", $id);
        return $this->db->cmd->get("user_message_detail");
    }

    public function AddMessage()
    {
        $insert = array();
        $insert['user_message_title'] = $_POST['message_title'];
        $insert['user_message_buyer_id'] = $_SESSION["userInfo"]["user_id"];
        $insert['user_message_seller_id'] = $_POST['seller_id'];
        $this->db->cmd->insert("user_message", $insert);

        echo $this->db->cmd->getLastQuery();

        $lastId = $this->db->cmd->getInsertId();

        $insert2 = array();
        $insert2['text'] = $_POST['message_text'];
        $insert2['user_buyer_id'] = $_SESSION["userInfo"]["user_id"];
        $insert2['user_seller_id'] = $_POST['seller_id'];
        $insert2['user_message_id'] = $lastId;
        if($this->db->cmd->insert("user_message_detail", $insert2))
        {
            return "Kayıt Başarılı";
        }
    }
	
	public function AddMessages($id)
    {
        $insert2 = array();
        $insert2['text'] = $_POST['message_text'];
        $insert2['user_buyer_id'] = $_SESSION["userInfo"]["user_id"];
        $insert2['user_seller_id'] = $_POST['seller_id'];
        $insert2['user_message_id'] = $id;
        if($this->db->cmd->insert("user_message_detail", $insert2))
        {
            return "Kayıt Başarılı";
        }
    }

    public function GetSellOrder()
    {
        $this->db->cmd->where("user_order.order_status", "S");
        $this->db->cmd->where("user_order_detail.order_detail_seller_id", $_SESSION['userInfo']['user_id']);
        $this->db->cmd->join("user_order_detail", "user_order_detail.order_detail_order_id = user_order.order_id", "INNER");
        $this->db->cmd->join("publication", "publication.publication_id = user_order_detail.order_detail_publication_id");
        return $this->db->cmd->get("user_order");

    }

}