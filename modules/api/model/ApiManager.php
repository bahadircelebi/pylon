<?php
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 13.04.2018
 * Time: 16:39
 */
namespace apispace;

class ApiManager extends \BaseModel
{

    static function CheckSecure(){

   
    }

    public function PublicationSearch()
    {

        $CategoryManager = new \defaultspace\CategoryManager();
        /*$this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        //return $this->db->cmd->get("publication");
        return $this->db->cmd->paginate("publication", 1);*/
        $word = $_GET['t'];
        $categoryId = $_GET['q'];
        $fromCountry = $_GET['from_country'];
        $toCountry = $_GET['to_country'];
        $startDate = $_GET['start_date'];
        $endDate = $_GET['end_date'];
        $minRange = $_GET['range_min'];
        $maxRange = $_GET['range_max'];
      
        $allCat = $CategoryManager->GetSubAllCategory($categoryId);

        $allCat = array_values($allCat);

        $allCat[] = $categoryId;

        $CatOptions = $CategoryManager->GetCategoryOptionList($categoryId);
        $opt = "";
        foreach($CatOptions as $CatOption):

            $options = $CategoryManager->GetOptions($CatOption['option_id']);

            foreach($options as $option):
                //numara == numara

                $values = array_values($_GET[$option['option_key_value']]);

                if( in_array($option['option_value'], $values)):
                    $opt[] = $option['option_detail_id'];
                endif;
            endforeach;

        endforeach;


        if($opt != ""){
            $this->db->cmd->where("publication_option.publication_option_detail_id", $opt, "IN");
        }

        if(isset($fromCountry) && $fromCountry){
            $this->db->cmd->where("publication.publication_from_country", $fromCountry);
        }

        if(isset($toCountry) && $toCountry){
            $this->db->cmd->where("publication.publication_to_country", $toCountry);
        }

        if(isset($startDate) && $startDate){
            $this->db->cmd->where("(publication.publication_start_date >= ?)", array($startDate));
        }
      
        if(isset($endDate) && $endDate){
            $this->db->cmd->where("(publication.publication_end_date <= ?)", array($endDate));
        }
      

        if(isset($minRange) && $minRange){
            $this->db->cmd->where("(publication.publication_price >= ?)", array($minRange));
        }
      
        if(isset($maxRange) && $maxRange){
            $this->db->cmd->where("(publication.publication_price <= ?)", array($maxRange));
        }

        $this->db->cmd->where("publication.publication_status", "A");
        $this->db->cmd->where("publication.publication_title", "%".$word."%", "LIKE");
        $this->db->cmd->where("publication.publication_category_id", $allCat, "IN");
        $this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        $this->db->cmd->join("publication_option", "publication_option.publication_option_publication_id = publication.publication_id", "LEFT");
        $data =  $this->db->cmd->get("publication");

        return $data;
    }
  
  
   // Populer İlanlar
    public function GetPopulerPublicationList(){

        $this->db->cmd->where("publication.publication_status", "A");
        $this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("publication", 12);

    }

    // Populer İlanlar
    public function GetNewPublicationList(){

        $this->db->cmd->where("publication.publication_status", "A");
        $this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("publication", 4);

    }
  
  public function addPublication($insert) {
    		$this->db->cmd->insert("publication", $insert);
		    $lastId = $this->db->cmd->getInsertId();
        return $lastId;
  }
  
  public function addPublicationPhotos($insert) {
    $this->db->cmd->insert("publication_photo", $insert);
    $lastId = $this->db->cmd->getInsertId();
        return $lastId;
  }
  
  //************************************************************************************************************//
  // Addresses
  //************************************************************************************************************//
	
}