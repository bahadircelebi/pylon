<?php
namespace apispace;

/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 15.01.2018
 * Time: 15:42
 */

class BasketManager extends \BaseModel
{

    // Sepete Ilan Ekle
    /**
     *
     */
    public function AddToBasket(){

        if(isset($_POST["id"]) && isset($_POST["user_id"]) && $_POST["qty"] && $_POST["sid"]){

            $UserId =  $_POST["user_id"];

            $this->db->cmd->where("user_basket_publication_id", $_POST["id"]);
            $this->db->cmd->where("user_basket_buyer_id", $UserId);
            $publication = $this->db->cmd->getOne("user_basket");

            if(isset($publication['user_basket_publication_id'])){

                $update = array();
                $update['user_basket_amount'] = $publication['user_basket_amount']+$_POST['qty'];

                $this->db->cmd->where("user_basket_publication_id", $_POST["id"]);
                $this->db->cmd->where("user_basket_buyer_id", $UserId);
                if($this->db->cmd->update('user_basket', $update)):
					return true;
				endif;
            }
            else
            {
                $insert = array();
                $insert['user_basket_publication_id'] = $_POST["id"];
                if($UserId > 0):
                    $insert['user_basket_buyer_id'] = $UserId;
                    $insert['user_tmp_key'] = 0;
                else:
                    $insert['user_basket_buyer_id'] = 0;
                    $insert['user_tmp_key'] = $UserId;
                endif;

				$insert['user_basket_seller_id'] = $_POST["sid"];
                $insert['user_basket_amount'] = $_POST["qty"];

                if($this->db->cmd->insert('user_basket', $insert)):
                    return true;
				endif;
            }

            return false;

        }

    }

    public function ChangeQuantity()
    {
        $UserId = isset($_POST["user_id"]) ? $_POST["user_id"] : 0;

        if( $UserId > 0 && isset($_POST['pid'])){

            $update = array();
            $update["user_basket_amount"] = $_POST['amount'];

            $this->db->cmd->where("user_basket_buyer_id", $UserId);
            $this->db->cmd->where("user_basket_publication_id", $_POST['pid']);
            $this->db->cmd->update('user_basket', $update);

            return true;
        }


    }

    public function DeleteFromBasket()
    {
        $UserId = isset($_POST["user_id"]) ? $_POST["user_id"] : 0;

        if(isset($_POST["id"]) && $UserId > 0){

            $this->db->cmd->where("user_basket_publication_id", $_POST["id"]);
            $this->db->cmd->where("user_basket_buyer_id", $UserId);

            if($this->db->cmd->delete('user_basket')):
                return true;
            endif;

        }else{
            return false;
        }

    }

    // Sepetden Tüm İlanları Sil
    public function BasketEmpty(){

        $UserId = isset($_POST["user_id"]) ? $_POST["user_id"] : 0;

        $this->db->cmd->where("user_basket_buyer_id", $UserId);

        $this->db->cmd->delete('user_basket');

        return true;
    }

  
  public function GetBasketProducts($UserId) {
    $this->db->cmd->orderBy("user_basket_seller_id", "ASC");
        $this->db->cmd->where("user_tmp_key", $UserId);
        $this->db->cmd->where("user_basket_buyer_id", $UserId, "=", "OR");
        $this->db->cmd->join("publication", "publication.publication_id = user_basket.user_basket_publication_id");
        $products =  $this->db->cmd->get("user_basket");
    return $products;
  }
    // Sepete Eklenen İlanlar
    public function GetBasketPublictionList(){

        $UserId = isset($_POST["user_id"]) ? $_POST["user_id"] : 0;

        $this->db->cmd->where("user_basket_buyer_id", $UserId);

        $this->db->cmd->groupBy("publication.publication_id");

        $this->db->cmd->join("publication", "publication.publication_id = user_basket.user_basket_publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");

        return $this->db->cmd->get("user_basket");

    }

    // Sepet Özeti
    public function GetCompleteOrder($orderId){
       $this->db->cmd->where("user_order.order_id", $orderId);
        $this->db->cmd->join("user_order_detail", "user_order_detail.order_detail_order_id = user_order.order_id", "INNER");
        $this->db->cmd->join("publication", "publication.publication_id = user_order_detail.order_detail_publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->getOne("user_order");
    }

    // Sepet Detay Özeti
    public function GetCompleteOrderDetail($orderId){

        if($orderId>0){
            $this->db->cmd->where("order_detail_order_id", $orderId);
            $this->db->cmd->groupBy("publication.publication_id");
            $this->db->cmd->join("publication", "publication.publication_id = user_order_detail.order_detail_publication_id");
            $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");

            return $this->db->cmd->get("user_order_detail");
        }
    }

    public function GetBasketTotalPubliction()
    {
        $UserId = isset($_SESSION["userInfo"]["user_id"]) ? $_SESSION["userInfo"]["user_id"] : $_COOKIE['user_tmp_key'];

        $this->db->cmd->where("user_tmp_key", $UserId);
        $this->db->cmd->where("user_basket_buyer_id", $UserId, "=", "OR");
        $product = $this->db->cmd->get("user_basket");

        $totalProduct = 0;
        if(count($product)>0){

            foreach($product as $qty):
                $totalProduct+=$qty['user_basket_amount'];
            endforeach;

            return $totalProduct;
        }else{
            return $totalProduct;
        }

    }

    public function GetAddressId($id)
    {
        $this->db->cmd->where("user_address_id", $id);
        return $this->db->cmd->getOne("user_address");
    }
	
	public function Address()
	{
		$insert = array();
		$insert['user_address_title'] = $_POST['address_title'];;
		$insert['user_address_user_id'] = $_SESSION["userInfo"]["user_id"];
		$insert['user_address_country_id'] = $_POST['user_country'];
		$insert['user_address_city_id'] = $_POST['city'];
		$insert['user_address_text'] = $_POST['address'];
		$insert['user_address_phone'] = $_POST['telephone'];
		$insert['user_address_post_code'] = $_POST['postal_code'];
		$insert['user_address_state_id'] = $_POST['state'];
		if($this->db->cmd->insert("user_address", $insert))
		{
			return "OK";
		}
	}

	public function SaveOrder($insert)
    {
      $this->db->cmd->insert("user_order", $insert);
      $lastId = $this->db->cmd->getInsertId();
      return $lastId;
    }
  
  public function UpdateOrder($orderId, $update)
    {
      $this->db->cmd->where("order_id", $orderId);
     $this->db->cmd->update("user_order", $update);
    }
  
  public function SaveMovement($insert) {
    $this->db->cmd->insert("user_order_movement", $insert);
  }
  
  public function SaveOrderPayment($insert) {
     $this->db->cmd->insert("pending_payment", $insert);
  }
  
  public function UpdateOrderPayment($orderId, $update) {
     $this->db->cmd->where("pending_payment_order_id", $orderId);
     $this->db->cmd->update("pending_payment", $update);
  }
  
  public function saveOrderDetails($insert) {
    $this->db->cmd->insert("user_order_detail", $insert);
  }

  public function deleteBasket($userId) {
    $this->db->cmd->where("user_basket_buyer_id", $userId);
    $this->db->cmd->delete('user_basket');
  }

}