<?php global $Caption; ?>
<div class="container-fluid bg-soft-gray py-5">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-12 col-lg-6">
				<h3 class="text-center">BuygoSell</h3>
				<p class="text-center mx-3">
					<b>Parola sıfırlama.</b> Lütfen hesabınızı oluşturmak için kullandığınız e-posta adresini girin. Parolanızı sıfırlama hakkında daha fazla talimatla size bir e-posta gönderilecektir.</p>
				<div class="card">
					<div class="card-body">
						<form id="login-form" action="/user/recovery" method="post" data-toggle="validator">
							<div class="form-group">
								<input type="text" name="member_mail" class="form-control" placeholder="mail_adresi@email.com">
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn-info bg-orange border-0 btn-block" name="send" value="Eposta Gönder">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>