<h4 class="orange-color">Parola Güncelleme</h4>
<form method="post" action="/profile/password-change">
	<div class="form-group">
		<input type="password" class="form-control green-border rounded-0" name="user_password" placeholder="Eski Şifre">
	</div>

	<div class="form-group">
		<input type="password" class="form-control green-border rounded-0" name="user_password_new" placeholder="Yeni Şifre">
	</div>

	<div class="form-group">
		<input type="password" class="form-control green-border rounded-0" name="user_password_retype" placeholder="Yeni Şifre Tekrar">
	</div>

	<div class="form-row mb-5">
        <button type="submit" class="btn btn-warning bg-orange rounded-0 orange-border text-white">Güncelle</button>
	</div>
</form>