<?php global $Caption; ?>
<?php
if(isset($data['m']) && $data['m'] == 'pwd_chngd')
    header("refresh:3; url=/user/login");
?>
<div class="container my-5">
    <div class="row justify-content-md-center">
        <div class="col-12 col-lg-6">
            <h3 class="text-center">BuygoSell</h3>
            <p class="text-center mx-3">
                <b>Şifre Yenileme.</b>
                Lütfen yeni şifrenizi oluşturun</p>
            <div class="card">
                <div class="card-body">
                    <form id="login-form" action="/user/recoverychange?key=<?=$_GET['key']?>" method="post" data-toggle="validator">
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Yeni Şifre">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password_retry" class="form-control" placeholder="Yeni Şifre Tekrar">
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="key" value="<?=$_GET['key']?>">
                            <input type="submit" class="btn btn-info bg-orange border-0 btn-block" name="send" value="Güncelle">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>