<h4 class="orange-color">Kişisel Bilgiler</h4>
<form action="/profile/update" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<input type="text" class="form-control green-border rounded-0" name="username" placeholder="Kullanıcı Adı" value="<?=$data['user']['username']?>">
	</div>
	<div class="form-group">
		<input type="text" class="form-control green-border rounded-0" name="user_first_name" placeholder="Ad" value="<?=$data['user']['user_first_name']?>">
	</div>
	<div class="form-group">
		<input type="text" class="form-control green-border rounded-0" name="user_last_name" placeholder="Soyad" value="<?=$data['user']['user_last_name']?>">
	</div>
	<div class="form-group">
		<input id="datepicker" type="text" class="form-control green-border rounded-0" name="user_birthday" placeholder="Doğum Günü" value="<?=$data['user']['user_birthday']?>">
	</div>

	<div class="form-group">
		<div class="btn-group btn-group-toggle" data-toggle="buttons">
		  <label class="btn btn-secondary <?=($data['user']['user_gender']=="M" ? "active" : "")?>">
			<input type="radio" name="user_gender" id="option1" autocomplete="off" <?=($data['user']['user_gender']=="M" ? "checked" : "")?> value="M"> Bay
		  </label>
		  <label class="btn btn-secondary <?=($data['user']['user_gender']=="F" ? "active" : "")?>">
			<input type="radio" name="user_gender" id="option2" autocomplete="off" <?=($data['user']['user_gender']=="F" ? "checked" : "")?> value="F"> Bayan
		  </label>
		  <label class="btn btn-secondary <?=($data['user']['user_gender']=="O" ? "active" : "")?>">
			<input type="radio" name="user_gender" id="option3" autocomplete="off" <?=($data['user']['user_gender']=="O" ? "checked" : "")?> value="O"> Diğer
		  </label>
		</div>
	</div>
	<div class="form-group">
		<input type="text" class="form-control green-border rounded-0" name="user_telephone" placeholder="Telefonu" value="<?=$data['user']['user_telephone']?>">
	</div>
	<div class="form-group">
		<select name="user_country" class="form-control green-border rounded-0" onchange="globalJS.utility.getPost('/user/get-state','state', 'id='+this.value)">
			<option>Ülke</option>
			<?php foreach($data['country'] as $country): ?>
				<option value="<?=$country['id']?>" <?=($country['id'] == $data['user']['user_country'] ? "selected" : "")?>><?=$country['name']?></option>
			<?php endforeach; ?>
		</select>
	</div>
    <div class="form-group">
        <div id="state">
            <select name="state" class="form-control green-border rounded-0" onchange="globalJS.utility.getPost('/user/get-city','city', 'id='+this.value)">
                <option>Eyalet</option>
                <?php foreach($data['state'] as $state): ?>
                    <option value="<?=$state['id']?>" <?=($state['id'] == $data['user']['user_state'] ? "selected" : "")?>><?=$state['name']?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
	<div class="form-group">
		<div id="city">
            <select name="city" class="form-control green-border rounded-0">
                <option>Şehir</option>
                <?php foreach($data['city'] as $city): ?>
                    <option value="<?=$city['id']?>" <?=($city['id'] == $data['user']['user_city'] ? "selected" : "")?>><?=$city['name']?></option>
                <?php endforeach; ?>
            </select>
        </div>
	</div>
	<div class="form-group">
		<small>Profil Resmi</small><br>
		<div class="custom-file">
			<input type="file" class="custom-file-input green-border rounden-0" name="file" id="file">
			<label class="custom-file-label" for="customFile">Resim Seç</label>
		</div>
	</div>
	<div class="form-group">
		<button class="btn btn-warning bg-orange rounded-0 orange-border text-white">Güncelle</button>
	</div>
</form>