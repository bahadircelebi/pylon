<div class="col-lg-4">
    <div class="card bg-green border-0 rounded-0 offcanvas-collapse">
        <div class="card-body">
            <ul class="list-group">
                <li class="list-group-item bg-green text-white border-0 rounded-0">PROFILIM</li>
                <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
                    <a href="/profile/manage" class="text-white">İlan Yönetimi</a>
				  <i class="fa fa-cog fa-lg float-right" aria-hidden="true"></i>
                </li>
                <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
                    <a href="/profile" class="text-white">Kişisel Bilgiler</a>
                    <i class="fa fa-user fa-lg float-right" aria-hidden="true"></i>
                </li>
                <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
                    <a href="/profile/message" class="text-white">Mesajlar</a>
					<i class="fa fa-comments-o fa-lg float-right" aria-hidden="true"></i>
                </li>
                <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
                    <a href="/profile/bank" class="text-white">Banka Hesap Bilgileri</a>
					<i class="fa fa-money fa-lg float-right" aria-hidden="true"></i>
                </li>
                <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
                    <a href="/profile/address" class="text-white">Adres Bilgileri</a>
					<i class="fa fa-location-arrow fa-lg float-right" aria-hidden="true"></i>
                </li>
                <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
					<a href="/profile/password-change" class="text-white">Parola Güncelleme</a>
                    <i class="fa fa-unlock-alt fa-lg float-right" aria-hidden="true"></i>
                </li>
                <!--<li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
                    Kişilerim
                    <i class="fa fa-users fa-lg float-right" aria-hidden="true"></i>
                </li>-->
                <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
					<a href="/profile/favorite" class="text-white">Favori Ürünlerim</a>
					<i class="fa fa-heart fa-lg float-right" aria-hidden="true"></i>
                </li>
                <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
                    <a href="/profile/buy" class="text-white">Aldıklarım</a>
					<i class="fa fa-arrow-left fa-lg float-right" aria-hidden="true"></i>
                </li>
                <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
                    <a href="/profile/sell" class="text-white">Sattıklarım</a>
					<i class="fa fa-arrow-right fa-lg float-right" aria-hidden="true"></i>
                </li>
                <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
                    <a href="/user/logout" class="text-white">Çıkış</a>
                    <i class="fa fa-sign-out fa-lg float-right" aria-hidden="true"></i>
                </li>
            </ul>
        </div>
    </div>
</div>