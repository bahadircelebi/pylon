<div class="container-fluid bg-white">
    <div class="container py-5">
        <div class="row text-center">
            <div class="col-12 col-md-3 orange-color mt-3">
                <h4 class="m-0">1. Alışveriş Sepeti</h4>
                <small class="text-muted">Ürün listenizi düzenleyin</small>
            </div>
			<div class="col-md-1 d-none d-md-block">
				<i class="fa fa-angle-right fa-4x" aria-hidden="true"></i>
			</div>
            <div class="col-12 col-md-4 mt-3 orange-color">
                <h4 class="m-0">2. Teslimat & Ödeme</h4>
                <small class="text-muted">Adresinizi düzenleyin & Ödeme tipi seçin</small>
            </div>
			<div class="col-md-1 d-none d-md-block">
				<i class="fa fa-angle-right fa-4x" aria-hidden="true"></i>
			</div>
            <div class="col-12 col-md-3 mt-3">
                <h4 class="m-0">3. Alışveriş Özeti</h4>
                <small class="text-muted">Sipariş özetiniz</small>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div><hr class="m-0">
<form action="/basket/save-order" method="post" class="was-validated">
    <div class="container-fluid bg-soft-gray">
        <div class="container py-5">
            <div class="row">
                <div class="col-12 col-md-6">
                    <h5>Teslimat Adresi</h5>
                    <div class="form-group">
                        <select name="user_delivery_address" class="form-control" required>
                            <option value="">Teslimat Adresi Seçin</option>
                            <?php foreach($data['address'] as $address): ?>
                            <option value="<?=$address['user_address_id']?>">
                                <?=$address['user_address_title']?></option>
                            <?php endforeach; ?>
                        </select>
                        <a href="#" class="btn btn-link" data-toggle="modal" data-target="#addressModal">+ Yeni Adres Ekle</a>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <h5>Ödeme Şekli</h5>
                    <div class="form-group">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio1" name="payment_type" class="custom-control-input" value="C" required disabled>
                            <label class="custom-control-label" for="customRadio1">Kredi Kartı <small>(Yakın zamanda hizmetinizde)</small></label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio2" name="payment_type" class="custom-control-input" value="D" required>
                            <label class="custom-control-label" for="customRadio2">Kapıda Ödeme</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio3" name="payment_type" class="custom-control-input" value="T" required>
                            <label class="custom-control-label" for="customRadio3">Havale</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php
        $totalProduct = 0;
        $productPrice = 0;

        if(count($data['publication_list'])>0):
            foreach($data['publication_list'] as $publication):
                $productPrice += $publication['user_basket_amount']*$publication['publication_price'];
            endforeach;
        endif;
    ?>
    <div class="container-fluid bg-white">
        <div class="container py-5">
            <div class="row">
                <div class="col-3 col-md-7 pt-2">
                    <a href="/basket/shopping-cart" class="btn btn-warning btn-sm bg-orange rounded-0 orange-border text-white">Geri</a>
                </div>
                <div class="col-4 col-md-2 text-right">
                    <small class="text-muted">Toplam</small>
                    <h5><?=$productPrice?> $</h5>
                </div>
                <div class="col-5 col-md-3 pt-2">
                    <button type="submit" class="btn btn-warning btn-sm bg-orange rounded-0 orange-border text-white btn-block">
                        Ödeme Yap
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>


<!-- Modal -->
<div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content rounded-0 border-0 shadow">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Yeni Adres</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="address-form">
		  <div class="form-row">
			<div class="form-group col-md-12">
			  <label for="inputEmail4">Adres Başlığı</label>
			  <input name="address_title" type="text" class="form-control" id="inputEmail4" placeholder="Başlık">
			</div>
		  </div>
		  <div class="form-group">
			<label for="inputAddress">Adres</label>
			<input name="address" type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
		  </div>
		  <div class="form-row">
			<div class="form-group col-md-6">
			  <label for="inputZip">Posta Kodu</label>
			  <input name="postal_code" type="text" class="form-control" id="inputZip">
			</div>
			<div class="form-group col-md-6">
			  <label>Telefon</label>
			  <input name="telephone" type="text" class="form-control">
			</div>  
		  </div>	
		  <div class="form-row">
			<div class="form-group col-md-4">
			  <label for="inputCity">Ülke</label>
			  <select name="user_country" class="form-control green-border rounded-0" onchange="globalJS.utility.getPost('/user/get-state','state', 'id='+this.value)">
					<option>Ülke</option>
					<?php foreach($data['country'] as $country): ?>
						<option value="<?=$country['id']?>"><?=$country['name']?></option>
					<?php endforeach; ?>
				</select>
			</div>
            <div class="form-group col-md-4">
                <label for="inputState">Eyalet</label>
                <div id="state"></div>
            </div>
			<div class="form-group col-md-4">
			  <label for="inputState">Şehir</label>
			  <div id="city"></div>
			</div>
		  </div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Kapat</button>
        <button type="button" class="btn btn-primary btn-sm" onclick="globalJS.utility.getPostSerialize('/basket/address','-','#address-form','')">Kaydet</button>
      </div>
    </div>
  </div>
</div>