<div class="container-fluid bg-white">
    <div class="container py-5">
        <div class="row text-center">
            <div class="col-12 col-md-3 orange-color mt-3">
                <h4 class="m-0">1. Alışveriş Sepeti</h4>
                <small class="text-muted">Ürün listenizi düzenleyin</small>
            </div>
            <div class="col-md-1 d-none d-md-block">
                <i class="fa fa-angle-right fa-4x" aria-hidden="true"></i>
            </div>
            <div class="col-12 col-md-4 mt-3 orange-color">
                <h4 class="m-0">2. Teslimat & Ödeme</h4>
                <small class="text-muted">Adresinizi düzenleyin & Ödeme tipi seçin</small>
            </div>
            <div class="col-md-1 d-none d-md-block">
                <i class="fa fa-angle-right fa-4x" aria-hidden="true"></i>
            </div>
            <div class="col-12 col-md-3 mt-3 orange-color">
                <h4 class="m-0">3. Alışveriş Özeti</h4>
                <small class="text-muted">Sipariş özetiniz</small>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div><hr class="m-0">

<div class="contaianer-fluid bg-soft-gray pb-4">
    <div class="container pt-5">
		
		<div class="alert alert-info rounded-0">
			<i class="fa fa-info-circle" aria-hidden="true"></i>
			Siparişiniz başarıyla alınmıştır. Sipariş detaylarını aşağıdan inceleyebilirsiniz. 
		</div>
		
        <h3>Sipariş Özeti</h3>
        <div class="clearfix"></div><hr>

        <div class="row">
            <div class="col-6 col-md-4"><small>Sipariş Kodu<br>#<?=$data['order']['order_code']?></small></div>
            <div class="col-6 col-md-4"><small>Sipariş Tarihi<br><?=$data['order']['order_date']?></small></div>
            <div class="col-6 col-md-4"><small>Ödeme Türü<br><?=$data['order']['order_payment_type']?></small></div>
        </div>

        <div class="clearfix my-3"></div>

        <div class="row">
            <div class="col-6"><small>Fatura Adresi<br><?=$data['delivery_address']['user_address_text']?></small></div>
            <div class="col-6"><small>Teslimat Adresi<br><?=$data['invoice_address']['user_address_text']?></small></div>
        </div>

        <h3>Ürün Özeti</h3>
        <div class="clearfix"></div><hr>

        <?php $i=1; foreach($data['order_detail'] as $detail): ?>

            <div class="row">
                <div class="col-3 col-md-2">
                    <img src="/images/product/<?=$detail['publication_photo_path']."-150.".$detail['publication_photo_extension']?>" class="img-fluid border orange-border" width="100">
                </div>
                <div class="col-9 col-md-10">
                    <div class="row">
                        <div class="col-12 col-md-6 text-left align-self-center">
                            <?=$detail['publication_title']?>
                        </div>
                        <div class="col-6 col-md-3 text-left text-md-right align-self-center">
                            <?=$detail['order_detail_publication_amount']?> Adet
                        </div>
                        <div class="col-6 col-md-3 text-right text-md-right align-self-center">
                            <?=$detail['publication_price']." ".$detail['publication_currency']?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix mb-2"></div>

        <?php $i++; endforeach; ?>

    </div>
</div>

<div class="container-fluid bg-white">
    <div class="container py-5">
        <div class="row text-right">
            <div class="col-5">
            </div>
            <div class="col-2">

            </div>
            <div class="col-2">

            </div>
            <div class="col-3">
                <small class="text-muted">Toplam</small>
                <h5><?=$data['order']['order_total_rate']?> $</h5>
            </div>
        </div>
    </div>
</div>