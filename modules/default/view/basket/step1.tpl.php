<script>
    $('#myTooltip').on('hidden.bs.tooltip', function () {
        // do something…
    })
</script>

<div class="container-fluid bg-white">
    <div class="container py-5">
        <div class="row text-center">
            <div class="col-12 col-md-3 orange-color mt-3">
                <h4 class="m-0">1. Alışveriş Sepeti</h4>
                <small class="text-muted">Ürün listenizi düzenleyin</small>
            </div>
			<div class="col-md-1 d-none d-md-block">
				<i class="fa fa-angle-right fa-4x" aria-hidden="true"></i>
			</div>
            <div class="col-12 col-md-4 mt-3">
                <h4 class="m-0">2. Teslimat & Ödeme</h4>
                <small class="text-muted">Adresinizi düzenleyin & Ödeme tipi seçin</small>
            </div>
			<div class="col-md-1 d-none d-md-block">
				<i class="fa fa-angle-right fa-4x" aria-hidden="true"></i>
			</div>
            <div class="col-12 col-md-3 mt-3">
                <h4 class="m-0">3. Alışveriş Özeti</h4>
                <small class="text-muted">Sipariş özetiniz</small>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div><hr class="m-0">

<div class="container-fluid bg-soft-gray">

    <div class="container bg-soft-gray py-5">

        <?php

			$totalProduct = 0; 
			$productPrice = 0;

            if(count($data['publication_list'])>0):

			foreach($data['publication_list'] as $publication): ?>
        <div class="row">
            <div class="col-4 col-md-2 col-lg-1">
                <img src="/images/product/<?=$publication['publication_photo_path']."-150.".$publication['publication_photo_extension']?>" class="img-fluid border orange-border" width="100">
            </div>
            <div class="col-12 col-md-4 col-lg-7 pt-2 d-none d-md-block align-self-center">
                <?=$publication['publication_title']?>
            </div>
            <div class="col-3 col-md-2 col-lg-1 text-right align-self-center">
                <form id="piece<?=$publication['publication_id']?>" method="post" action="/basket/change-quantity">
                    <input name="qty<?=$publication['publication_id']?>" class="form-control" type="number" onchange="$('#piece<?=$publication['publication_id']?>').submit();" value="<?=$publication['user_basket_amount']?>">
                    <input name="pid" type="hidden" value="<?=$publication['publication_id']?>">
                </form>
            </div>
            <div class="col-3 col-md-2 col-lg-2 text-right align-self-center">
                <h6><?=$publication['publication_price']." ".$publication['publication_currency']?></h6>
            </div>
            <div class="col-2 col-md-2 col-lg-1 align-self-center">
                <button class="btn btn-link" onclick="globalJS.utility.getPostSerialize('/basket/delete-item','-','#prd<?=$publication['publication_id']?>','')">
                    <i class="fa fa-times text-danger" aria-hidden="true"></i>
                </button>
                <input name="id" id="prd<?=$publication['publication_id']?>" type="hidden" value="<?=$publication['publication_id']?>">
            </div>
            <div class="col-12 mt-2 d-block d-md-none align-self-center">
                <a href="/product/detail/<?=$publication['publication_id']?>" class="black-color"><?=$publication['publication_title']?></a>
            </div>
        </div>

        <div class="clearfix"></div><hr>
			
        <?php 
			$productPrice += $publication['user_basket_amount']*$publication['publication_price'];
			endforeach;
        ?>
            <form method="post" action="/basket/empty-basket">
                <button type="submit" class="btn btn-danger btn-sm rounded-0 float-right mb-3">Sepeti Boşalt</button>
            </form>

        <?php else: ?>
            <p class="text-center">Sepette hiç ürün yok</p>
        <?php endif; ?>

    </div>

</div>

<div class="container-fluid bg-white">
    <div class="container py-5">
        <div class="row">
            <div class="col-8 col-md-10 text-right pt-2">
                <small class="text-muted">Toplam</small>
                <h5><?=$productPrice?> $</h5>
            </div>
            <div class="col-4 col-md-2 pt-2">
                <?php if(isset($_SESSION['userInfo'])): ?>
                    <a href="/basket/delivery-payment" class="btn btn-warning btn-sm bg-orange rounded-0 orange-border text-white btn-block">İleri</a>
                <?php else: ?>
                    <button type="button" class="btn btn-warning btn-sm bg-orange rounded-0 orange-border text-white btn-block" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Satın almak için Giriş Yapmalısınız... Giriş yapmak için <a href='/user/login'>tıklayınız.</a>">
                        İleri
                    </button>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>