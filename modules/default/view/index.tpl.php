<?php global $Caption; ?>
<script src="/js/jquery.vide.js"></script>
<style>
    <?php $i=1; foreach($data['slider'] as $slider): ?>
    .accordion ul li:nth-child(<?=$i?>) { background-image: url("/images/site/<?=$slider['slider_image']?>"); }
    <?php $i++; endforeach; ?>

</style>
<!--
<div class="container-fluid p-0 d-none d-md-block">
    <div id="block2" style="width: 100%; height: 500px;"
         data-vide-bg="mp4: /video/ocean, webm: video/ocean, ogv: http://vodkabears.github.io/vide/video/ocean, poster: /jquery.vide.jsvideo/ocean"
         data-vide-options="position: 0% 50%">
    </div>
</div>
-->
<div class="container-fluid py-4 bg-white">
    <div class="container mb-5">
        <h3 class="float-left gray-color mt-5"><b>Buygosell <?=$Caption['categories']?> </b></h3>
        <div class="clearfix"></div><hr class="bg-green">

        <div class="row main-categories">
            <?php $i=1; foreach($data['category'] as $category): ?>
            <div class="col text-center">
                <a href="/category?q=<?=$category['category_id']?>">
                <img src="/images/site/<?=$category['category_icon']?>">
                <p class="gray-color">
                    <?php if($_SESSION['lang']=='tr'): ?>
                        <?=$category['category_name']?>
                    <?php else: ?>
                        <?=$category['category_name_en']?>
                    <?php endif; ?>
                </p>
                </a>
            </div>
            <?php $i++; endforeach; ?>
        </div>
    </div>
</div>

<div class="container-fluid bg-white">
<?php if(count($data['publicationList'])>0): ?>
<div class="container">

    <h3 class="float-left gray-color"><b><?=$Caption['best_sellers']?></b></h3>
    <div class="clearfix d-block d-sm-none"></div>
    <div class="clearfix d-none d-sm-block d-md-none"></div>
    <!--<ul class="list-inline inline-menu float-md-right mt-2">
        <li class="list-inline-item">
            <a href="#" class="">Yenü Ürünler</a>
        </li>
        <li class="list-inline-item">
            <a href="#">Aktif Yolculuk</a>
        </li>
        <li class="list-inline-item">
            <a href="#">Seçtiklerimiz</a>
        </li>
    </ul>-->

    <div class="clearfix"></div><hr class="bg-green">

    <div class="row">

        <?php foreach($data['publicationList'] as $publication): ?>
        <div class="col-md-6 col-lg-3 mb-4">

            <div class="card border-0 text-white shadow">
                <img class="card-img" src="/images/product/<?=$publication['publication_photo_path'].".".$publication['publication_photo_extension']?>">
                <div class="d-flex justify-content-center card-img-overlay p-0">

                    <div class="btn-toolbar align-items-end" role="toolbar" aria-label="Toolbar with button groups">
                        <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                            <button type="button" class="btn btn-light orange-color" onclick="location.href = '/product/detail/<?=$publication['publication_id']?>'">
                                <?=$Caption['detail']?>
                            </button>
                            <button type="button" class="btn btn-light btn-block orange-color" onclick="globalJS.utility.AddBasket('/basket/add','basketTotal','id=<?=$publication['publication_id']?>&qty=1&sid=<?=$publication['publication_user_id']?>', '/basket/total-product')">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=$Caption['add_to_basket']?>
                            </button>
                            <a href="/images/product/<?=$publication['publication_photo_path'].".".$publication['publication_photo_extension']?>" class="btn btn-light orange-color" data-fancybox="">
                                <i class="fa fa-search-plus" aria-hidden="true"></i>
                            </a>
                            <?php if(isset($_SESSION["userInfo"]["user_id"])): ?>
                                <button type="button" class="btn btn-light orange-color" onclick="globalJS.utility.getPost('/product/add-favorite','-','id=<?=$publication['publication_id']?>','Favori Listesine Eklendi')">
                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                </button>
                            <?php endif; ?>
                        </div>
                    </div>

                </div>
            </div>
            <p class="m-0 pt-1 text-center"><?=$publication['publication_title']?></p>

            <!--
            <div class="card bg-dark text-white border-0 rounded-0 shadow">
                <img class="card-img rounded-0" src="/images/product/<?=$publication['publication_photo_path'].".".$publication['publication_photo_extension']?>" alt="Card image">
                <div class="card-img-overlay">
                    <span class="badge badge-secondary rounded-0 bg-orange">Yeni</span>
                </div>
            </div>-->

        </div>
        <?php endforeach; ?>

    </div>

</div>
<?php endif; ?>
</div>

<div class="container-fluid bg-white">
    <?php if(count($data['newPublicationList'])>0): ?>
        <div class="container">

            <h3 class="float-left gray-color"><b>Yeni Eklenen Ürünler</b></h3>
            <div class="clearfix d-block d-sm-none"></div>
            <div class="clearfix d-none d-sm-block d-md-none"></div>
            <!--<ul class="list-inline inline-menu float-md-right mt-2">
                <li class="list-inline-item">
                    <a href="#" class="">Yenü Ürünler</a>
                </li>
                <li class="list-inline-item">
                    <a href="#">Aktif Yolculuk</a>
                </li>
                <li class="list-inline-item">
                    <a href="#">Seçtiklerimiz</a>
                </li>
            </ul>-->

            <div class="clearfix"></div><hr class="bg-green">

            <div class="row">
                <div class="owl-carousel owl-theme">
                <?php foreach($data['newPublicationList'] as $publication): ?>
                    <div class="item">
                        <div class="card border-0 text-white shadow">
                            <img class="card-img" src="/images/product/<?=$publication['publication_photo_path'].".".$publication['publication_photo_extension']?>">
                            <div class="d-flex justify-content-center card-img-overlay p-0">

                                <div class="btn-toolbar align-items-end" role="toolbar" aria-label="Toolbar with button groups">
                                    <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                                        <button type="button" class="btn btn-light orange-color" onclick="location.href = '/product/detail/<?=$publication['publication_id']?>'">
                                            <?=$Caption['detail']?>
                                        </button>
                                        <button type="button" class="btn btn-light btn-block orange-color" onclick="globalJS.utility.AddBasket('/basket/add','basketTotal','id=<?=$publication['publication_id']?>&qty=1&sid=<?=$publication['publication_user_id']?>', '/basket/total-product')">
                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=$Caption['add_to_basket']?>
                                        </button>
                                        <a href="/images/product/<?=$publication['publication_photo_path'].".".$publication['publication_photo_extension']?>" class="btn btn-light orange-color" data-fancybox="">
                                            <i class="fa fa-search-plus" aria-hidden="true"></i>
                                        </a>
                                        <?php if(isset($_SESSION["userInfo"]["user_id"])): ?>
                                            <button type="button" class="btn btn-light orange-color" onclick="globalJS.utility.getPost('/product/add-favorite','-','id=<?=$publication['publication_id']?>','Favori Listesine Eklendi')">
                                                <i class="fa fa-heart" aria-hidden="true"></i>
                                            </button>
                                        <?php endif; ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <p class="m-0 pt-1 text-center"><?=$publication['publication_title']?></p>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>

        </div>
    <?php endif; ?>
</div>

<div class="container mt-3">

    <h3 class="float-left gray-color mt-5"><b><?=$Caption['what_is_buygosell']?> ? </b></h3>
    <div class="clearfix"></div><hr class="bg-green">

    <div class="row">
        <div class="col-12 col-md-4 text-center mb-3">
            <div class="card rounded-0 shadow border-0">
                <div class="card-body">
                    <h1>Buy</h1>
                    <img src="/images/site/<?=$data['company']['company_buy_image']?>" class="img-fluid my-3">
                    <p class="text-muted">
                        <small><?=$data['company']['company_buy_info']?></small>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4 text-center mb-3">
            <div class="card rounded-0 shadow border-0">
                <div class="card-body">
                    <h1>go</h1>
                    <img src="/images/site/<?=$data['company']['company_go_image']?>" class="img-fluid my-3">
                    <p class="text-muted">
                        <small><?=$data['company']['company_go_info']?></small>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4 text-center mb-3">
            <div class="card rounded-0 shadow border-0">
                <div class="card-body">
                    <h1>Sell</h1>
                    <img src="/images/site/<?=$data['company']['company_sell_image']?>" class="img-fluid my-3">
                    <p class="text-muted">
                        <small><?=$data['company']['company_sell_info']?></small>
                    </p>
                </div>
            </div>
        </div>
    </div>

</div>


<div class="container-fluid p-4" style="background-image: url('/images/bg-center.png')">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-9">
                <div class="card rounded-0 border-0 bg-green">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-lg-5 my-5 pl-5">
                                <h2 class="text-white"><?=$data['featured'][0]['featured_title']?></h2>
                                <p class="text-white"><?=$data['featured'][0]['featured_info']?></p>
                                <a href="<?=$data['featured'][0]['featured_link']?>" class="btn btn-outline-light btn-sm rounded-0"><?=$Caption['buy_now']?></a>
                            </div>
                            <div class="col-12 col-lg-7 mt-5">
                                <img src="/images/site/<?=$data['featured'][0]['featured_image']?>" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-3">
                <div class="card rounded-0 border-0 bg-orange">
                    <div class="card-body text-center">
                        <img src="/images/site/<?=$data['featured'][1]['featured_image']?>" class="img-fluid mx-auto d-block" style="height: 200px;">
                        <h6 class="text-white text-uppercase"><?=$data['featured'][1]['featured_title']?></h6>
                        <a href="<?=$data['featured'][1]['featured_link']?>" class="btn btn-outline-light btn-sm rounded-0 mb-2 mx-auto"><?=$Caption['buy_now']?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid p-0">
    <div class="row no-gutters">
        <?php $i=1; foreach($data['bigslider'] as $slider): ?>
            <?php if($i==1 || $i==2): $col=6; else: $col=4; endif; ?>
            <div class="col-md-<?=$col?>">
                <a href="<?=$slider['slider_link']?>">
                    <img src="/images/site/<?=$slider['slider_image']?>" class="img-fluid">
                </a>
            </div>
        <?php $i++; endforeach; ?>
    </div>
</div>

<!--
<div class="container-fluid d-none d-sm-block">
    <img src="/images/promo.png" class="img-fluid">
</div>-->

<div class="jumbotron jumbotron-fluid mb-5">
    <div class="container">
        <div class="row text-center">
            <div class="col-12 col-md-4 p-5 border border-secondary border-left-0 border-top-0">
                <img src="/images/icon/service1.png">
                <h3 class="orange-color my-2"><?=$Caption['live_support']?></h3>
                <p>Lorem ipsum dolor sit amet dolor sit amet lorem</p>
            </div>
            <div class="col-12 col-md-4 p-5 border border-secondary border-top-0 border-left-0">
                <img src="/images/icon/service2.png">
                <h3 class="orange-color my-2"><?=$Caption['follow_with_sms']?></h3>
                <p>Lorem ipsum dolor sit amet dolor sit amet lorem</p>
            </div>
            <div class="col-12 col-md-4 p-5 border border-secondary border-right-0 border-top-0 border-left-0">
                <img src="/images/icon/service3.png">
                <h3 class="orange-color my-2"><?=$Caption['cargo']?></h3>
                <p>Lorem ipsum dolor sit amet dolor sit amet lorem</p>
            </div>
            <div class="col-12 col-md-4 p-5 border border-secondary border-bottom-0 border-left-0 border-top-0">
                <img src="/images/icon/service4.png">
                <h3 class="orange-color my-2"><?=$Caption['service_quide']?></h3>
                <p>Lorem ipsum dolor sit amet dolor sit amet lorem</p>
            </div>
            <div class="col-12 col-md-4 p-5 border border-secondary border-left-0 border-top-0 border-bottom-0">
                <img src="/images/icon/service5.png">
                <h3 class="orange-color my-2"><?=$Caption['prohibited_products']?></h3>
                <p>Lorem ipsum dolor sit amet dolor sit amet lorem</p>
            </div>
            <div class="col-12 col-md-4 p-5">
                <img src="/images/icon/service6.png">
                <h3 class="orange-color my-2"><?=$Caption['warranty']?></h3>
                <p>Lorem ipsum dolor sit amet dolor sit amet lorem</p>
            </div>
        </div>
    </div>
</div>

<div class="container mb-5 d-none d-sm-block">
    <h3 class="float-left gray-color mt-5"><b><?=$Caption['beside_you']?></b></h3>
    <div class="clearfix"></div><hr class="bg-green">

    <div class="accordion">
        <ul>
            <?php $i=1; foreach($data['slider'] as $slider): ?>
                <li>
                    <div>
                        <a href="<?=$slider['slider_link']?>">
                            <h2><?=$slider['slider_title']?></h2>
                            <p><?=$slider['slider_info']?></p>
                        </a>
                    </div>
                </li>
            <?php $i++; endforeach; ?>
        </ul>
    </div>
</div>