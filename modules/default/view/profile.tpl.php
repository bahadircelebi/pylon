<div class="container my-3">
    <div class="row">
        <div class="col-md-2">
            <img src="/images/user.png" class="img-fluid shadow">
        </div>
        <div class="col-md-10">
            <h3 class="float-left mb-4">Kullanıcı Profil Sayfası</h3>
            <div class="clearfix"></div>
            <div class="row align-items-end text-center mt-5">
                <div class="col text-left">Roger Waters</div>
                <div class="col">
                    <h6>2+</h6>
                    <small>Yıldan Beri Üye</small>
                </div>
                <div class="col">
                    <h6>150+</h6>
                    <small>Ürün Satışı</small>
                </div>
                <div class="col">
                    <h6>$ 1,253,000</h6>
                    <small>Kazanç</small>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="container">
    <div class="jumbotron my-4">
        <div class="row">
            <div class="col-md-4">
                <div class="card bg-green border-0 rounded-0">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item bg-green text-white border-0 rounded-0">Profilim</li>
                            <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
                                Kişisel Bilgiler
                                <i class="fa fa-user fa-lg float-right" aria-hidden="true"></i>
                            </li>
                            <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
                                Email
                                <i class="fa fa-envelope-o fa-lg float-right" aria-hidden="true"></i>
                            </li>
                            <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
                                Telefon
                                <i class="fa fa-phone fa-lg float-right" aria-hidden="true"></i>
                            </li>
                            <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
                                Parola Güncelleme
                                <i class="fa fa-unlock-alt fa-lg float-right" aria-hidden="true"></i>
                            </li>
                            <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
                                Kişilerim
                                <i class="fa fa-users fa-lg float-right" aria-hidden="true"></i>
                            </li>
                            <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">Ürünlerim</li>
                            <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">Getirdiklerim</li>
                            <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">Siparişlerim</li>
                            <li class="list-group-item bg-green text-white border-0 rounded-0 white-top-border">
                                Çıkış
                                <i class="fa fa-sign-out fa-lg float-right" aria-hidden="true"></i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <h4 class="orange-color">Kişisel Bilgiler</h4>
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control green-border rounded-0" name="username" placeholder="Kullanıcı Adı">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control green-border rounded-0" name="fullname" placeholder="İsim veya Soyisim">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control green-border rounded-0" name="birthday" placeholder="Doğum Günü">
                    </div>
                    <div class="custom-control custom-radio custom-control-inline mb-3">
                        <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                        <label class="custom-control-label" for="customRadioInline1">Bay</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline mb-3">
                        <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                        <label class="custom-control-label" for="customRadioInline2">Bayan</label>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control green-border rounded-0" name="homephone" placeholder="Ev Telefonu">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control green-border rounded-0" name="officephone" placeholder="İş Telefonu">
                    </div>
                    <div class="form-group">
                        <select class="form-control green-border rounded-0">
                            <option>Ülke</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-warning bg-orange rounded-0 orange-border text-white">Güncelle</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
