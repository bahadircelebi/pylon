<?php 
$UserManager = new \defaultspace\UserManager();
$data['user'] = $UserManager->GetUserInfo();
$data['sell'] = $UserManager->GetSellOrder();
global $Config;
?>
<!doctype html>
<html lang="en">
<head>
    <?php Application::RenderView("global/header", "default", $data); ?>
</head>
<body>
<?php Application::RenderView("global/navbar", "default", $data); ?>

<div class="container my-4">
    <div class="row">
        <div class="col-12 col-md-2">
            <?php if(file_exists($Config['system']['user_path'].$data['user']['user_photo']) && $data['user']['user_photo']!=''): ?>
                <img src="/images/user/<?=$data['user']['user_photo']?>" class="img-fluid d-block mx-auto shadow">
            <?php else: ?>
                <img src="/images/icon/user.png" class="img-fluid d-block mx-auto shadow bg-white">
            <?php endif; ?>
        </div>
        <div class="col-12 col-md-10">
            <h3 class="text-center text-sm-left mb-4">Kullanıcı Profil Sayfası</h3>
            <div class="clearfix"></div>
            <div class="row align-items-end text-center mt-5 p-3">
                <div class="col-12 col-md-3 text-center text-sm-left p-3"><?=$data['user']['user_first_name']." ".$data['user']['user_last_name']?></div>
                <div class="col-4 col-md-3 p-3">
					<?php
						$diff = intval(abs(time() - strtotime($data['user']['user_register_date'])));

						$yil = floor($diff / (365*60*60*24));
						if($yil > 0):
							echo "<h6>".$yil."+</h6> <small>Yıldan Beri Üye</small>";
						else:
							$ay = floor(($diff - $yil * 365*60*60*24) / (30*60*60*24));
							echo "<h6>".$ay."+</h6> <small>Aydan Beri Üye</small>";
						endif;
					?>
                </div>
                <div class="col-4 col-md-3 p-3">
                    <?php $totalProduct = count($data['sell']); ?>
                    <h6><?=$totalProduct?>+</h6>
                    <small>Ürün Satışı</small>
                </div>
                <div class="col-4 col-md-3 p-3">
                    <?php $totalPrice = 0; foreach ($data['sell'] as $sell):
                            $totalPrice += $sell['order_detail_publication_price'];
                        endforeach;
                    ?>
                    <h6>$ <?=$totalPrice?></h6>
                    <small>Kazanç</small>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="container my-3">
    <div class="jumbotron rounded-0 bg-soft-gray shadow p-4" style="min-height: 640px; overflow: hidden;">
        <div class="row no-gutters">

            <?php Application::RenderView("profile/profile.menu", "default", $data); ?>

            <div class="col-lg-8 pl-lg-3">
				<a id="menu" href="#menu" class="btn btn-primary btn-sm float-right rounded-0 bg-orange orange-border text-white d-lg-none" data-toggle="offcanvas">Menü</a>
                <div class="clearfix"></div>
				<?php echo $data["PYLON_VIEW"]; ?>
               
            </div>
        </div>
    </div>
</div>	

<div class="global-message sh-1" style="bottom: 32px !important;"></div>

<?php Application::RenderView("global/footer", "default", $data); ?>

</body>
</html>
