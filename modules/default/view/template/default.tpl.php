<!doctype html>
<html lang="en">
<head>
    <?php Application::RenderView("global/header", "default", $data); ?>
</head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/tr_TR/sdk.js#xfbml=1&version=v3.0';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<?php Application::RenderView("global/navbar", "default", $data); ?>

<?php echo $data["PYLON_VIEW"]; ?>

<div class="global-message sh-1" style="bottom: 32px !important;"></div>

<?php Application::RenderView("global/footer", "default", $data); ?>

</body>
</html>
