<?php
global $Caption;

$BasketManager = new defaultspace\BasketManager();
$CategoryManager = new defaultspace\CategoryManager();
$categoryList = $CategoryManager->GetCategoryInfo();
$totalProduct = $BasketManager->GetBasketTotalPubliction();
?>
<div class="container my-3">
    <div class="row">
        <div class="col-6 col-md-3 col-lg-3">
            <a href="/"><img src="/images/logo.png" class="img-fluid"></a>
        </div>
        <div class="col-12 col-lg-5 my-1 d-none d-lg-block d-lg-block">
            <form id="search-form" method="get" action="/product/search">
                <div class="input-group">
                    <select name="q" class="custom-select" id="inputGroupSelect04">
						<option value="0" selected><?=$Caption["all_categories"]?></option>
						<?php foreach($categoryList as $category): ?>
                            <?php if($_SESSION['lang']=='tr'): ?>
                                <option value="<?=$category['category_id']?>"><?=$category['category_name']?></option>
                            <?php else: ?>
                                <option value="<?=$category['category_id']?>"><?=$category['category_name_en']?></option>
                            <?php endif; ?>
						<?php endforeach; ?>
                    </select>
                    <div class="input-group-append">
                        <input name="t" id="search" type="text" class="form-control rounded-0 rounded-right" placeholder="<?=$Caption["search"]?>">
                    </div>
                </div>
            </form>
        </div>
        <div class="col-12 col-md-9 col-lg-4">
            <ul class="list-inline mt-1 float-md-right">
                <li class="list-inline-item">
                    <?php if(isset($_SESSION['userInfo']['user_id'])): ?>
                        <a href="/profile" class="orange-color"></a>
						<div class="dropdown show" style="z-index: 1100;">
						  <a class="btn btn-link btn-sm dropdown-toggle orange-color" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<?=$_SESSION['userInfo']['user_first_name']." ".$_SESSION['userInfo']['user_last_name']?>
						  </a>

						  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
							<a class="dropdown-item" href="/product/seller"><?=$Caption["publication"]?></a>
							<a class="dropdown-item" href="/profile"><?=$Caption["personel_info"]?></a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="/user/logout"><?=$Caption["logout"]?></a>
						  </div>
						</div>
                    <?php else: ?>
                        <i class="fa fa-user orange-color" aria-hidden="true"></i>
                        <a href="/user/login" class="orange-color"><?=$Caption["login"]?></a>
                    <?php endif; ?>
                </li>
                <li class="list-inline-item dropdown" style="z-index: 1100;">
                    <!--<button class="btn btn-link dropdown-toggle btn-sm gray-color" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        USD
                    </button>-->
                    <div class="dropdown-menu dropdown-menu-right">
                        <button class="dropdown-item" type="button">EUR</button>
                    </div>
                </li>
                <li class="list-inline-item dropdown" style="z-index: 1100;">
                    <button class="btn btn-link dropdown-toggle btn-sm gray-color" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       <?php if($_SESSION['lang']=='tr'): ?>
                           TR
                       <?php else: ?>
                           EN
                       <?php endif; ?>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <?php if($_SESSION['lang']=='tr'): ?>
                            <button class="dropdown-item" onclick="location.href='?lang=en'" type="button">EN</button>
                        <?php else: ?>
                            <button class="dropdown-item" onclick="location.href='?lang=tr'" type="button">TR</button>
                        <?php endif; ?>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<nav class="navbar navbar-expand-lg navbar-light sticky-top main-menu bg-green text-white p-0">
    <div class="container">
        <a href="/basket/shopping-cart" class="navbar-toggler border-0 py-2 text-white">
            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            <small><?=$Caption["my_basket"]?> (<span id="basketTotalMobil"><?=$totalProduct?></span>)</small>
        </a>
        <button class="navbar-toggler border-0 py-2 ml-auto" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav mr-auto">
				<?php $link = explode("/",$_SERVER['REQUEST_URI']);?>
                <a class="nav-item nav-link px-4 py-3 <?=($link[1]=='' ? "active":"")?>" href="/"> <?=$Caption["home"]?> <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link px-4 py-3 <?=($link[1]=='category' ? "active":"")?>" href="/category"><?=$Caption["categories"]?></a>
                <a class="nav-item nav-link px-4 py-3 <?=($link[1]=='best' ? "active":"")?>" href="/best"><?=$Caption["best"]?></a>
                <a class="nav-item nav-link px-4 py-3 <?=($link[2]=='buyer' ? "active":"")?>" href="/product/buyer"><?=$Caption["buyer"]?></a>
                <a class="nav-item nav-link px-4 py-3 <?=($link[2]=='seller' ? "active":"")?>" href="/product/seller"><?=$Caption["seller"]?></a>
                <a class="nav-item nav-link px-4 py-3 <?=($link[1]=='about' ? "active":"")?>" href="/about"><?=$Caption["about"]?></a>
				<a class="nav-item nav-link px-4 py-3 <?=($link[1]=='contact' ? "active":"")?>" href="/contact"><?=$Caption["contact"]?></a>
				<a class="nav-item nav-link px-4 py-3 bg-orange <?=($link[1]=='seller' ? "active":"")?>" href="/product/seller"><?=$Caption["publication"]?></a>
            </div>
            <span class="navbar-text text-white d-none d-lg-block dxl-block">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                <small>
                    <a href="/basket/shopping-cart" class="text-white">
                        <?=$Caption["my_basket"]?> (<span id="basketTotal"><?=$totalProduct?></span>)
                    </a>
                </small>
            </span>
        </div>
    </div>
</nav>
<div class="col-12 p-0 d-md-block d-lg-none">
    <form id="search-form" method="get" action="/product/search">
        <div class="input-group">
            <select name="q" class="custom-select" id="inputGroupSelect04">
                <option value="0" selected><?=$Caption["all_categories"]?></option>
                <?php foreach($categoryList as $category): ?>
                    <?php if($_SESSION['lang']=='tr'): ?>
                        <option value="<?=$category['category_id']?>"><?=$category['category_name']?></option>
                    <?php else: ?>
                        <option value="<?=$category['category_id']?>"><?=$category['category_name_en']?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            </select>
            <div class="input-group-append">
                <input name="t" id="search" type="text" class="form-control rounded-0 rounded-right" placeholder="Arama">
            </div>
        </div>
    </form>
</div>