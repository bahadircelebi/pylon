<?php
global $Caption;
$IndexManager = new \defaultspace\IndexManager();
$contact = $IndexManager->Contact();
$CategoryManager = new \defaultspace\CategoryManager();
$categoryList = $CategoryManager->GetCategoryInfo();
?>
<footer class="py-5 d-none d-sm-block" style="background-image: url('/images/shopping-bg.jpg'); background-size: cover;">
    <div class="container my-5">
        <div class="row">
            <div class="col-6 col-md-3 my-5">
                <ul class="list-unstyled">
                    <li><b class="green-color text-uppercase"><?=$Caption["categories"]?></b></li>
					<?php foreach($categoryList as $category): ?>
                        <li>
                            <a href="/category?q=<?=$category['category_id']?>" class="black-color">
                                <?php if($_SESSION['lang']=='tr'): ?>
                                    <?=$category['category_name']?>
                                <?php else: ?>
                                    <?=$category['category_name_en']?>
                                <?php endif; ?>
                            </a>
                        </li>
					<?php endforeach; ?>
                </ul>
            </div>
            <div class="col-6 col-md-3 my-5">
                <ul class="list-unstyled">
                    <li><b class="green-color text-uppercase"><?=$Caption["information"]?></b></li>
                    <li><a href="/delivery" class="black-color"><?=$Caption["delivery"]?></a></li>
                    <li><a href="/legal-warning" class="black-color"><?=$Caption["legal_warning"]?></a></li>
                    <li><a href="/empty" class="black-color"><?=$Caption["terms_conditions"]?></a></li>
                    <li><a href="/contact" class="black-color"><?=$Caption["about"]?></a></li>
                    <li><a href="/empty" class="black-color"><?=$Caption["secure_payment"]?></a></li>
                    <li><a href="/empty" class="black-color"><?=$Caption["page_conditions"]?></a></li>
                    <li><a href="/empty" class="black-color"><?=$Caption["warranty"]?></a></li>
                    <li><a href="/empty" class="black-color"><?=$Caption["faq"]?></a></li>
                    <li><a href="/empty" class="black-color"><?=$Caption["support"]?></a></li>
                    <li><a href="/empty" class="black-color"><?=$Caption["site_map"]?></a></li>
                    <li><a href="/empty" class="black-color"><?=$Caption["shop"]?></a></li>
                </ul>
            </div>
            <div class="col-6 col-md-3 my-5">
                <ul class="list-unstyled">
                    <li><b class="green-color text-uppercase"><?=$Caption["your_account"]?></b></li>
                    <li><a href="/profile" class="black-color"><?=$Caption["personel_information"]?></a></li>
                    <li><a href="/profile/address" class="black-color"><?=$Caption["addresses"]?></a></li>
                    <li><a href="/profile/buy" class="black-color"><?=$Caption["my_orders"]?></a></li>
                    <li><a href="/profile/message" class="black-color"><?=$Caption["messages"]?></a></li>
                </ul>
            </div>
            <div class="col-6 col-md-3 my-5">
                <ul class="list-unstyled">
                    <li><b class="green-color text-uppercase"><?=$Caption["contact"]?></b></li>
                    <li><?=$contact['company_name']?></li>
                    <li><?=$contact['company_country']?></li>
                    <li><?=$contact['company_telephone']?></li>
                    <li><?=$contact['company_mail']?></li>
					<li>
						<a style="color: #343a40" href="<?=$contact['company_facebook_url']?>" target="_blank">
							<i class="fa fa-facebook-square fa-lg" aria-hidden="true"></i>
						</a>
                        <a style="color: #343a40" href="<?=$contact['company_linkedin_url']?>" target="_blank">
						<i class="fa fa-linkedin-square fa-lg" aria-hidden="true"></i>
                        </a>
						<a style="color: #343a40" href="<?=$contact['company_instagram_url']?>" target="_blank">
							<i class="fa fa-instagram fa-lg" aria-hidden="true"></i>
						</a>
                        <a style="color: #343a40" href="<?=$contact['company_google_url']?>" target="_blank">
                        <i class="fa fa-google-plus fa-lg" aria-hidden="true"></i>
                        </a>
					</li>
                    <li class="py-1">
                        <a href="<?=$contact['company_app_store_url']?>" target="_blank">
                            <img src="/images/download_app_store.svg" class="img-fluid">
                        </a>
                    </li>
                    <li class="py-1">
                        <a href="<?=$contact['company_google_play_url']?>" target="_blank">
                            <img src="/images/google-play-download.png" class="img-fluid" width="120px">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<div class="container-fluid bg-green text-center py-2">
    <small>Designed by Web Mobil Yazilim</small>
</div>