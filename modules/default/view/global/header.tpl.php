<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<?php if(isset($data['og_data'])): ?>
<meta property="og:url"           content="<?=$data['og_data']['url']?>"/>
<meta property="og:type"          content="website"/>
<meta property="og:title"         content="<?=$data['og_data']['title']?>"/>
<meta property="og:description"   content="<?=$data['og_data']['description']?>"/>
<meta property="og:image"         content="<?=$data['og_data']['image']?>"/>
<?php endif; ?>

<!-- Bootstrap CSS -->
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:600&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
<link rel="stylesheet" href="/css/xzoom.css">
<link rel="stylesheet" href="/css/jquery.raty.css">
<link rel="stylesheet" href="/css/jquery.fancybox.min.css">
<link href="/css/offcanvas.css" rel="stylesheet">
<link href="/js/assets/owl.carousel.min.css" rel="stylesheet">
<link href="/js/assets/owl.theme.default.min.css" rel="stylesheet">
<link rel="stylesheet" href="/css/main.css">
<style>
	body {
		font-family: 'Open Sans', sans-serif !important;
		font-size: 14px !important;
	}
</style>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="/js/jquery.fancybox.min.js"></script>
<script src="/js/xzoom.js"></script>
<script src="/js/jquery.raty.js"></script>
<script src="/js/offcanvas.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/global.js"></script>
<script>
    $( document ).ready(function() {

        <?php if(isset($data['msg'])): ?>
        globalJS.messages.globalMessage('<?=$data['msg']?>',3);
        <?php endif; ?>

        $( function() {
            $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
        });

        $(function () {
            $('[data-toggle="popover"]').popover(
                {html:true}
            )
        })
		
		$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		})

         $('div.star').raty({
             half: false,
             starHalf : '/images/icon/star-half-mono.png',
             click: function(score, evt) {
                 var id = $(this).attr('id')
                 $.ajax({
                     type: "POST",
                     url: "/product/publication-point",
                     data: 'pid='+id+"&score="+score,
                     success: function(msg){
                         $( "#" + id ).html( msg );
                     }
                 });
             }
         });

        $('div.order-star').raty({
            half: false,
            starHalf : '/images/icon/star-half-mono.png',
            click: function(score, evt) {
                var id = $(this).attr('id')
                $.ajax({
                    type: "POST",
                    url: "/profile/order-point",
                    data: 'pid='+id+"&score="+score,
                    success: function(msg){
                        $( "#" + id ).html( msg );
                    }
                });
            }
        });

         $('#search').keydown(function() {
            var key = e.which;
            if (key == 13) {
                $('#search-form').submit(); // Submit form code
            }
         });

         // Example starter JavaScript for disabling form submissions if there are invalid fields
         (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
         })();

        var owl = $('.owl-carousel');
        owl.owlCarousel({
            items: 4,
            loop: true,
            stagePadding: 15,
            margin: 30,
            autowidth: true,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: true
        });

    });
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-71796881-2"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-71796881-2');
</script>

<script src='https://www.google.com/recaptcha/api.js'></script>
<title>Buygosell</title>