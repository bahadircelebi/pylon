<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/tr_TR/sdk.js#xfbml=1&version=v2.12';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div class="container my-5">
    <div class="row no-gutters shadow mx-2 bg-white">
        <div class="col-lg-6">
            <img src="https://images.unsplash.com/photo-1483181994834-aba9fd1e251a?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=af45d8f2523730b421a67048b9fae59a&auto=format&fit=crop&w=1050&q=80" class="img-fluid">
        </div>
        <div class="col-lg-6 p-5 align-self-center">
            <form action="/user/login" method="post" class="was-validated">
                <h3 class="mb-3">BuygoSell Giriş</h3>
                <div class="form-group">
                    <input class="form-control rounded-0 green-border" type="email" name="user_mail" placeholder="Kullanıcı Mail" required>
                </div>
                <div class="form-group">
                    <input class="form-control rounded-0 green-border" type="password" name="user_pass" placeholder="Kullanıcı Şifresi" required>
                </div>
                <div class="form-group">
                    <a href="/user/recovery" class="green-color">Şifremi Unuttum</a>
                    <a href="/user/register" class="green-color float-right">Hesap Oluştur</a>
                </div>
                <div class="form-group">
                    <button name="login" type="submit" class="btn btn-warning bg-orange rounded-0 orange-border text-white">Giriş Yap</button>
                </div>
                <div class="fb-login-button" data-max-rows="1" data-size="medium" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="true"></div>
            </form>
        </div>
    </div>
</div>