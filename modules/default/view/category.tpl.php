<?php $CategoryManager = new defaultspace\CategoryManager(); ?>
<?php $pcount = count($data['category']); ?>
<div class="container-fluid bg-white">
	<div class="container">
		<h3 class="float-left gray-color mt-5"><b>Buygosell Kategoriler </b></h3>
		<div class="clearfix"></div><hr class="bg-green">

        <div class="row main-categories">
            <?php $i=1; foreach($data['categoryMain'] as $category): ?>
                <div class="col text-center">
                    <a href="/category?q=<?=$category['category_id']?>">
                        <img src="/images/site/<?=$category['category_icon']?>">
                        <p class="gray-color">
                            <?php if($_SESSION['lang']=='tr'): ?>
                                <?=$category['category_name']?>
                            <?php else: ?>
                                <?=$category['category_name_en']?>
                            <?php endif; ?>
                        </p>
                    </a>
                </div>
                <?php $i++; endforeach; ?>
        </div>

	</div>
</div>

<?php if($pcount > 0): ?>
<div class="container my-4">
	<?php if($data['categoryInfo']['category_parent'] == 0 && isset($_GET['q']) && $_GET['q']!=0): ?>
		<div class="card bg-dark text-white rounded-0 border-0">
		  <img class="card-img" src="/images/site/<?=$data['categoryInfo']['category_picture']?>">
		  <div class="card-img-overlay d-flex">
			<h1 class="card-title white-color align-self-center">
				<?=$data['categoryInfo']['category_name']?>
			</h1>
		  </div>
		</div>
	<?php elseif(isset($_GET['q']) && $_GET['q']!=0): ?>
		<div class="card rounded-0 border-0">
			<div class="card-body">
				<h1 class="card-title white-color align-self-center">
					<?=$data['categoryInfo']['category_name']?>
				</h1>
			</div>
		</div>
	<?php endif; ?>
</div>
<?php endif; ?>

<div class="container mt-5">

<?php
	if($pcount > 0 && $_GET['q']!=0):
	if(isset($data["breadLink"])){
		if(count($data["breadLink"])>0){
			?>
	<nav aria-label="breadcrumb">
			<ol class="breadcrumb bg-soft-gray rounded-0">
			 <li class="breadcrumb-item"><a href="/">Anasayfa</a></li>
			<?php

			foreach ($data["breadLink"] as $linkData){
				$link = $linkData["category_id"];
				$title = $linkData["category_name"];
			  ?>
				<li class="breadcrumb-item"><a href="/category?q=<?=$link?>" title="<?=htmlentities($title)?>"><?=$title?></a></li>
			  <?php
			}
			?>
			</ol>
	</nav>
	<?php
    }
}
	endif;
?>

    <div class="row">
        <div class="col-md-4 col-lg-3 pt-3 d-none d-md-block">
			<?php if(count($data['subCategory'])>0): ?>
            <h6 class="bg-green text-white text-center p-2">İlgili Kategoriler</h6>
            <ul class="list-unstyled border green-border p-3">
                <?php foreach ($data['subCategory'] as $sub): ?>
                <li class="mb-2"><a href="/category?q=<?=$sub['category_id']?>"><?=$sub['category_name']?></a></li>
                <?php endforeach; ?>
            </ul>
			<?php endif; ?>

            <div class="border green-border p-3">
                <form method="get" class="was-walidated">
                    <?php if(count($data['categoryOption'])>0): ?>
                        <?php foreach($data['categoryOption'] as $Catoption): ?>

                            <?php $options = $CategoryManager->GetOptions($Catoption['option_id']); ?>
                            <h6><?=$options[0]['option_key']?></h6>

                            <?php foreach($options as $option): ?>
                                <div class="custom-control custom-checkbox">
                                    <input name="<?=$option['option_key_value']?>[]" type="checkbox" class="custom-control-input" id="customCheck<?=$option['option_detail_id']?>" value="<?=$option['option_value']?>">
                                    <label class="custom-control-label" for="customCheck<?=$option['option_detail_id']?>">
                                        <?=$option['option_value']?>
                                    </label>
                                </div>
                            <?php endforeach; ?>

                            <hr>

                        <?php endforeach; ?>
                    <?php endif; ?>

                    <h6><small>Hangi Ülkeden Ürün İstiyorsunuz ? </small></h6>
                    <select name="country" class="form-control form-control-sm green-border" required>
                        <option value="">Ülke</option>
                        <?php foreach($data['country'] as $country): ?>
                            <option value="<?=$country['id']?>"><?=$country['name']?></option>
                        <?php endforeach; ?>
                    </select>

                    <hr>

                    <input name="q" type="hidden" value="<?=$_GET['q']?>">
                    <button type="submit" class="btn btn-warning bg-orange btn-sm rounded-0 orange-border text-white float-right" value="">Gönder</button>
                    <div class="clearfix"></div>

               		</form>
				</div>

            <hr>

            <h6 class="bg-green text-white text-center p-2">Çok Satılan</h6>
            <ul class="list-unstyled border green-border p-3">
                <?php if(count($data['publicationList'])>0): ?>
				<?php foreach($data['publicationList'] as $publication): ?>
                <li class="mb-3 text-center">
                    <a href="/product/detail/<?=$publication['publication_id']?>">
                        <img src="/images/product/<?=$publication['publication_photo_path']."-150.".$publication['publication_photo_extension']?>" class="float-left border orange-border" width="80">
                    </a>
                    <small>
						<a href="/product/detail/<?=$publication['publication_id']?>" class="black-color">
							<?=$publication['publication_title']?>
						</a>
					</small><br>
                    <b><?=$publication['publication_price']." ".$publication['publication_currency']?></b><br>
                    <small>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </small>
                    <div class="clearfix"></div>
                </li>
				<?php endforeach; ?>
                <?php else: ?>
                    <li class="text-center">
                        <small>Liste Boş</small>
                    </li>
                <?php endif; ?>
            </ul>
            <!--
            <h6 class="bg-green text-white text-center p-2">Benzer Ürünler</h6>
            <ul class="list-unstyled border green-border p-3 text-center">
                <li class="text-center">
                    <small>Liste Boş</small>
                </li>
            </ul>-->
        </div>
        <div class="col-md-8 col-lg-9">
            <?php if( $pcount > 0): ?>
                <p class="float-left mt-4">
                    <small>Toplam <?=$pcount?> üründen 1 ve 6 arası</small>
                </p>
                <div class="btn-group btn-group-sm float-right py-3" role="group" aria-label="Button group with nested dropdown">
                    <button type="button" class="btn btn-secondary" onclick="globalJS.utility.getPostSerialize('/user/column','-','#list-column','')">
                        <i class="fa fa-th-list" aria-hidden="true"></i>
                        <input name="list_column" id="list-column" type="hidden" value="list">
                    </button>
                    <button type="button" class="btn btn-secondary" onclick="globalJS.utility.getPostSerialize('/user/column','-','#box-column','')">
                        <i class="fa fa-th" aria-hidden="true"></i>
                        <input name="box_column" id="box-column" type="hidden" value="box">
                    </button>
                    <input id="view-column" name="view_column" type="hidden" value="<?=$_SESSION["view_column"]?>">
                    <div class="btn-group" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Sayfa Başı Adet
                        </button>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                            <a class="dropdown-item" href="#">6 Adet</a>
                            <a class="dropdown-item" href="#">12 Adet</a>
                            <a class="dropdown-item" href="#">24 Adet</a>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <?php foreach($data['category'] as $publication): ?>
                        <?php if($_SESSION["view_column"]=='box'): ?>
                            <div class="col-md-6 col-lg-4 mb-3 text-center align-self-end">
                                <div class="card border-0 text-white shadow">
                                    <img class="card-img" src="/images/product/<?=$publication['publication_photo_path'].".".$publication['publication_photo_extension']?>">
                                    <div class="d-flex justify-content-center card-img-overlay p-0">

                                        <div class="btn-toolbar align-items-end" role="toolbar" aria-label="Toolbar with button groups">
                                            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                                                <button type="button" class="btn btn-light orange-color" onclick="location.href = '/product/detail/<?=$publication['publication_id']?>'">
                                                    Detay
                                                </button>
                                                <button type="button" class="btn btn-light btn-block orange-color" onclick="globalJS.utility.AddBasket('/basket/add','basketTotal','id=<?=$publication['publication_id']?>&qty=1&sid=<?=$publication['publication_user_id']?>', '/basket/total-product')">
                                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> Sepete Ekle
                                                </button>
                                                <a href="/images/product/<?=$publication['publication_photo_path'].".".$publication['publication_photo_extension']?>" class="btn btn-light orange-color" data-fancybox="">
                                                    <i class="fa fa-search-plus" aria-hidden="true"></i>
                                                </a>
                                                <?php if(isset($_SESSION["userInfo"]["user_id"])): ?>
                                                <button type="button" class="btn btn-light orange-color" onclick="globalJS.utility.getPost('/product/add-favorite','-','id=<?=$publication['publication_id']?>','Favori Listesine Eklendi')">
                                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                                </button>
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <p class="m-0 pt-1"><?=$publication['publication_title']?></p>
                                <p class="m-0 pt-1"><small><?=$publication['publication_price']." ".$publication['publication_currency']?> <!--<del class="green-color"> 333$</del>--></small></p>

                            </div>
                        <?php else: ?>

                            <div class="col-12">
                                <div class="row">
                                    <div class="col-3">
                                        <a href="/product/detail/<?=$publication['publication_id']?>">
                                            <img class="img-fluid border orange-border" src="/images/product/<?=$publication['publication_photo_path']."-150.".$publication['publication_photo_extension']?>">
                                        </a>
                                    </div>
                                    <div class="col-9">
                                        <p class="float-left">
                                            <a href="/product/detail/<?=$publication['publication_id']?>" class="black-color"><?=$publication['publication_title']?></a>
                                        </p>
                                        <span class="float-right"><?=$publication['publication_price']." ".$publication['publication_currency']?></span>


                                        <div class="clearfix"></div>

                                        <div class="btn-toolbar align-items-end float-right" role="toolbar" aria-label="Toolbar with button groups">
                                            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                                                <button type="button" class="btn btn-light orange-color" onclick="location.href = '/product/detail/<?=$publication['publication_id']?>'">
                                                    Detay
                                                </button>
                                                <button type="button" class="btn btn-light btn-block orange-color" onclick="globalJS.utility.AddBasket('/basket/add','basketTotal','id=<?=$publication['publication_id']?>&qty=1&sid=<?=$publication['publication_user_id']?>', '/basket/total-product')">
                                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> Sepete Ekle
                                                </button>
                                                <a href="/images/product/<?=$publication['publication_photo_path'].".".$publication['publication_photo_extension']?>" class="btn btn-light orange-color" data-fancybox="">
                                                    <i class="fa fa-search-plus" aria-hidden="true"></i>
                                                </a>
                                                <?php if(isset($_SESSION["userInfo"]["user_id"])): ?>
                                                    <button type="button" class="btn btn-light orange-color" onclick="globalJS.utility.getPost('/product/add-favorite','-','id=<?=$publication['publication_id']?>','Favori Listesine Eklendi')">
                                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                                    </button>
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="clearfix"></div><hr>
                            </div>

                        <?php endif; ?>

                    <?php endforeach; ?>
                </div>
            <?php else: ?>
                <div class="col-12 alert alert-warning mt-3">
                    <div class="h3 my-3 text-center">Aradığınız kategori ile eşleşen ürün bulunamadı.</div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>