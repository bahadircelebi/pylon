<div class="container">
    <h4 class="mt-5 mb-3">Ürün Yükleme Sayfası</h4>
    <div class="card border-0 rounded-0 shadow orange-lr-border mb-3">
        <div class="card-body">
            <div class="row text-center">
                <div class="col-md-4">
                    <img src="/images/user.png" class="img-fluid rounded-circle orange-border mr-3" width="50">
                    Roger Waters
                </div>
                <div class="col-md-2 gray-left-border">
                    <h6>Telefon</h6>
                    <small>+90 212 999 00 11</small>
                </div>
                <div class="col-md-2 gray-left-border">
                    <h6>Ülke</h6>
                    <small>Türkiye</small>
                </div>
                <div class="col-md-2 gray-left-border">
                    <h6>Ürün Satışı</h6>
                    <small>150+</small>
                </div>
                <div class="col-md-2 gray-left-border">
                    <h6>Kazanç</h6>
                    <small>$ 1,253,000</small>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container mt-5 pt-5">
    <div class="jumbotron rounded-0 bg-soft-gray shadow">
        <h4 class="orange-color mb-3">Ne Satmak İstiyorsun ?</h4>
        <div class="clearfix"></div>
        <form method="post">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <select class="form-control rounded-0 green-border">
                            <option>Kategori Seç</option>
                        </select>
                        Eklenti gelecek buraya
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control rounded-0 green-border" placeholder="Ürün Başlığı *">
                        <small>Minimum 10 karakter</small>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control rounded-0 green-border" rows="6">Ürün Açıklaması</textarea>
                        <small>Marka, model ve diğer özellikler</small>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control rounded-0 green-border" placeholder="Parça *">
                    </div>
                    <div class="form-group">
                        <select class="form-control rounded-0 green-border">
                            <option>Ürün Adı</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <small>Ürün Resimleri</small><br>
                        Eklenti gelecek buraya
                    </div>
                    <div class="form-group">
                        <select class="form-control rounded-0 green-border">
                            <option>Renk</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control rounded-0 green-border" placeholder="Model">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control rounded-0 green-border" placeholder="IMEI">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control rounded-0 green-border" placeholder="Price *">
                    </div>
                    <div class="form-group">
                        <input type="checkbox"> Sözleşmeyi Kabul Ediyorum
                    </div>
                    <div class="form-group">
                        <button class="btn btn-warning bg-orange rounded-0 orange-border text-white">Kaydet</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>