<div class="container my-5">
    <div class="row bg-white no-gutters shadow mx-2">
        <div class="col-lg-6">
            <img src="https://images.unsplash.com/photo-1513094735237-8f2714d57c13?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=e05a342ece1d031eb180935b3eb93826&auto=format&fit=crop&w=675&q=80" class="img-fluid">
        </div>
        <div class="col-lg-6 p-5 align-self-center">
            <?php if(isset($data['registerMsg']) && $data['registerMsg']=="OK"): ?>
                <img src="/images/logo.png" class="img-fluid mx-auto d-block mb-5">
                <h6 class="text-center">Üye kaydınız başarıyla alınmıştır. <br>Lütfen mail adresinize gönderilecek aktivasyon linkini tıklayarak üyeliğinizi aktif ediniz. <br><br> Keyifli alışverişler.</h6>
            <?php else: ?>
            <form action="/user/register" method="post" class="was-validated">
                <h3 class="mb-3">BuygoSell Kayıt</h3>
                <div class="form-group">
                    <input class="form-control rounded-0 green-border" type="text" name="user_first_name" placeholder="Ad" required>
                </div>
                <div class="form-group">
                    <input class="form-control rounded-0 green-border" type="text" name="user_last_name" placeholder="Soyad" required>
                </div>
                <div class="form-group">
                    <input class="form-control rounded-0 green-border" type="text" name="user_mail" placeholder="Mail Adresi" required>
                </div>
                <div class="form-group">
                    <input class="form-control rounded-0 green-border" type="password" name="user_pass" placeholder="Şifre" required>
                </div>
                <div class="form-group">
                    <input class="form-control rounded-0 green-border" type="password" name="user_repass" placeholder="Şifre Tekrar" required>
                </div>
                <div class="form-group">
                    <select name="user_country" class="form-control green-border" onchange="globalJS.utility.getPost('/user/get-state','state', 'id='+this.value,'')" required>
                        <option>Ülkeler</option>
                        <?php foreach($data['country'] as $country): ?>
                            <option value="<?=$country['id']?>"><?=$country['name']?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <div id="state"></div>
                </div>
                <div class="form-group">
                    <div id="city"></div>
                </div>
                <div class="form-group">
                    <div class="g-recaptcha" data-sitekey="6LdWsWEUAAAAAAAoXx0ge8KQykujWLSOFTYTGWmq"></div>
                </div>
                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customCheck1" required>
                        <label class="custom-control-label" for="customCheck1">
                            <small>Kayıt olmak için yandaki kutuyu tıklayarak Hizmet Şartları'nı kabul etmiş olursunuz.</small>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <button name="login" type="submit" class="btn btn-warning bg-orange rounded-0 orange-border text-white">Kayıt Ol</button>
                    <a href="/user/login" class="btn btn-warning bg-orange rounded-0 orange-border text-white">Giriş Yap</a>
                </div>
            </form>
            <?php endif; ?>
        </div>
    </div>
</div>