<div class="container-fluid p-0">
    <img src="/images/product-bg.jpg" class="img-fluid">
</div>

<div class="container my-5">
    <div class="card rounded-0 my-4 shadow">
        <div class="card-body px-5">
            <div class="input-group">
                <input type="text" class="form-control rounded-0" placeholder="Ne Satmak İstiyorsun ?">
                <div class="input-group-append">
                    <button class="btn btn-warning bg-orange rounded-0 orange-border text-white">Hızlı Arama</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-3">
                <div class="card rounded-0 green-border">
                    <div class="card-body">
                        <form method="post" action="/product/upload-product">
                            <div class="form-group">
                                <h5><small>Nereden Geliyorsun ?</small></h5>
                                <select name="from_country" class="form-control green-border">
                                    <option>Ülke</option>
                                    <?php foreach($data['country'] as $country): ?>
                                    <option value="<?=$country['country_id']?>"><?=$country['country_title']?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <h5><small>Nereye Gidiyorsun ?</small></h5>
                                <select name="to_country" class="form-control green-border">
                                    <option>Ülke</option>
                                    <?php foreach($data['country'] as $country): ?>
                                        <option value="<?=$country['country_id']?>"><?=$country['country_title']?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <h5><small>Ne Zaman Gidiyorsun ?</small></h5>
                                <input type="text" class="form-control rounded-0 green-border" placeholder="Tarih">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-warning bg-orange rounded-0 orange-border text-white">Kaydet</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="card rounded-0 green-border">
                    <div class="card-body">
                        <div class="form-group">
                            <h5><small>Hangi Ülkeden Ürün İstiyorsunuz ? </small></h5>
                            <input type="text" class="form-control rounded-0 green-border" placeholder="Ülke">
                        </div>
                        <div class="form-group">
                            <h5><small>Ne Almak İstiyorsun ?</small></h5>
                            <input type="text" class="form-control rounded-0 green-border" placeholder="Ürün">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-warning bg-orange rounded-0 orange-border text-white">Kaydet</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <h3 class="float-left gray-color mt-5"><b>Neye İhtiyacın Var?</b></h3>
    <div class="clearfix"></div><hr class="bg-green">

    <div class="row">
        <div class="col-md-7">
            <h6 class="mb-3">
                <small>En son açılan puanlar üst seviye satıcılar olabilir</small>
            </h6>
            <div class="row mb-4">
                <div class="col-6 col-md-3">
                    <img src="/images/item1.png" class="img-fluid">
                    <div class="text-center border orange-border orange-color py-1">
                        <small>Antik Takılar</small>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <img src="/images/item2.png" class="img-fluid">
                    <div class="text-center border orange-border orange-color py-1">
                        <small>Cüzdanlar</small>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <img src="/images/item3.png" class="img-fluid">
                    <div class="text-center border orange-border orange-color py-1">
                        <small>Makyaj Malzemesi</small>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <img src="/images/item4.png" class="img-fluid">
                    <div class="text-center border orange-border orange-color py-1">
                        <small>Ceketler</small>
                    </div>
                </div>
            </div>

            <h6 class="mb-3">
                <small>Çok Satanlar</small>
            </h6>

            <div class="row">
                <div class="col-6 col-md-3">
                    <img src="/images/item1.png" class="img-fluid">
                    <div class="text-center border orange-border orange-color py-1">
                        <small>Antik Takılar</small>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <img src="/images/item2.png" class="img-fluid">
                    <div class="text-center border orange-border orange-color py-1">
                        <small>Cüzdanlar</small>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <img src="/images/item3.png" class="img-fluid">
                    <div class="text-center border orange-border orange-color py-1">
                        <small>Makyaj Malzemesi</small>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <img src="/images/item4.png" class="img-fluid">
                    <div class="text-center border orange-border orange-color py-1">
                        <small>Ceketler</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <img src="/images/itembig.png" class="img-fluid">
        </div>
    </div>

    <h3 class="float-left gray-color mt-5"><b>Son Satışlar</b></h3>
    <div class="clearfix"></div><hr class="bg-green">

    <div class="row">
        <div class="col-md-6 col-lg-4 mb-3 text-center align-self-end">
            <div class="card border-0 text-white shadow">
                <img class="card-img" src="/images/model1.png">
                <div class="d-flex justify-content-center card-img-overlay p-0">

                    <div class="btn-toolbar align-items-end" role="toolbar" aria-label="Toolbar with button groups">
                        <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                            <button type="button" class="btn btn-light orange-color" onclick="location.href = '/product/detail'">
                                Detay
                            </button>
                            <button type="button" class="btn btn-light btn-block orange-color" onclick="globalJS.utility.AddBasket('/basket/add','basketTotal','id=1&qty=1', '/basket/total-product')">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i> Sepete Ekle
                            </button>
                            <button type="button" class="btn btn-light orange-color">
                                <i class="fa fa-search-plus" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-light orange-color">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>

                </div>
            </div>
            <p class="m-0 pt-1"><small>Small Shirt Dress With Small Laces</small></p>
            <p class="m-0 pt-1"><small>234$ <del class="green-color"> 333$</del></small></p>
            <p>
                <small>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    By: Fajar Accessories</small>
            </p>
        </div>
        <div class="col-md-6 col-lg-4 mb-3 text-center">
            <div class="card border-0 text-white shadow">
                <img class="card-img" src="/images/model2.png">
                <div class="d-flex justify-content-center card-img-overlay p-0">

                    <div class="btn-toolbar align-items-end" role="toolbar" aria-label="Toolbar with button groups">
                        <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                            <button type="button" class="btn btn-light orange-color" onclick="location.href = '/product/detail'">
                                Detay
                            </button>
                            <button type="button" class="btn btn-light btn-block orange-color" onclick="globalJS.utility.AddBasket('/basket/add','basketTotal','id=1&qty=1', '/basket/total-product')">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i> Sepete Ekle
                            </button>
                            <button type="button" class="btn btn-light orange-color">
                                <i class="fa fa-search-plus" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-light orange-color">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>

                </div>
            </div>
            <p class="m-0 pt-1"><small>Small Shirt Dress With Small Laces</small></p>
            <p class="m-0 pt-1"><small>234$ <del class="green-color"> 333$</del></small></p>
            <p>
                <small>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    By: Fajar Accessories</small>
            </p>
        </div>
        <div class="col-md-6 col-lg-4 mb-3 text-center">
            <div class="card border-0 text-white shadow">
                <img class="card-img" src="/images/model3.png">
                <div class="d-flex justify-content-center card-img-overlay p-0">

                    <div class="btn-toolbar align-items-end" role="toolbar" aria-label="Toolbar with button groups">
                        <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                            <button type="button" class="btn btn-light orange-color" onclick="location.href = '/product/detail'">
                                Detay
                            </button>
                            <button type="button" class="btn btn-light btn-block orange-color" onclick="globalJS.utility.AddBasket('/basket/add','basketTotal','id=1&qty=1', '/basket/total-product')">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i> Sepete Ekle
                            </button>
                            <button type="button" class="btn btn-light orange-color">
                                <i class="fa fa-search-plus" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-light orange-color">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>

                </div>
            </div>
            <p class="m-0 pt-1"><small>Small Shirt Dress With Small Laces</small></p>
            <p class="m-0 pt-1"><small>234$ <del class="green-color"> 333$</del></small></p>
            <p>
                <small>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    By: Fajar Accessories</small>
            </p>
        </div>
    </div>

</div>

<!-- Modal Fullscreen -->
<div class="modal fade modal-fullscreen" id="modal-fullscreen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
                <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
                <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>