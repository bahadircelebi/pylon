<?php $UserManager = new \defaultspace\UserManager(); ?>
<script>
    $( document ).ready(function() {
        $('.xzoom, .xzoom-gallery').xzoom({zoomWidth: 555, title: true, tint: '#333', Xoffset: 15, scroll: true});
    });
</script>

<div class="container my-5">

    <?php if($data['publication']['publication_status']=="P" && isset($_SESSION["userInfo"]["user_id"])): ?>
    <div class="alert alert-info rounded-0 mb-5" role="alert">
        <h4 class="alert-heading"><i class="fa fa-info-circle" aria-hidden="true"></i> Ürün Onay Sürecinde</h4>
        <p>İlanınız kalite kontrol ekiplerimiz tarafından inceleme sürecindedir. Onaylandıktan sonra ilan yayına alınacaktır. Sabrınız için teşekkürler.</p>
        <hr>
        <p class="mb-0">Bu sayfa sadece size özel olarak oluşturulmuştur.</p>
    </div>
    <?php endif; ?>

    <?php if($data['publication']['publication_status']=="P" && isset($_SESSION["adminInfo"]["admin_id"])): ?>
        <div class="alert alert-warning rounded-0 mb-5" role="alert">
            <h4 class="alert-heading"><i class="fa fa-info-circle" aria-hidden="true"></i> Ürünü Onaylama</h4>
            <p>Sayın Yönetici lütfen bilgilerin doğruluğunu kontrol edip onaylayınız.</p>
            <a href="/product/confirm/<?=$data['publication']['publication_id']?>" class="btn btn-outline-success float-right rounded-0">Onayla</a>
            <div class="clearfix"></div>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-12 col-md-5">
            <img class="xzoom img-fluid" src="/images/product/<?=$data['publication_photo']['0']['publication_photo_path'].".".$data['publication_photo']['0']['publication_photo_extension']?>" xoriginal="/images/product/<?=$data['publication_photo']['0']['publication_photo_path'].".".$data['publication_photo']['0']['publication_photo_extension']?>" />
		</div>
		<div class="col-md-1 order-first">
				<div class="row no-gutter">
                <?php $i=1; foreach($data['publication_photo'] as $photo): ?>
                <div class="col-3 col-md-12">
                    <div class="xzoom-thumbs">
                        <a href="/images/product/<?=$photo['publication_photo_path'].".".$photo['publication_photo_extension']?>">
                            <img class="xzoom-gallery img-fluid" src="/images/product/<?=$photo['publication_photo_path']."-150.".$photo['publication_photo_extension']?>"  xpreview="/images/product/<?=$photo['publication_photo_path'].".".$photo['publication_photo_extension']?>">
                        </a>
                    </div>
                </div>
                <?php $i++; endforeach; ?>
				</div>
		</div>

        <div class="col-12 col-md-6">

            <div class="bg-white border green-border p-3">
                <h4><?=$data['publication']['publication_title']?></h4>
                <p class="pt-2 text-justify"><?=$data['publication']['publication_description']?></p>
                <hr>

                <?php
                $point = $UserManager->GetUserPublicationPoint($data['publication']['publication_id']);
                if(count($point)>0):
                ?>

                    <script>$( document ).ready(function() { $('#<?=$data['publication']['publication_id']?>').raty({ readOnly: true, score: <?=$data['publication_point']?> }); });</script>
                    <h6><small>Ürün Puanı</small></h6>
                    <div class="float-left" id="<?=$data['publication']['publication_id']?>"></div>

                <?php else: ?>

                    <small class="float-left">Ürüne Puan Verin</small><br>
                    <div class="star float-left" id="<?=$data['publication']['publication_id']?>"></div>

                <?php endif; ?>

                <h2 class="text-right orange-color"><?=$data['publication']['publication_price']?> <?=$data['publication']['publication_currency']?></h2>
            </div>

            <div class="clearfix my-3"></div>

            <div class="bg-white border green-border p-3">
                <div class="fb-share-button float-right"
                     data-href=""
                     data-layout="button_count">
                </div>

                <div class="float-left">
                    <h6><small>Kategori</small></h6>
                    <h6><?=$data['publication']['category_name']?></h6>
                </div>

                <div class="clearfix"></div>

                <div class="row my-3">
                    <div class="col-4">
                        <h6><small>Hangi Ülkeden</small></h6>
                        <h6><?=$data['from_country']['name']?></h6>
                    </div>
                    <div class="col-2">
                        <i class="fa fa-share" aria-hidden="true"></i>
                    </div>
                    <div class="col-4">
                        <h6><small>Hangi Ülkeye</small></h6>
                        <h6><?=$data['to_country']['name']?></h6>
                    </div>
                </div>

                <div class="row my-3">
                    <div class="col-6">
                        <h6><small>Başlangıç Tarihi</small></h6>
                        <h6><?=$data['publication']['publication_start_date']?></h6>
                    </div>
                    <div class="col-6">
                        <h6><small>Bitiş Tarihi</small></h6>
                        <h6><?=$data['publication']['publication_end_date']?></h6>
                    </div>
                </div>

                <div class="row my-3">
                <?php foreach($data['publicationOption'] as $option): ?>
                    <div class="col-4 col-md-3">
                        <h6><small><?=$option['option_key']?></small></h6>
                        <h6><?=$option['option_value']?></h6>
                    </div>
                <?php endforeach; ?>
                </div>

                <div class="clearfix"></div><hr>

                <div class="row">
                    <div class="col-6">
                        <h6><small>Ürün Sahibi</small></h6>
                        <a class="" data-toggle="modal" data-target="#messageModal" >
                            <span data-toggle="tooltip" data-placement="right" title="Satıcıya Mesaj Gönder"><?=$data['publication']['user_first_name']." ".$data['publication']['user_last_name']?></span>
                        </a>
                        <a class="" data-toggle="modal" data-target="#complaintModal" >
                            <span data-toggle="tooltip" data-placement="right" title="Şikayet Et"><i class="fa fa-flag fa-lg text-danger ml-2" aria-hidden="true"></i></span>
                        </a>
                    </div>
                    <div class="col-6">
                        <script>$( document ).ready(function() { $('#s<?=$data['pid']?>').raty({ readOnly: true, score: <?=$UserManager->GetSellerPoint($data['publication']['user_id'])?> }); });</script>
                        <h6><small class="float-right">Satıcı Puanı</small></h6><br>
                        <div class="float-right" id="s<?=$data['pid']?>"></div>
                    </div>
                </div>



            </div>

            <div class="clearfix my-3"></div>

            <div class="bg-white border green-border p-3">
                <div class="row">
                    <div class="col-9 col-md-9">
                        <form method="post" action="/basket/add" class="was-validated">
                            <?php
                                if($data['publication']['publication_piece_type']=='A'):
                                    $q = "Adet";
                                elseif($data['publication']['publication_piece_type']=='KG'):
                                    $q = "Kilogram";
                                else:
                                    $q = "Litre";
                                endif;
                            ?>
                            <div class="input-group">
                                <input name="qty" id="qty" type="number" class="form-control rounded-0" placeholder="<?=$q?>" required>
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="submit">
                                        Sepete At
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="id" value="<?=$data['publication']['publication_id']?>">
                            <input type="hidden" name="sid" value="<?=$data['publication']['publication_user_id']?>">
                        </form>
                    </div>
                    <div class="col-3 col-md-3">
                        <?php if(isset($_SESSION["userInfo"]["user_id"])): ?>
                        <button class="btn btn-outline-secondary btn-block" onclick="globalJS.utility.getPost('/product/add-favorite','-','id=<?=$data['publication']['publication_id']?>','Favori Listesine Eklendi')">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                        </button>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="message-form" method="post" action="/user/add-message/<?=$data['pid']?>" class="was-validated">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ürün Sahibine Mesaj Yolla</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input class="form-control" name="message_title" placeholder="Mesaj Başlığı" required>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message_text" rows="5" placeholder="Mesajınız" required></textarea>
                        <input type="hidden" name="seller_id" value="<?=$data['publication']['user_id']?>">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm rounded-0" data-dismiss="modal">Kapat</button>
                    <button type="submit" class="btn btn-warning btn-sm bg-orange rounded-0 orange-border text-white">Gönder</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="complaintModal" tabindex="-1" role="dialog" aria-labelledby="complaintModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="complaint-form" method="post" action="/user/add-complaint/<?=$data['pid']?>" class="was-validated">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Şikayet Et</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <textarea class="form-control" name="complaint_text" rows="5" placeholder="Şikayetiniz hakkında bir kaç cümle yazınız." required></textarea>
                        <input type="hidden" name="seller_id" value="<?=$data['publication']['user_id']?>">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm rounded-0" data-dismiss="modal">Kapat</button>
                    <button type="submit" class="btn btn-warning btn-sm bg-orange rounded-0 orange-border text-white">Gönder</button>
                </div>
            </form>
        </div>
    </div>
</div>