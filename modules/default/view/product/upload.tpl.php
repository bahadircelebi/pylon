<div class="container">
    <h4 class="mt-5 mb-3">Ürün Yükleme Sayfası</h4>
    <div class="card border-0 rounded-0 shadow orange-lr-border mb-3">
        <div class="card-body">
            <div class="row text-center">
                <div class="col-md-4">
                    <?php if(file_exists($Config['system']['user_path'].$data['user']['user_photo'])): ?>
                        <img src="/images/user/<?=$data['user']['user_photo']?>" class="img-fluid rounded-circle orange-border mr-3" width="50">
                    <?php else: ?>
                        <img src="/images/icon/user.png" class="img-fluid rounded-circle orange-border mr-3" width="50">
                    <?php endif; ?>
                    <?=$data['user']['user_first_name']." ".$data['user']['user_last_name']?>
                </div>
                <div class="col-md-2 gray-left-border">
                    <h6>Telefon</h6>
                    <small><?=$data['user']['user_telephone']?></small>
                </div>
                <div class="col-md-2 gray-left-border">
                    <h6>Ülke</h6>
                    <small><?=$data['myCountry']['country_title']?></small>
                </div>
                <div class="col-md-2 gray-left-border">
                    <h6>Ürün Satışı</h6>
                    <?php $totalProduct = count($data['sell']); ?>
                    <small><?=$totalProduct?>+</small>
                </div>
                <div class="col-md-2 gray-left-border">
                    <?php $totalPrice = 0; foreach ($data['sell'] as $sell):
                        $totalPrice += $sell['order_detail_publication_price'];
                    endforeach;
                    ?>
                    <h6>Kazanç</h6>
                    <small>$ <?=$totalPrice?></small>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container pt-5">
    <div class="jumbotron rounded-0 bg-soft-gray shadow">
        <h4 class="orange-color mb-3">Ne Satmak İstiyorsun ?</h4>
        <div class="clearfix"></div>
        <form method="post" action="/product/upload" enctype="multipart/form-data" class="was-validated">
            <div class="row">
                <div class="col-12 col-md-4">
                    <small class="orange-color"><b>Nereden Geliyorsun</b></small>
                    <h3><?=$data['from_country']['name']?><small><?=" (".$data['from_country']['sortname'].")"?></small></h3>
                    <input type="hidden" name="from_country" value="<?=$data['from_country']['id']?>">
                </div>
                <div class="col-12 col-md-4">
                    <small class="orange-color"><b>Nereye Gidiyorsun</b></small>
                    <h3><?=$data['to_country']['name']?><small><?=" (".$data['to_country']['sortname'].")"?></small></h3>
                    <input type="hidden" name="to_country" value="<?=$data['to_country']['id']?>">
                </div>
                <div class="col-12 col-md-4">
                    <small class="orange-color"><b>Tarih</b></small>
                    <h3><?=$data['date']?></h3>
                    <input type="hidden" name="date" value="<?=$data['date']?>">
                </div>
            </div>

            <div class="clearfix"></div><hr class="my-4">

            <div class="row">
                <div class="col-12">
                    <div class="alert alert-info rounded-0">
                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                        İlan yükleme esnasında farklı özellikte olan ürünleriniz için farklı ilanlar oluşturmanız gerekmektedir.
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input name="product_title" type="text" class="form-control rounded-0 green-border" placeholder="Ürün Başlığı *" required>
                        <small>Minimum 10 karakter</small>
                    </div>
                    <div class="form-group">
                        <div class="row">
						  <div class="col-6">
							<div class="list-group" id="list-tab" role="tablist">
							  <?php foreach($data['category'] as $category): ?>
							  <a class="list-group-item list-group-item-action" id="list-home-list" data-toggle="list" href="#"
                                 role="tab" aria-controls="home" onclick="globalJS.utility.getPost('/product/sub-category','subCategory','id=<?=$category['category_id']?>','')"><?=$category['category_name']?></a>
							  <?php endforeach; ?>
							</div>
						  </div>
						  <div class="col-6">
							<div id="subCategory"></div>
						  </div>
						</div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <textarea name="product_info" class="form-control rounded-0 green-border" rows="3" placeholder="Ürün Açıklaması" required></textarea>
                        <small>Marka, model ve diğer özellikler</small>
                    </div>
                    <div class="form-group">
                        <small>Ürün Resimleri</small><br>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="file[]" id="file[]" multiple="multiple" required>
                            <label class="custom-file-label" for="customFile">Resim Seç</label>
                        </div>
                    </div>
                    <div class="form-group">
						<div class="input-group mb-3">
							<input name="product_price" type="number" class="form-control rounded-0 green-border" placeholder="Fiyat *" required>
							<div class="input-group-append">
							  <select name="product_currency" class="custom-select rounded-0 green-border" id="inputGroupSelect03" required>
								<option>USD</option>
								<option>EUR</option>
							  </select>
							</div>
						</div>
                    </div>
                    <div class="form-group">
                        <div class="input-group mb-3">
                            <input name="product_piece" type="number" class="form-control rounded-0 green-border" placeholder="Adet / Kilogram / Litre" required>
                            <div class="input-group-append">
                                <select name="product_piece_type" class="custom-select rounded-0 green-border" id="inputGroupSelect03" required>
                                    <option value="A">ADET</option>
                                    <option value="KG">KILOGRAM</option>
                                    <option value="L">LITRE</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input name="terms" type="checkbox" required> Sözleşmeyi Kabul Ediyorum
                    </div>
                </div>
            </div>
            <div class="clearfix"></div><hr>
            <div class="form-group float-right">
                <button type="submit" class="btn btn-warning btn-sm bg-orange rounded-0 orange-border text-white">Kaydet</button>
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
</div>