<?php

/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 15.01.2018
 * Time: 15:40
 */

class User extends BaseModel
{

    private $table = "user";
    private $key = "user_id";

    private $user_id;
    private $user_mail;
    private $user_birthday;
    private $user_country;
    private $user_city;
    private $user_register_date;
    private $user_profile_picture;
    private $user_type;
    private $user_status;
    private $user_latest_ip;
    private $user_identification;

    /**
     * User constructor.
     * @param $user_id
     */
    public function __construct($user_id = 0)
    {
        $this->user_id = $user_id;

        if($user_id != 0){
            $this->FillClass($user_id);
        }

    }


    public function FillClass($user_id){

        $this->db->cmd->where($this->key, $user_id);
        $row = $this->db->cmd->getOne($this->table);

        foreach ($row as $key => $value){

            $this->$key = $value;
        }

    }


    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getUserMail()
    {
        return $this->user_mail;
    }

    /**
     * @param mixed $user_mail
     */
    public function setUserMail($user_mail)
    {
        $this->user_mail = $user_mail;
    }

    /**
     * @return mixed
     */
    public function getUserBirthday()
    {
        return $this->user_birthday;
    }

    /**
     * @param mixed $user_birthday
     */
    public function setUserBirthday($user_birthday)
    {
        $this->user_birthday = $user_birthday;
    }

    /**
     * @return mixed
     */
    public function getUserCountry()
    {
        return $this->user_country;
    }

    /**
     * @param mixed $user_country
     */
    public function setUserCountry($user_country)
    {
        $this->user_country = $user_country;
    }

    /**
     * @return mixed
     */
    public function getUserCity()
    {
        return $this->user_city;
    }

    /**
     * @param mixed $user_city
     */
    public function setUserCity($user_city)
    {
        $this->user_city = $user_city;
    }

    /**
     * @return mixed
     */
    public function getUserRegisterDate()
    {
        return $this->user_register_date;
    }

    /**
     * @param mixed $user_register_date
     */
    public function setUserRegisterDate($user_register_date)
    {
        $this->user_register_date = $user_register_date;
    }

    /**
     * @return mixed
     */
    public function getUserProfilePicture()
    {
        return $this->user_profile_picture;
    }

    /**
     * @param mixed $user_profile_picture
     */
    public function setUserProfilePicture($user_profile_picture)
    {
        $this->user_profile_picture = $user_profile_picture;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->user_type;
    }

    /**
     * @param mixed $user_type
     */
    public function setUserType($user_type)
    {
        $this->user_type = $user_type;
    }

    /**
     * @return mixed
     */
    public function getUserStatus()
    {
        return $this->user_status;
    }

    /**
     * @param mixed $user_status
     */
    public function setUserStatus($user_status)
    {
        $this->user_status = $user_status;
    }

    /**
     * @return mixed
     */
    public function getUserLatestIp()
    {
        return $this->user_latest_ip;
    }

    /**
     * @param mixed $user_latest_ip
     */
    public function setUserLatestIp($user_latest_ip)
    {
        $this->user_latest_ip = $user_latest_ip;
    }

    /**
     * @return mixed
     */
    public function getUserIdentification()
    {
        return $this->user_identification;
    }

    /**
     * @param mixed $user_identification
     */
    public function setUserIdentification($user_identification)
    {
        $this->user_identification = $user_identification;
    }








}