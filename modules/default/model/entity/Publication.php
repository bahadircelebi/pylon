<?php

/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 15.01.2018
 * Time: 15:41
 */
class Publication  extends BaseModel
{

    private $table = "publication";
    private $key = "publication_id";

    private $publication_id;
    private $publication_title;
    private $publictaion_description;
    private $publication_category_id;
    private $publication_user_id;
    private $publication_from_country;
    private $publication_to_country;
    private $publication_start_date;
    private $publication_end_date;
    private $publication_price;
    private $publication_currency;
    private $publication_status;




    public function FillClass($publication_id){

        $this->db->cmd->where($this->key, $publication_id);
        $row = $this->db->cmd->getOne($this->table);

        foreach ($row as $key => $value){

            $this->$key = $value;
        }

    }

}