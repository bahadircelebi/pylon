<?php

/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 15.01.2018
 * Time: 15:42
 */
class Category extends BaseModel
{

    private $table = "category";
    private $key = "category_id";

    private $category_id;
    private $category_name;
    private $category_description;
    private $category_picture;
    private $category_parent;


    public function FillClass($category_id){

        $this->db->cmd->where($this->key, $category_id);
        $row = $this->db->cmd->getOne($this->table);

        foreach ($row as $key => $value){

            $this->$key = $value;
        }

    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param mixed $category_id
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
    }

    /**
     * @return mixed
     */
    public function getCategoryName()
    {
        return $this->category_name;
    }

    /**
     * @param mixed $category_name
     */
    public function setCategoryName($category_name)
    {
        $this->category_name = $category_name;
    }

    /**
     * @return mixed
     */
    public function getCategoryDescription()
    {
        return $this->category_description;
    }

    /**
     * @param mixed $category_description
     */
    public function setCategoryDescription($category_description)
    {
        $this->category_description = $category_description;
    }

    /**
     * @return mixed
     */
    public function getCategoryPicture()
    {
        return $this->category_picture;
    }

    /**
     * @param mixed $category_picture
     */
    public function setCategoryPicture($category_picture)
    {
        $this->category_picture = $category_picture;
    }

    /**
     * @return mixed
     */
    public function getCategoryParent()
    {
        return $this->category_parent;
    }

    /**
     * @param mixed $category_parent
     */
    public function setCategoryParent($category_parent)
    {
        $this->category_parent = $category_parent;
    }





}