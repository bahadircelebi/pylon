<?php
namespace defaultspace;

/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 15.01.2018
 * Time: 15:42
 */

class BasketManager extends \BaseModel
{


    // Sepete Ilan Ekle
    /**
     *
     */
    public function AddToBasket(){

        /*$this->db->cmd->where("publication_id", $_POST['id']);
        $product = $this->db->cmd->getOne("publication");

        $insert = array();
        $insert[''] = "";

        $_SESSION['basket'][$product['publication_id']] = isset($_SESSION['basket'][$product['publication_id']]) ? ($_SESSION['basket'][$product['publication_id']] + $_POST['qty']) : $_POST['qty'];
        */

        if(isset($_POST["id"])){

            $UserId = isset($_SESSION["userInfo"]["user_id"]) ? $_SESSION["userInfo"]["user_id"] : $_COOKIE['user_tmp_key'];

            $this->db->cmd->where("user_basket_publication_id", $_POST["id"]);
            $this->db->cmd->where("(user_basket_buyer_id = ? OR user_tmp_key = ?)", array($UserId,$UserId));
            $publication = $this->db->cmd->getOne("user_basket");
            if(isset($publication['user_basket_publication_id'])){

                $update = array();
                $update['user_basket_amount'] = $publication['user_basket_amount']+$_POST['qty'];

                $this->db->cmd->where("user_basket_publication_id", $_POST["id"]);
                $this->db->cmd->where("(user_basket_buyer_id = ? OR user_tmp_key = ?)", array($UserId,$UserId));
                if($this->db->cmd->update('user_basket', $update)):
					\Globalf::redirect("product/detail/".$_POST["id"]);
				endif;

            }
            else
            {
                $insert = array();
                $insert['user_basket_publication_id'] = $_POST["id"];
                if(isset($_SESSION["userInfo"]["user_id"])):
                    $insert['user_basket_buyer_id'] = $UserId;
                    $insert['user_tmp_key'] = 0;
                else:
                    $insert['user_basket_buyer_id'] = 0;
                    $insert['user_tmp_key'] = $UserId;
                endif;
				$insert['user_basket_seller_id'] = $_POST["sid"];
                $insert['user_basket_amount'] = $_POST["qty"];
                if($this->db->cmd->insert('user_basket', $insert)):
					\Globalf::redirect("product/detail/".$_POST["id"]);
				endif;
            }

            echo $this->db->cmd->getLastQuery();

        }

    }

    public function ChangeQuantity()
    {
        \Globalf::p($_POST);


        $UserId = isset($_SESSION["userInfo"]["user_id"]) ? $_SESSION["userInfo"]["user_id"] : $_COOKIE['user_tmp_key'];

        $update = array();
        $update["user_basket_amount"] = $_POST['qty'.$_POST['pid']];

        $this->db->cmd->where("(user_basket_buyer_id = ? OR user_tmp_key = ?)", array($UserId,$UserId));
        $this->db->cmd->where("user_basket_publication_id", $_POST['pid']);
        $this->db->cmd->update('user_basket', $update);

        echo $this->db->cmd->getLastQuery();

        \Globalf::redirect("basket/shopping-cart");
    }

    public function DeleteFromBasket()
    {
        $UserId = isset($_SESSION["userInfo"]["user_id"]) ? $_SESSION["userInfo"]["user_id"] : $_COOKIE['user_tmp_key'];

        if(isset($_POST["id"])){

            $this->db->cmd->where("user_basket_publication_id", $_POST["id"]);
            $this->db->cmd->where("(user_basket_buyer_id = ? OR user_tmp_key = ?)", array($UserId,$UserId));
            if($this->db->cmd->delete('user_basket')):
                return "OK";
            endif;

        }
    }

    // Sepetden Tüm İlanları Sil
    public function BasketEmpty(){

        $UserId = isset($_SESSION["userInfo"]["user_id"]) ? $_SESSION["userInfo"]["user_id"] : $_COOKIE['user_tmp_key'];

        if(isset($_SESSION["userInfo"]["user_id"])):
            $this->db->cmd->where("user_basket_buyer_id", $UserId);
        else:
            $this->db->cmd->where("user_tmp_key", $UserId);
        endif;
        $this->db->cmd->delete('user_basket');

        \Globalf::redirect("basket/shopping-cart");

    }

    // Sepete Eklenen İlanlar
    public function GetBasketPublictionList(){

        $UserId = isset($_SESSION["userInfo"]["user_id"]) ? $_SESSION["userInfo"]["user_id"] : $_COOKIE['user_tmp_key'];

        $this->db->cmd->where("user_tmp_key", $UserId);
        $this->db->cmd->where("user_basket_buyer_id", $UserId, "=", "OR");
        $this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication", "publication.publication_id = user_basket.user_basket_publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("user_basket");

    }

    // Sepet Özeti
    public function GetCompleteOrder($id){

        $this->db->cmd->where("order_id", $id);
        return $this->db->cmd->getOne("user_order");

    }

    // Sepet Detay Özeti
    public function GetCompleteOrderDetail($id){

        $this->db->cmd->where("order_detail_order_id", $id);
        $this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication", "publication.publication_id = user_order_detail.order_detail_publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("user_order_detail");

    }

    public function GetBasketTotalPubliction()
    {
        $UserId = isset($_SESSION["userInfo"]["user_id"]) ? $_SESSION["userInfo"]["user_id"] : $_COOKIE['user_tmp_key'];

        $this->db->cmd->where("user_tmp_key", $UserId);
        $this->db->cmd->where("user_basket_buyer_id", $UserId, "=", "OR");
        $product = $this->db->cmd->get("user_basket");

        $totalProduct = 0;
        if(count($product)>0){

            foreach($product as $qty):
                $totalProduct+=$qty['user_basket_amount'];
            endforeach;

            return $totalProduct;
        }else{
            return $totalProduct;
        }

    }

    public function GetAddressId($id)
    {
        $this->db->cmd->where("user_address_id", $id);
        return $this->db->cmd->getOne("user_address");
    }
	
	public function Address()
	{
		$insert = array();
		$insert['user_address_title'] = $_POST['address_title'];;
		$insert['user_address_user_id'] = $_SESSION["userInfo"]["user_id"];
		$insert['user_address_country_id'] = $_POST['user_country'];
		$insert['user_address_city_id'] = $_POST['city'];
		$insert['user_address_text'] = $_POST['address'];
		$insert['user_address_phone'] = $_POST['telephone'];
		$insert['user_address_post_code'] = $_POST['postal_code'];
		$insert['user_address_state_id'] = $_POST['state'];
		if($this->db->cmd->insert("user_address", $insert))
		{
			return "OK";
		}
	}

    /** Bahadır Örnek Kodlar

    // Sepete Ilan Ekle
    public function AddToBasket(){

        if(isset($_POST["publication_id"])){

            $UserId = isset($_SESSION["user"]["user_id"]) ? $_SESSION["user"]["user_id"] : $_SESSION["user_tmp_key"];

            $data = Array (
                "user_basket_publication_id" => $_POST["publication_id"],
                "user_basket_buyer_id" => $UserId,
                "user_basket_amount" => $this->db->inc(1),
            );

            $updateColumns = Array ("user_basket_amount");

            $this->db->cmd->onDuplicate($updateColumns);

            if($this->db->cmd->insert ('user_basket', $data)){
                return "OK";
            }

        }

    }

    // Sepete Eklenen İlanlar
    /public function GetBasketPublictionList($UserId){

        $UserId = isset($_SESSION["user"]["user_id"]) ? $_SESSION["user"]["user_id"] : $_SESSION["user_tmp_key"];

        $this->db->cmd->where("user_basket_buyer_id", $UserId);

        $this->db->cmd->orderBy("user_basket_id", "ASC");

        return $this->db->cmd->get("user_basket");


    }

    /*
    // Sepetden İlan Sil
    public function RemoveFromBasket(){

        if(isset($_POST["publication_id"])){

            $UserId = isset($_SESSION["user"]["user_id"]) ? $_SESSION["user"]["user_id"] : $_SESSION["user_tmp_key"];

            $this->db->cmd->where("publication_id", $_POST["publication_id"]);
            $this->db->cmd->where("user_basket_buyer_id", $UserId);

            if($this->db->cmd->delete("user_basket")){
                return "OK";
            }
        }

    }

    public function DecreaseProductCount(){

        if(isset($_POST["publication_id"])){

            $UserId = isset($_SESSION["user"]["user_id"]) ? $_SESSION["user"]["user_id"] : $_SESSION["user_tmp_key"];

            $update = array("user_basket_amount" => $this->db->inc(1) );



        }

    }


     *
     */

}