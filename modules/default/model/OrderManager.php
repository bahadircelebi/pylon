<?php
namespace defaultspace;

/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 15.01.2018
 * Time: 15:42
 */

/*
 *  Order Status : PW - PA - DW - DA
 */
class OrderManager extends \BaseModel
{

    // Sepeti siparişe dönüştür
    public function BuyBasketPublicationList(){

    }

    // Satıcının sattığı ürünler
    public function GetSellerProducts($status = "PW"){

    }

    public function SaveOrder()
    {

        $IndexManager = new \defaultspace\IndexManager();

        $UserId = isset($_SESSION["userInfo"]["user_id"]) ? $_SESSION["userInfo"]["user_id"] : $_COOKIE['user_tmp_key'];

        $this->db->cmd->orderBy("user_basket_seller_id", "ASC");
        $this->db->cmd->where("user_tmp_key", $UserId);
        $this->db->cmd->where("user_basket_buyer_id", $UserId, "=", "OR");
        $this->db->cmd->join("publication", "publication.publication_id = user_basket.user_basket_publication_id");
        $products =  $this->db->cmd->get("user_basket");

        $seller_id = 0;
        $totalPrice = 0;
        $Orderotal = array();
        foreach($products as $product):
            //0-1
            if($seller_id != $product['user_basket_seller_id']):

                $insert = array();
                $insert['order_buyer_id'] = $_SESSION["userInfo"]["user_id"];
                $insert['order_seller_id'] = $product['user_basket_seller_id'];
                $insert['order_code'] = "PN1234567890";
                $insert['order_address_id'] = $_POST['user_delivery_address'];
                $insert['order_invoice_address_id'] = $_POST['user_delivery_address'];
                $insert['order_payment_type'] = $_POST['payment_type'];
                $insert['order_status'] = "P";
                $insert['order_cargo_no'] = "0";
                $this->db->cmd->insert("user_order", $insert);

                $lastId = $this->db->cmd->getInsertId();
                //Order kayıt bitiş

                //Sipariş Hareketleri
                $insert = array();
                $insert['order_movement_type'] = "P";
                $insert['order_movement_order_id'] = $lastId;
                $this->db->cmd->insert("user_order_movement", $insert);

                //Sipariş Ödemesi
                $insert = array();
                $insert['pending_payment_user_id'] = $_SESSION["userInfo"]["user_id"];
                $insert['pending_payment_order_id'] = $lastId;
                $insert['pending_payment_price'] = 0;
                $insert['pending_payment_currency'] = "USD";
                $insert['pending_payment_accept_user'] = 0;
                $insert['pending_payment_accept_ip'] = "";
                $insert['pending_payment_status'] = "P";

                $this->db->cmd->insert("pending_payment", $insert);

                $totalPrice = 0;

                $this->db->cmd->where("user_id", $product['user_basket_seller_id']);
                $buyer = $this->db->cmd->getOne("user");

                $HTML = <<<HTML
                    <p>Yeni bir siparişiniz var.</p>
                    <p>Profilinizde Satışlarım bölümünden inceleyebilirsiniz.</p>
                    <p>Buygosell'i tercih ettiğiniz için teşekkür ederiz.</p>                    
HTML;

                $IndexManager->Mail($buyer['user_mail'], "Yeni Sipariş Bildirimi", $HTML);

            endif;

            //Sipariş Detayları
            $insert = array();
            $insert['order_detail_order_id'] = $lastId;
            $insert['order_detail_user_id'] = $product['user_basket_buyer_id'];
            $insert['order_detail_seller_id'] = $product['user_basket_seller_id'];
            $insert['order_detail_publication_id'] = $product['user_basket_publication_id'];
            $insert['order_detail_publication_amount'] = $product['user_basket_amount'];
            $insert['order_detail_publication_price'] = $product['publication_price'];
            $this->db->cmd->insert("user_order_detail", $insert);

            $totalPrice += ($product['user_basket_amount']*$product['publication_price']); //5
            $Orderotal[$lastId] =+ $totalPrice;

            //Sipariş Tutarını Güncelle
            $update = array();
            $update['order_total_rate'] = $Orderotal[$lastId];
            $this->db->cmd->where("order_id", $lastId);
            $this->db->cmd->update("user_order", $update);

            //Ödeme Tutarını Güncelle
            $update = array();
            $update['pending_payment_price'] = $Orderotal[$lastId];
            $this->db->cmd->where("pending_payment_order_id", $lastId);
            $this->db->cmd->update("pending_payment", $update);

            $seller_id = $product['user_basket_seller_id'];

            /*$update = array();
            $update['publication_option_stock'] -= $product['user_basket_amount'];

            $this->db->cmd->where("publication_option_publication_id", $product['user_basket_publication_id']);
            $this->db->cmd->update("publication_option", $update);*/

        endforeach;

        $this->db->cmd->where("user_basket_buyer_id", $UserId);
        $this->db->cmd->delete('user_basket');

        \Globalf::redirect("basket/complete-order/".$lastId);

    }


    public function GetOrderMovement($id)
    {
        $this->db->cmd->where("order_movement_order_id", $id);
        return $this->db->cmd->get("user_order_movement");
    }

    public function UpdateOrderStatus($status)
    {
        $IndexManager = new \defaultspace\IndexManager();

        $insert = array();
        $insert['order_movement_type'] = $status;
        $insert['order_movement_order_id'] = $_POST['oid'];
        $this->db->cmd->insert("user_order_movement", $insert);

        $this->db->cmd->where("user_order.order_id", $_POST['oid']);
        $this->db->cmd->join("user", "user.user_id = user_order.order_seller_id", "INNER");
        $buyer = $this->db->cmd->getOne("user_order");

        $HTML = <<<HTML
                    <p>Ürün(ler) alıcıya başarılı bir şekilde teslim edilmiştir.</p>
                    <p>Yönetim panelinizden inceleyebilirsiniz.</p>
                    <p>Buygosell'i tercih ettiğiniz için teşekkür ederiz.</p>                    
HTML;

        $IndexManager->Mail($buyer['user_mail'], "Ürün Teslimat Onayı", $HTML);

    }

    public function UpdateOrderCargoKey()
    {
        $IndexManager = new \defaultspace\IndexManager();

        $insert = array();
        $insert['order_movement_type'] = "S";
        $insert['order_movement_order_id'] = $_POST['oid'];
        $this->db->cmd->insert("user_order_movement", $insert);

        $update = array();
        $update['order_cargo_no'] = $_POST['cargo_no'];
        $update['order_cargo_company'] = $_POST['cargo_company'];

        $this->db->cmd->where("order_id", $_POST['oid']);
        $this->db->cmd->update("user_order", $update);

        $this->db->cmd->where("user_order.order_id", $_POST['oid']);
        $this->db->cmd->join("user", "user.user_id = user_order.order_buyer_id", "INNER");
        $buyer = $this->db->cmd->getOne("user_order");

        $HTML = <<<HTML
                    <p>Siparişiniz kargoya verilmiştir. Aşağıdaki kargo bilgilerinden siparişinizi takip edebilirsiniz.</p>
                    <h3>Kargo Firması : {$buyer['order_cargo_company']}</h3>
                    <h3>Karno Numarası : {$buyer['order_cargo_no']}</h3>
                    <p>Buygosell'i tercih ettiğiniz için teşekkür ederiz.</p>                    
HTML;

        $IndexManager->Mail($buyer['user_mail'], "Siparişiniz Kargoya Verildi", $HTML);
    }
	
	// Sepet Özeti
    public function GetCompleteOrder($id){

        $this->db->cmd->where("order_id", $id);
        return $this->db->cmd->getOne("user_order");

    }

    // Sepet Detay Özeti
    public function GetCompleteOrderDetail($id){

        $this->db->cmd->where("order_detail_order_id", $id);
        $this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication", "publication.publication_id = user_order_detail.order_detail_publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("user_order_detail");

    }

    public function GetOrderPoint($id)
    {
        $this->db->cmd->where("point_order_id", $id);
        return $this->db->cmd->getOne("user_order_point");
    }


}