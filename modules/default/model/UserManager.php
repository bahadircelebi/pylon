<?php
namespace defaultspace;
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 15.01.2018
 * Time: 15:42
 */

class UserManager extends \BaseModel
{

    public function GetUserInfo(){

        /*$User = new User($UserId);
        return $User;*/

        $this->db->cmd->where("user_id", $_SESSION['userInfo']['user_id']);
        return $this->db->cmd->getOne("user");
    }

    public function GetUser($id){
        $this->db->cmd->where("user_id", $id);
        return $this->db->cmd->getOne("user");
    }

    public function Register($first_name, $last_name, $user_email, $password, $repassword, $country_id, $state_id, $city_id){

        $IndexManager = new \defaultspace\IndexManager();

        if($password != $repassword)
            return "Şifreler Eşleşmiyor";

        if(strlen($password) < 6 || strlen($repassword) < 6)
            return "Şifre 6 karakterden kısa olamaz!";

        if (!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
            return "Mail adresi hatalı";
        }

        if (isset($_POST['g-recaptcha-response'])) {
            $captcha = $_POST['g-recaptcha-response'];
        }

        if (!$captcha) {
            return "Lütfen robot olmadığınızı doğrulayın.";
            exit;
        }

        $kontrol = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=SECRET KEY&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
        if ($kontrol.success == false) {
            return "Spam Gönderi!";
        }

        $this->db->cmd->where("user_mail", $user_email);
        $user = $this->db->cmd->getOne("user");
        if(count($user)>0){
            return "Bu Mail Adresi ile Daha Önce Kayıt Olunmuş!";
        }

        $tmpkey = \Globalf::Encrypt("encrypt", $user_email . time());

        $insert = array();

        $insert["user_first_name"] = $first_name;
        $insert["user_last_name"] = $last_name;
        $insert["user_mail"] = $user_email;
        $insert["user_password"] = \Globalf::Encrypt("encrypt", $password);
        $insert["user_country"] = $country_id;
        $insert["user_state"] = $state_id;
        $insert["user_city"] = $city_id;
        $insert["user_register_date"] = date("Y-m-d H:i:s");
        $insert["user_tmp_key"] = $tmpkey;

        $link = $this->config["system"]["base_url"] . "user/activate?key=$tmpkey";
        $HTML = <<<HTML
                    <h1></h1>
                    Lünten aşağıdaki linke tıklayarak üyeliğinizi aktif ediniz. 
                    <a href="{$link}">Hesabımı Aktif Et</a>                    
HTML;

        if($this->db->cmd->insert("user", $insert)){

            $IndexManager->Mail($user_email, "Üyelik Aktivasyonu", $HTML);
            //\Globalf::SendMail($user_email, "Üyelik Aktivasyonu", $HTML);

            return "OK";
        }

        return "Error";

    }

    public function AccountActivate()
    {
        $update = array();
        $update["user_status"] = "A";

        $this->db->cmd->where("user_tmp_key", $_GET["key"]);
        if($this->db->cmd->update("user", $update)){
            return "success";
        }
    }

    public function Login($user_email, $password){

		$pass = \Globalf::Encrypt("encrypt", $password);
		$this->db->cmd->where("user_mail", $user_email);
        $this->db->cmd->where("user_password", $pass);

        $user = $this->db->cmd->getOne("user");

        if(isset($user["user_id"])){

            if($user["user_status"]!='A'){
                return "Hesabınız Henüz Aktive Edilmemiş!";
            }

            $_SESSION["userInfo"] = $user;

            $update = array();
            $update['user_last_entry_date'] = date("Y-m-d H:i:s");

            $this->db->cmd->where("user_id", $user["user_id"]);
            $this->db->cmd->update('user', $update);

            $this->db->cmd->where("user_tmp_key", $_COOKIE['user_tmp_key']);
            $product = $this->db->cmd->get("user_basket");

            if(count($product)>0):
                $update = array();
                $update["user_basket_buyer_id"] = $_SESSION["userInfo"]["user_id"];
                $update['user_tmp_key'] = 0;

                $this->db->cmd->where("user_tmp_key", $_COOKIE['user_tmp_key']);
                $this->db->cmd->update('user_basket', $update);

            endif;

            return "OK";
        }

        return "Kullanıcı adı veya şifre hatalı";

    }

    //Kullanıcı bilgileri güncelleme
    public function UpdateProfile(){

		if(!empty($_FILES['file']['name'])){

			$filename = md5($_SESSION["userInfo"]["user_id"] . time());

			$array = explode('.', $_FILES['file']['name']);
			$extension = strtolower(end($array));

			$folder = $this->config["system"]["user_path"] . $_SESSION["userInfo"]["user_id"];

			@mkdir($folder, 0755, true);
			chmod($folder, 0755);

			$fullPath = $folder . "/" . $filename . ".$extension";
			move_uploaded_file($_FILES['file']['tmp_name'], $fullPath);

			$bigImage = $fullPath;
			//$this->PhotoResizeWidth($bigImage, $bigImage, 1000);
			$PublicationManager = new \defaultspace\PublicationManager();
			$PublicationManager->PhotoCrop($bigImage, 250, 250);
		
		}

        $update = array();

        $update['user_first_name'] = $_POST['user_first_name'];
        $update['user_last_name'] = $_POST['user_last_name'];
        $update['username'] = $_POST['username'];
        $update['user_birthday'] = $_POST['user_birthday'];
        $update['user_gender'] = $_POST['user_gender'];
        $update['user_country'] = $_POST['user_country'];
        $update['user_state'] = $_POST['state'];
        $update['user_city'] = $_POST['city'];
		$update['user_telephone'] = $_POST['user_telephone'];
		if(!empty($_FILES['file']['name']))
		$update['user_photo'] = $_SESSION["userInfo"]["user_id"]."/".$filename . ".$extension";

        $this->db->cmd->where("user_id", $_SESSION["userInfo"]["user_id"]);
        if($this->db->cmd->update("user", $update)){
            return "Bilgiler Güncellendi";
        }

    }
	
	public function SendPassword()
    {

        $IndexManager = new \defaultspace\IndexManager();

        if (isset($_POST["member_mail"]) && $_POST["member_mail"] != "" && isset($_POST["send"])) {

            $this->db->cmd->where("user_mail", $_POST["member_mail"]);
            $data = $this->db->cmd->getOne("user");

            if (isset($data["user_mail"])) {

                $tmpkey = \Globalf::Encrypt("encrypt",$data["user_id"] . $data["user_mail"] . time());

                $update = array();
                $update["user_tmp_key"] = $tmpkey;

                $this->db->cmd->where("user_id", $data["user_id"]);
                $this->db->cmd->update("user", $update);

                $link = $this->config["system"]["base_url"] . "user/recoverychange?key=$tmpkey";

                $HTML = <<<HTML
                    Lütfen Parolayı sıfırlamak için linke tıklayınız. 
                    <a href="{$link}">Şifremi Sıfırla</a>                    
HTML;


                $IndexManager->Mail($data["user_mail"], "Şifre Sıfırlama", $HTML);
                //return \Globalf::SendMail($data["user_mail"], "Password Recovery", $HTML);
                return "OK";

            }

        }else{
            return "missing";
        }
    }
	
	public function RecoveryChange(){

        if(isset($_POST['password']) && $_POST['password'] != "" && isset($_POST['password_retry']) && isset($_POST["key"])){
            if($_POST['password'] == $_POST['password_retry']){

                if (strlen($_POST["password"]) < 6) {
                    return "pwd_short";
                }

                $update = array();
                $update["user_password"] = \Globalf::Encrypt("encrypt", $_POST['password']);

                $this->db->cmd->where("user_tmp_key", $_POST["key"]);
                if($this->db->cmd->update("user", $update)){
                    return "OK";
                }

            }else{
                return "repassword";
            }

        }else{
            return "missing";
        }


    }
	
	public function ChangePassword()
    {

        if (isset($_POST["user_password"]) && isset($_POST["user_password_new"]) && isset($_POST["user_password_retype"])) {

            $password = \Globalf::Encrypt("encrypt", $_POST["user_password"]);

            $this->db->cmd->where("user_password", $password);

            $this->db->cmd->where("user_id", $_SESSION["userInfo"]["user_id"]);

            $info = $this->db->cmd->getOne("user");

            if (!isset($info["user_id"])) {
                return "not_exist";
            }

            if ($_POST["user_password_new"] != $_POST["user_password_retype"]) {
                return "not_match";
            }

            if (strlen($_POST["user_password_new"]) < 6) {
                return "pwd_short";
            }

            $password = \Globalf::Encrypt("encrypt", $_POST["user_password_new"]);

            $update = array();
            $update["user_password"] = $password;

            $this->db->cmd->where("user_id", $_SESSION["userInfo"]["user_id"]);
            $this->db->cmd->update("user", $update);

            return "pwd_chngd";

        } else {

            return "missing";
        }
    }

    // Kullanıcıya yapılan yorumlar
    public function GetUserReviewList(){

    }

    // Kullanıcının İlanları
    public function GetUserPublication(){

    }

    // Login olan kullanıcının ilanlarını getir ...
    public function GetMyPublication(){

    }

    // Login olan kullanıcının sepetindeki ürünleri ...
    public function GetMyBasketList(){

    }

    // Login olan kullanıcının toplam siparişleri ...
    public function GetMyOrderList(){

    }
	
	// Kullanıcının adresleri
    public function GetMyAddress(){
		$this->db->cmd->where("user_address.user_address_user_id", $_SESSION['userInfo']['user_id']);
        $this->db->cmd->join("countries as C", "C.id = user_address.user_address_country_id", "INNER");
        return $this->db->cmd->get("user_address");
    }

    public function GetAddress($id){
        $this->db->cmd->where("user_address_id", $id);
        return $this->db->cmd->getOne("user_address");
    }
	
	public function GetFavorite()
    {
        $this->db->cmd->where("favorite_user_id", $_SESSION['userInfo']['user_id']);
		$this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication", "publication.publication_id = user_favorite.favorite_publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("user_favorite");
    }

    public function DeleteFavorite()
    {
        $this->db->cmd->where("favorite_id", $_POST['id']);
        if($this->db->cmd->delete("user_favorite")){
            return "Ürün Favori Listesinden Çıkarıldı";
        }
    }

    public function GetBuy()
    {
        $this->db->cmd->where("user_order.order_buyer_id", $_SESSION['userInfo']['user_id']);
        $this->db->cmd->groupBy("user_order.order_id");
        $this->db->cmd->join("user_order_detail", "user_order_detail.order_detail_order_id = user_order.order_id", "INNER");
        $this->db->cmd->join("publication", "publication.publication_id = user_order_detail.order_detail_publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("user_order");
    }

    public function GetSell()
    {
        $this->db->cmd->where("user_order_detail.order_detail_seller_id", $_SESSION['userInfo']['user_id']);
        $this->db->cmd->groupBy("user_order.order_id");
        $this->db->cmd->join("user_order_detail", "user_order_detail.order_detail_order_id = user_order.order_id", "INNER");
        $this->db->cmd->join("publication", "publication.publication_id = user_order_detail.order_detail_publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("user_order");
    }

    public function SaveAddress()
    {
        $insert = array();
        $insert['user_address_title'] = $_POST['address_title'];
        $insert['user_address_user_id'] = $_SESSION["userInfo"]["user_id"];
        $insert['user_address_country_id'] = $_POST['user_country'];
        $insert['user_address_city_id'] = $_POST['city'];
        $insert['user_address_text'] = $_POST['address'];
        $insert['user_address_phone'] = $_POST['telephone'];
        $insert['user_address_post_code'] = $_POST['postal_code'];
        $insert['user_address_state_id'] = $_POST['state'];;
        if($this->db->cmd->insert("user_address", $insert))
        {
            return "Kayıt Başarılı";
        }
    }

    public function GetBankInfo()
    {
        $this->db->cmd->where("user_bank_user_id", $_SESSION['userInfo']['user_id']);
        return $this->db->cmd->get("user_bank");
    }

    public function SaveBank()
    {
        $insert = array();
        $insert['user_bank_name'] = $_POST['bank_name'];
        $insert['user_bank_holder_name'] = $_POST['account_holder'];
        $insert['user_bank_iban'] = $_POST['bank_iban'];
        $insert['user_bank_user_id'] = $_SESSION["userInfo"]["user_id"];
        if($this->db->cmd->insert("user_bank", $insert))
        {
            return "Kayıt Başarılı";
        }
    }

    public function DeleteBank()
    {
        $this->db->cmd->where("user_bank_id", $_POST['id']);
        $this->db->cmd->where("user_bank_user_id", $_SESSION["userInfo"]["user_id"]);
        if($this->db->cmd->delete("user_bank")){
            return "OK";
        }
    }

    public function GetCountry(){
        return $this->db->cmd->get("countries");
    }

    public function GetCountrySelect($id){
        $this->db->cmd->where("id", $id);
        return $this->db->cmd->getOne("countries");
    }

    public function GetState($id){
        $this->db->cmd->where("country_id", $id);
        return $this->db->cmd->get("states");
    }

    public function GetCity($id){
        $this->db->cmd->where("state_id", $id);
        return $this->db->cmd->get("cities");
    }

    public function GetUserId(){

        $UserId = isset($_SESSION["user"]["user_id"]) ? $_SESSION["user"]["user_id"] : $_SESSION["user_tmp_key"];

        $this->db->cmd->where("user_id=? or user_tmp_key = ?", array($UserId, $UserId));

        $res = $this->db->cmd->getOne("user", "user_id");

        return $res["user_id"];
    }

    public function GetMessage()
    {
        $this->db->cmd->where("user_message_receiver", $_SESSION["userInfo"]["user_id"]);
        $this->db->cmd->where("user_message_sender", $_SESSION["userInfo"]["user_id"], "=", "OR");
        $this->db->cmd->join("user", "user.user_id = user_message.user_message_sender", "INNER");
        return $this->db->cmd->get("user_message");
    }

    public function GetMessageDetail($id)
    {
        $this->db->cmd->where("user_message_id", $id);
        return $this->db->cmd->get("user_message_detail");
    }

    public function AddMessage()
    {
        $insert = array();
        $insert['user_message_title'] = $_POST['message_title'];
        $insert['user_message_sender'] = $_SESSION["userInfo"]["user_id"];
        $insert['user_message_receiver'] = $_POST['seller_id'];
        $insert['user_message_datetime'] = date("Y-m-d H:i:s");
        $this->db->cmd->insert("user_message", $insert);

        $lastId = $this->db->cmd->getInsertId();

        $insert2 = array();
        $insert2['text'] = $_POST['message_text'];
        $insert2['user_id'] = $_SESSION["userInfo"]["user_id"];
        $insert2['user_message_id'] = $lastId;
        $insert2['user_message_ip'] = $this->GetIP();
        $insert2['message_detail_datetime'] = date("Y-m-d H:i:s");
        if($this->db->cmd->insert("user_message_detail", $insert2))
        {
            return "Mesajınız İletilmiştir.";
        }
    }
	
	public function AddMessages($id)
    {
        $insert2 = array();
        $insert2['text'] = $_POST['message_text'];
        $insert2['user_id'] = $_SESSION["userInfo"]["user_id"];
        $insert2['user_message_id'] = $id;
        $insert2['user_message_ip'] = $this->GetIP();
        $insert2['message_detail_datetime'] = date("Y-m-d H:i:s");
        if($this->db->cmd->insert("user_message_detail", $insert2))
        {
            return "Kayıt Başarılı";
        }
    }

    public function AddComplaint()
    {
        $insert = array();
        $insert['user_complaint_text'] = $_POST['complaint_text'];
        $insert['user_complaint_sender_id'] = $_SESSION["userInfo"]["user_id"];
        $insert['user_complaint_seller_id'] = $_POST['seller_id'];
        $insert['user_complaint_datetime'] = date("Y-m-d H:i:s");
        if($this->db->cmd->insert("user_complaint", $insert))
        {
            return "Şikayetiniz İletilmiştir.";
        }
    }

    public function GetSellOrder()
    {
        $this->db->cmd->where("user_order.order_status", "S");
        $this->db->cmd->where("user_order_detail.order_detail_seller_id", $_SESSION['userInfo']['user_id']);
        $this->db->cmd->join("user_order_detail", "user_order_detail.order_detail_order_id = user_order.order_id", "INNER");
        $this->db->cmd->join("publication", "publication.publication_id = user_order_detail.order_detail_publication_id");
        return $this->db->cmd->get("user_order");

    }

    public function GetIP(){
        if(getenv("HTTP_CLIENT_IP")) {
            $ip = getenv("HTTP_CLIENT_IP");
        } elseif(getenv("HTTP_X_FORWARDED_FOR")) {
            $ip = getenv("HTTP_X_FORWARDED_FOR");
            if (strstr($ip, ',')) {
                $tmp = explode (',', $ip);
                $ip = trim($tmp[0]);
            }
        } else {
            $ip = getenv("REMOTE_ADDR");
        }
        return $ip;
    }

    public function GetSellerPoint($id)
    {
        $this->db->cmd->where("point_user_id", $id);
        $points = $this->db->cmd->get("user_order_point");

        $p = 0;
        foreach($points as $point):
            $p += $point['point_rate'];
        endforeach;

        if(count($points)>0):
            $quantity = count($points);
            $result = $p / $quantity;
        else:
            $result = 0;
        endif;

        return $result;
    }

    public function GetUserPublicationPoint($id)
    {
        $this->db->cmd->where("point_user_id", $_SESSION['userInfo']['user_id']);
        $this->db->cmd->where("point_publication_id", $id);
        return $this->db->cmd->getOne("publication_point");
    }

}