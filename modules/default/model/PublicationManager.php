<?php
namespace defaultspace;

/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 16.01.2018
 * Time: 09:17
 */
/*
 *    Publication Notes
 *    Status : P : Pending A:Active D:Disabled C : Com
 */
class PublicationManager extends \BaseModel
{

    // Yeni ilan girişi
    public function AddPublication(){


		//ilan Kaydet
		$insert = array();
		$insert['publication_title'] = $_POST['product_title'];
		$insert['publication_description'] = $_POST['product_info'];
		$insert['publication_category_id'] = $_POST['category'];
		$insert['publication_user_id'] = $_SESSION["userInfo"]["user_id"];
		$insert['publication_from_country'] = $_POST['from_country'];
		$insert['publication_to_country'] = $_POST['to_country'];
		$insert['publication_start_date'] = $_POST['date'];
		$insert['publication_end_date'] = $_POST['date'];
		$insert['publication_price'] = $_POST['product_price'];
		$insert['publication_currency'] = $_POST['product_currency'];
		$insert['publication_piece'] = $_POST['product_piece'];
		$insert['publication_piece_type'] = $_POST['product_piece_type'];
		$this->db->cmd->insert("publication", $insert);

		$lastId = $this->db->cmd->getInsertId();

		//ilan resimleri kaydet

        $count = count($_FILES['file']['name']);

        for($i=0; $i < $count; $i++){
            if(!empty($_FILES['file']['name'][$i])){

                $filename = md5($_SESSION["userInfo"]["user_id"] .$i. time());

                $array = explode('.', $_FILES['file']['name'][$i]);
                $extension = strtolower(end($array));

                $folder = $this->config["system"]["upload_path"] . $lastId;

                @mkdir($folder, 0755, true);
                chmod($folder, 0755);

                $fullPath = $folder . "/" . $filename . ".$extension";
                move_uploaded_file($_FILES['file']['tmp_name'][$i], $fullPath);

                $bigImage = $fullPath;
                $imgResizer = new \Eventviva\ImageResize($fullPath);
                $imgResizer->resizeFit($bigImage, 1000, 1435);
                //$this->PhotoResizeWidth($bigImage, $bigImage, 1000);
				//$this->PhotoCrop($bigImage, 1000, 1435);
                $newPath = $folder . "/" . $filename . "-640.$extension";
                $this->PhotoResizeWidth($fullPath, $newPath, 640); //640px genişlik ürün resmi
                $newPath = $folder . "/" . $filename . "-150.$extension";
                $this->PhotoResizeWidth($fullPath, $newPath, 150); //150x150px küçük resim
                $this->PhotoCrop($newPath, 150, 150);

                $insert = array();
                $insert['publication_id'] = $lastId;
                $insert['publication_photo_path'] = $lastId."/".$filename;
                $insert['publication_photo_extension'] = $extension;
                $this->db->cmd->insert("publication_photo", $insert);

            }
        }
		
	   //ilan seçenekleri kaydet

        if(isset($_POST['option'])>0):
            foreach ($_POST['option'] as $_POST['option']=>$value) {
                $insert = array();
                $insert['publication_option_publication_id'] = $lastId;
                $insert['publication_option_detail_id'] = $value;
                $insert['publication_option_size'] = 0;
                $insert['publication_option_stock'] = 0;
                $this->db->cmd->insert("publication_option", $insert);
            }
        endif;

        return $lastId;
		
    }

    public function UpdatePublication($id)
    {
        //ilan Güncelle
        $update = array();
        $update['publication_title'] = $_POST['product_title'];
        $update['publication_description'] = $_POST['product_info'];
        $update['publication_from_country'] = $_POST['from_country'];
        $update['publication_to_country'] = $_POST['to_country'];
        $update['publication_start_date'] = $_POST['date'];
        $update['publication_end_date'] = $_POST['date'];
        $update['publication_price'] = $_POST['product_price'];
        $update['publication_currency'] = $_POST['product_currency'];
        $update['publication_piece'] = $_POST['product_piece'];
        $update['publication_piece_type'] = $_POST['product_piece_type'];

        $this->db->cmd->where("publication_id", $id);
        $this->db->cmd->update("publication", $update);

        \Globalf::redirect("profile/manage-detail/".$id);
    }

    public function DeletePublicationPhoto()
    {
        if(isset($_POST["id"])){

            $this->db->cmd->where("publication_photo_id", $_POST["id"]);
            if($this->db->cmd->delete('publication_photo')):
                return "OK";
            endif;

        }
    }

    public function AddPublicationPhoto()
    {
        $pid = $_POST['pid'];
        $count = count($_FILES['file']['name']);

        for($i=0; $i < $count; $i++){
            if(!empty($_FILES['file']['name'][$i])){

                $filename = md5($_SESSION["userInfo"]["user_id"] .$i. time());

                $array = explode('.', $_FILES['file']['name'][$i]);
                $extension = strtolower(end($array));

                $folder = $this->config["system"]["upload_path"] . $pid;

                @mkdir($folder, 0755, true);
                chmod($folder, 0755);

                $fullPath = $folder . "/" . $filename . ".$extension";
                move_uploaded_file($_FILES['file']['tmp_name'][$i], $fullPath);

                $bigImage = $fullPath;
                $imgResizer = new \Eventviva\ImageResize($fullPath);
                $imgResizer->resizeFit($bigImage, 1000, 1435);
                //$this->PhotoResizeWidth($bigImage, $bigImage, 1000);
                //$this->PhotoCrop($bigImage, 1000, 1435);
                $newPath = $folder . "/" . $filename . "-640.$extension";
                $this->PhotoResizeWidth($fullPath, $newPath, 640); //640px genişlik ürün resmi
                $newPath = $folder . "/" . $filename . "-150.$extension";
                $this->PhotoResizeWidth($fullPath, $newPath, 150); //150x150px küçük resim
                $this->PhotoCrop($newPath, 150, 150);

                $insert = array();
                $insert['publication_id'] = $pid;
                $insert['publication_photo_path'] = $pid."/".$filename;
                $insert['publication_photo_extension'] = $extension;
                $this->db->cmd->insert("publication_photo", $insert);

            }
        }

        \Globalf::redirect("profile/manage-detail/".$pid);
    }

    public function GetPublications()
    {
        $this->db->cmd->orderBy("publication.publication_id", "DESC");
        $this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("publication");
    }

    public function GetPublication($id)
    {
        $this->db->cmd->where("publication.publication_id", $id);
        $this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("publication");
    }

    public function GetSellerPendingPublications()
    {
        $this->db->cmd->where("publication.publication_status", "P");
        $this->db->cmd->where("publication.publication_user_id", $_SESSION["userInfo"]["user_id"]);
        $this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("publication");
    }

    public function GetSellerActivePublications()
    {
        $this->db->cmd->where("publication.publication_status", "A");
        $this->db->cmd->where("publication.publication_user_id", $_SESSION["userInfo"]["user_id"]);
        $this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("publication");
    }
	
	public function AddFavorite()
	{
		$insert = array(); 
		$insert['favorite_user_id'] = $_SESSION["userInfo"]["user_id"];
		$insert['favorite_publication_id'] = $_POST['id'];
		$this->db->cmd->insert("user_favorite", $insert);
	}

    // İlan Detayları
    public function GetPublicationDetail($id){

        $this->db->cmd->where("publication_id", $id);
        $this->db->cmd->join("category", "category.category_id = publication.publication_category_id");
        $this->db->cmd->join("user", "user.user_id = publication.publication_user_id");
        return $this->db->cmd->getOne("publication");

    }

    public function PublicationConfirm($id)
    {
        if(isset($_SESSION["adminInfo"]["admin_id"])):
            $update = array();
            $update['publication_status'] = "A";

            $this->db->cmd->where("publication_id", $id);
            if($this->db->cmd->update("publication", $update)):
                \Globalf::redirect("product/detail/".$id);
            endif;

        endif;

    }

    // İlan Resimleri
    public function GetPublicationPhoto($id){

        $this->db->cmd->where("publication_id", $id);
        return $this->db->cmd->get("publication_photo");

    }

    public function GetPublicationOption($id)
    {
        $this->db->cmd->where("publication_option.publication_option_publication_id", $id);
        $this->db->cmd->join("option_detail", "option_detail.option_detail_id = publication_option.publication_option_detail_id");
        $this->db->cmd->join("option", "option.option_id = option_detail.option_id");
        return $this->db->cmd->get("publication_option");
    }

    // Populer İlanlar
    public function GetPopulerPublicationList(){

        $this->db->cmd->where("publication.publication_status", "A");
        $this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("publication", 12);

    }

    // Populer İlanlar
    public function GetNewPublicationList(){

        $this->db->cmd->where("publication.publication_status", "A");
        $this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("publication", 4);

    }
	
	// Son Satılan İlanlar
    public function GetLastSalePublicationList(){

        $this->db->cmd->where("publication.publication_status", "A");
        $this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("publication", 3);

    }

    // Ülkelere Göre İlanlar
    public function GetPublicationListByCountry($from = "", $to = ""){

    }

    // Kullanıcının İlanları
    public function GetPublicationListByUser($user_id){

    }

    public function AutoSearch()
    {
        $return = array();

        $this->db->cmd->where("publication_title", "%".$_GET['term']."%", "LIKE");
        $titles = $this->db->cmd->get("publication");

        foreach ($titles as $title):
               $return[] = $title['publication_title'];
        endforeach;

        return json_encode($return);
    }

    public function PublicationSearch()
    {

        $CategoryManager = new \defaultspace\CategoryManager();
        /*$this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        //return $this->db->cmd->get("publication");
        return $this->db->cmd->paginate("publication", 1);*/
        $word = $_GET['t'];
        $categoryId = $_GET['q'];

        $allCat = $CategoryManager->GetSubAllCategory($categoryId);

        $allCat = array_values($allCat);

        $allCat[] = $categoryId;

        $CatOptions = $CategoryManager->GetCategoryOptionList($categoryId);
        $opt = "";
        foreach($CatOptions as $CatOption):

            $options = $CategoryManager->GetOptions($CatOption['option_id']);

            foreach($options as $option):
                //numara == numara

                $values = array_values($_GET[$option['option_key_value']]);

                if( in_array($option['option_value'], $values)):
                    $opt[] = $option['option_detail_id'];
                endif;
            endforeach;

        endforeach;


        if($opt != ""){
            $this->db->cmd->where("publication_option.publication_option_detail_id", $opt, "IN");
        }

        if(isset($_GET['country'])){
            $this->db->cmd->where("publication.publication_from_country", $_GET['country']);
        }

        if(isset($_GET['to_country'])){
            $this->db->cmd->where("publication.publication_to_country", $_GET['to_country']);
        }

        if(isset($_GET['date'])){
            $this->db->cmd->where("(publication.publication_start_date >= ? AND publication.publication_end_date <= ?)", array($_GET['date'],$_GET['date']));
        }

        if(isset($_GET['range_min']) && isset($_GET['range_max'])){
            $this->db->cmd->where("(publication.publication_price >= ? AND publication.publication_price <= ?)", array($_GET['range_min'],$_GET['range_max']));
        }

        $this->db->cmd->where("publication.publication_status", "A");
        $this->db->cmd->where("publication.publication_title", "%".$word."%", "LIKE");
        $this->db->cmd->where("publication.publication_category_id", $allCat, "IN");
        $this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        $this->db->cmd->join("publication_option", "publication_option.publication_option_publication_id = publication.publication_id", "LEFT");
        return $this->db->cmd->get("publication");

    }
	
	
	public function GetBestPublication()
	{
        $this->db->cmd->where("publication.publication_status", "A");
		$this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("publication");
	}

    // Anasayfadaki ilanları çek

    public function GetHomePagePublicationList(){

        /*$this->db->cmd->where("publication_status", "A");
        $this->db->cmd->orderBy("publication_id", "DESC");

        $data = $this->db->cmd->get("publication", null, "publication_id");

        $PublicationList = null;

        if(count($data)>0){

            foreach ($data as $publication){

                $PublicationClass = new Publication($publication["publication_id"]);
                $PublicationList[] = $PublicationClass;

            }
        }

        return $PublicationList;*/

        $this->db->cmd->where("publication.publication_status", "A");
		$this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
        return $this->db->cmd->get("publication");
    }

    public function OrderPoint()
    {
        $insert = array();
        $insert['point_rate'] = $_POST['score'];
        $insert['point_order_id'] = $_POST['pid'];
        $insert['point_user_id'] = $_SESSION["userInfo"]["user_id"];
        if($this->db->cmd->insert("user_order_point", $insert)):
            return "Puanınız Kaydedildi.";
        endif;
    }

    public function PhotoResize($fullPath, $width, $height)
    {
        $imgResizer = new \Eventviva\ImageResize($fullPath);
        $imgResizer->resizeToBestFit($width, $height);
        $imgResizer->save($fullPath);
    }

    public function PhotoResizeWidth($fullPath, $newPath, $width)
    {
        $imgResizer = new \Eventviva\ImageResize($fullPath);
        $imgResizer->resizeToWidth($width);
        $imgResizer->save($newPath);
    }

    public function PhotoResizeHeight($fullPath, $newPath, $height)
    {
        $imgResizer = new \Eventviva\ImageResize($fullPath);
        $imgResizer->resizeToHeight($height);
        $imgResizer->save($newPath);
    }

    public function PhotoCrop($fullPath, $width, $height)
    {
        $imgResizer = new \Eventviva\ImageResize($fullPath);
        $imgResizer->crop($width, $height);
        $imgResizer->save($fullPath);
    }

    public function PublicationPoint()
    {
        $insert = array();
        $insert['point_rate'] = $_POST['score'];
        $insert['point_publication_id'] = $_POST['pid'];
        $insert['point_user_id'] = $_SESSION["userInfo"]["user_id"];
        if($this->db->cmd->insert("publication_point", $insert)):
            return "Puanınız Kaydedildi.";
        else:
            return "Daha Önce Puan Verdiniz.";
        endif;
    }

    public function GetPublicationPoint($id)
    {
        $this->db->cmd->where("point_publication_id", $id);
        $points = $this->db->cmd->get("publication_point");

        $p = 0;
        foreach($points as $point):
            $p += $point['point_rate'];
        endforeach;

        if(count($points)>0):
            $quantity = count($points);
            $result = $p / $quantity;
        else:
            $result = 0;
        endif;

        return $result;
    }

}