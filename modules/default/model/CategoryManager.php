<?php
namespace defaultspace;

/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 15.01.2018
 * Time: 15:42
 */

class CategoryManager extends \BaseModel
{

    public function GetCategoryInfo(){
        $this->db->cmd->where("category_parent", "0");
        return $this->db->cmd->get("category");
    }

	public function GetCategory($id){
        $this->db->cmd->where("category_id", $id);
        return $this->db->cmd->getOne("category");
    }
	
    public function GetSubCategory($id){

        $this->db->cmd->where("category_parent", $id);
        return $this->db->cmd->get("category");
    }

    function GetSubAllCategory($id, $children = array()) {
        $r = $this->GetSubCategory($id);

        if(count($r) > 0) {
            # It has children, let's get them.
            foreach($r as $sub):
                $children[] = $sub['category_id'];
                $children = $this->GetSubAllCategory($sub['category_id'], $children);
            endforeach;
        }

        return $children;
    }

    public function GetAllCategoryId($parent = 0, $spacing = '', $user_tree_array = '') {

        if (!is_array($user_tree_array))
            $user_tree_array = array();

        $this->connection->where("category.category_parent", $parent);

        $this->connection->orderBy("category.category_id", "ASC");

        $result = $this->connection->get("category");


        foreach ($result as $row){

            if(intval($row["category_id"]) == 0){
                continue;
            }

            $user_tree_array[] = $row["category_id"];

            $user_tree_array = $this->GetAllCategoryId($row["category_id"], $spacing . '&nbsp;&nbsp;', $user_tree_array);
        }

        if($parent > 0) {

            $IdList = $this->GetRelatedCategoryIdList($parent);

            if(count($IdList)>0){
                $user_tree_array[] = $parent;
                foreach ($IdList as $ID){
                    $user_tree_array =  $this->GetAllCategoryId($ID, $spacing . '&nbsp;&nbsp;', $user_tree_array);
                }
            }
        }

        return $user_tree_array;

    }


    // Kategorideki İlanlar
    public function GetCategoryPublicationList($CatId = 0){

        $categoryId = $CatId == 0 ?  $_GET['q'] : $CatId;

        $allCat = $this->GetSubAllCategory($categoryId);

        $allCat = array_values($allCat);

        $allCat[] = $categoryId;

        $CatOptions = $this->GetCategoryOptionList($categoryId);
		$opt = "";
        foreach($CatOptions as $CatOption):
		
			$options = $this->GetOptions($CatOption['option_id']);
			
			foreach($options as $option):
                //numara == numara

                $values = array_values($_GET[$option['option_key_value']]);

				if( in_array($option['option_value'], $values)):
                    $opt[] = $option['option_detail_id'];
				endif;
			endforeach;
				
        endforeach;


        if($opt != ""){
            $this->db->cmd->where("publication_option.publication_option_detail_id", $opt, "IN");
        }

        if(isset($_GET['country'])){
            $this->db->cmd->where("publication.publication_from_country", $_GET['country']);
        }

        $this->db->cmd->where("publication.publication_category_id", $allCat, "IN");
        $this->db->cmd->groupBy("publication.publication_id");
        $this->db->cmd->join("publication_photo", "publication_photo.publication_id = publication.publication_id", "INNER");
		$this->db->cmd->join("publication_option", "publication_option.publication_option_publication_id = publication.publication_id", "LEFT");
        return $this->db->cmd->paginate("publication",1);

    }

    public function GetOptions($id)
    {
        $this->db->cmd->where("option.option_id", $id);
        $this->db->cmd->join("option_detail", "option_detail.option_id = option.option_id", "INNER");
        return $this->db->cmd->get("option");
    }

    public function GetCategoryOptionList($id)
    {
        $this->db->cmd->where("category_id", $id);
        return $this->db->cmd->get("category_option");
    }

    // Kategorideki ilanları filitrele
    public function GetFilterCategory(){

    }

    public function GetCategoryTree($category_id){

    }

	public function GetCategoryListToParent($categoryId){

		$CatAllList = array();

		$this->db->cmd->where("category_id", $categoryId);
		$CatInfo =$this->db->cmd->getOne("category");

		$CatAllList[] = $CatInfo;

		if($CatInfo["category_parent"] == 0){
			return $CatAllList;
		}

		$this->db->cmd->where("category_id", $CatInfo["category_parent"]);
		$CatInfo =$this->db->cmd->getOne("category");

		$CatAllList[] = $CatInfo;

		if($CatInfo["category_parent"] == 0){
			return $CatAllList;
		}

		$this->db->cmd->where("category_id", $CatInfo["category_parent"]);
		$CatInfo =$this->db->cmd->getOne("category");

		$CatAllList[] = $CatInfo;

		return $CatAllList;
	}

}