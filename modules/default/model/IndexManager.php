<?php
namespace defaultspace;

/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 09/08/2017
 * Time: 05:28
 */

class IndexManager extends \BaseModel
{

    public function Contact()
    {
        return $this->db->cmd->getOne("company");
    }

    public function Slider()
    {
        $this->db->cmd->where("slider_type", "b");
        return $this->db->cmd->get("slider");
    }

    public function BigSlider()
    {
        $this->db->cmd->where("slider_type", "C");
        return $this->db->cmd->get("slider");
    }

    public function Featured()
    {
        return $this->db->cmd->get("publication_featured");
    }

    public function Mail($mail, $title, $message)
    {
		$contact = $this->Contact();
        $HTML = <<<HTML
                    <table width="600px" border="0" align="center" style="background-color: #eee; padding: 15px;">
                        <tr>
                            <td>
                                <table align="center" border="0" style="padding:10px;">
                                    <tr><td><img src="http://buygosell.webmobilyazilim.com/images/logo.png"></td></tr>
                                </table></td>
                        </tr>
                        <tr>
                            <td>
								<table style="padding:30px 10px; width:580px; border-top:1px solid #ccc;">
									<tr>
										<td><h3 style="">{$title}</h3></td>
									</tr>
									<tr>
										<td>{$message}</td>
									</tr>
								</table>
							</td>
                        </tr>
						<tr>
							<td>
								<table style="padding:15px 10px; width:580px; border-top:1px solid #ccc;">
									<tr style="padding:10px">
										<td>{$contact['company_name']}</td>
										<td>{$contact['company_telephone']}</td>
										<td>{$contact['company_fax']}</td>
										<td>{$contact['company_mail']}</td>
									</tr>
								</table>
							</td>
						</tr>
                    </table>                               
HTML;


        return \Globalf::SendMail($mail, $title, $HTML);
    }

    public function Add($param = array())
    {
        // TODO: Implement Add() method.
    }

    public function Get($id)
    {
        // TODO: Implement Get() method.
    }

    public function Delete($id)
    {
        // TODO: Implement Delete() method.
    }

    public function Edit($id)
    {
        // TODO: Implement Edit() method.
    }

    public function Update()
    {
        // TODO: Implement Update() method.
    }

    public function Check($field, $value)
    {
        // TODO: Implement Check() method.
    }

    public function GetList()
    {
        // TODO: Implement GetList() method.
    }
}