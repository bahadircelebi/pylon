<?php
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 09/08/2017
 * Time: 05:27
 */
namespace defaultspace;

Class ProductController extends \BaseModel implements \FrontController {


    public function Index($param = null)
    {
        $data = array();

        $BasketManager = new \defaultspace\BasketManager();
        $UserManager = new \defaultspace\UserManager();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();


        \Application::RenderLayout("default", "product/product", "default", $data);
    }

    public function Detail($id)
    {

        $data = array();
        global $Config;

        $UserManager = new \defaultspace\UserManager();
        $PublicationManager = new \defaultspace\PublicationManager();

        $data['publication'] = $PublicationManager->GetPublicationDetail($id);
        $data['publicationOption'] = $PublicationManager->GetPublicationOption($id);
        $data['publication_photo'] = $PublicationManager->GetPublicationPhoto($id);
        $data['from_country'] = $UserManager->GetCountrySelect($data['publication']['publication_from_country']);
        $data['to_country'] = $UserManager->GetCountrySelect($data['publication']['publication_to_country']);
        $data['publication_point'] = $PublicationManager->GetPublicationPoint($id);
        $data['pid'] = $id;

        $data['og_data']['url'] = $Config['system']['base_url']."product/detail/".$data['publication']['publication_id'];
        $data['og_data']['title'] = $data['publication']['publication_title'];
        $data['og_data']['description'] = $data['publication']['publication_description'];
        $data['og_data']['image'] = $Config['system']['base_url']."images/".$data['publication_photo']['publication_photo_path']."-150.".$data['publication_photo']['publication_photo_extension'];

        \Application::RenderLayout("default", "product/product.detail", "default", $data);

    }

    public function Confirm($id)
    {
        $data = array();

        $UserManager = new \defaultspace\UserManager();
        $PublicationManager = new \defaultspace\PublicationManager();

        $PublicationManager->PublicationConfirm($id);
        $data['publication'] = $PublicationManager->GetPublicationDetail($id);
        $data['publicationOption'] = $PublicationManager->GetPublicationOption($id);
        $data['publication_photo'] = $PublicationManager->GetPublicationPhoto($id);
        $data['from_country'] = $UserManager->GetCountrySelect($data['publication']['publication_from_country']);
        $data['to_country'] = $UserManager->GetCountrySelect($data['publication']['publication_to_country']);

        \Application::RenderLayout("default", "product/product.detail", "default", $data);
    }
	
	public function Seller()
	{
        $data = array();
        $UserManager = new \defaultspace\UserManager();
        $BasketManager = new \defaultspace\BasketManager();
		$PublicationManager = new \defaultspace\PublicationManager();
		$data["publicationList"] = $PublicationManager->GetLastSalePublicationList();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();
        $data['country'] = $UserManager->GetCountry();

		\Application::RenderLayout("default", "product/product.seller", "default", $data);
	}
	
	public function Buyer()
	{
        $data = array();
        $UserManager = new \defaultspace\UserManager();
        $BasketManager = new \defaultspace\BasketManager();
		$PublicationManager = new \defaultspace\PublicationManager();
		$data["publicationList"] = $PublicationManager->GetLastSalePublicationList();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();
        $data['country'] = $UserManager->GetCountry();

		\Application::RenderLayout("default", "product/product.buyer", "default", $data);
	}

    public function Upload()
    {
        $data = array();
        $UserManager = new \defaultspace\UserManager();
		$CategoryManager = new \defaultspace\CategoryManager();
        $BasketManager = new \defaultspace\BasketManager();

        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();
        $data['user'] = $UserManager->GetUserInfo();
        $data['sell'] = $UserManager->GetSellOrder();
		$data['category'] = $CategoryManager->GetCategoryInfo();
		$data['myCountry'] = $UserManager->GetCountrySelect($data['user']['user_country']);
		$data['from_country'] = $UserManager->GetCountrySelect($_POST['from_country']);
		$data['to_country'] = $UserManager->GetCountrySelect($_POST['to_country']);
        $data['date'] = $_POST['date'];

        \Application::RenderLayout("default", "product/upload", "default", $data);
    }

    public function SubCategory()
    {
        $data = array();
        $CategoryManager = new \defaultspace\CategoryManager();
        $data['subCategory'] = $CategoryManager->GetSubCategory($_POST['id']);
        $data['category'] = $CategoryManager->GetCategory($_POST['id']);

        \Application::RenderView("product/product.sub.category", "default", $data);
    }

    public function Option()
    {
        $data = array();
        $CategoryManager = new \defaultspace\CategoryManager();
        $data['categoryOption'] = $CategoryManager->GetCategoryOptionList($_POST['id']);

        \Application::RenderView("product/product.option", "default", $data);
    }

    public function SendUpload()
    {
        $data = array();
        $PublicationManager = new \defaultspace\PublicationManager();
        $id = $PublicationManager->AddPublication();
        $data['publication'] = $PublicationManager->GetPublication($id);

        \Application::RenderLayout("default", "product/upload.success", "default", $data);
    }

    public function UpdatePublication($id)
    {
        $data = array();
        $PublicationManager = new \defaultspace\PublicationManager();
        $PublicationManager->UpdatePublication($id);
    }

    public function AddPublicationPhoto()
    {
        $PublicationManager = new \defaultspace\PublicationManager();
        $PublicationManager->AddPublicationPhoto();
    }

    public function DeletePublicationPhoto()
    {
        $PublicationManager = new \defaultspace\PublicationManager();
        echo $PublicationManager->DeletePublicationPhoto();
    }

    public function AddFavorite()
    {
        $data = array();
        $PublicationManager = new \defaultspace\PublicationManager();
		$PublicationManager->AddFavorite();
		
    }

    public function AutoSearch()
    {
        $PublicationManager = new \defaultspace\PublicationManager();
        echo $PublicationManager->AutoSearch();
    }

    public function Search()
    {
        $data = array();
        $UserManager = new \defaultspace\UserManager();
        $CategoryManager = new \defaultspace\CategoryManager();
        $PublicationManager = new \defaultspace\PublicationManager();

        $data['country'] = $UserManager->GetCountry();
        $data['search'] = $PublicationManager->PublicationSearch();
        $data['categoryOption'] = $CategoryManager->GetCategoryOptionList($_GET['q']);
        $data["publicationList"] = $PublicationManager->GetPopulerPublicationList();

        \Application::RenderLayout("default", "product/search", "default", $data);
    }
	
	public function BestPublication()
	{
		$data = array();
        $PublicationManager = new \defaultspace\PublicationManager();
		$data['publication'] = $PublicationManager->GetBestPublication();
        $data["publicationList"] = $PublicationManager->GetPopulerPublicationList();
		
		\Application::RenderLayout("default", "best", "default", $data);
	}

    public function PublicationPoint()
    {
        $PublicationManager = new \defaultspace\PublicationManager();
        echo $PublicationManager->PublicationPoint();

    }

    public function NotFound()
    {
        // TODO: Implement NotFound() method.
    }

}