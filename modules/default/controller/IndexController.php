<?php
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 09/08/2017
 * Time: 05:27
 */
namespace defaultspace;

Class IndexController extends \BaseModel implements \FrontController {


    public function Index($param = null)
    {
        $PublicationManager = new \defaultspace\PublicationManager();
		$CategoryManager = new \defaultspace\CategoryManager();
        $BasketManager = new \defaultspace\BasketManager();
        $IndexManager = new \defaultspace\IndexManager();

        $data['slider'] = $IndexManager->Slider();
        $data['bigslider'] = $IndexManager->BigSlider();
        $data['company'] = $IndexManager->Contact();
        $data['featured'] = $IndexManager->Featured();
		$data['category'] = $CategoryManager->GetCategoryInfo();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();
        $data["publicationList"] = $PublicationManager->GetPopulerPublicationList();
        $data["newPublicationList"] = $PublicationManager->GetNewPublicationList();

        \Application::RenderLayout("default", "index", $param["module"], $data);
    }

    public function Category()
    {
        $data = array();
        $BasketManager = new \defaultspace\BasketManager();
		
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();

        \Application::RenderLayout("default", "category", "default", $data);
    }

    public function Profile()
    {
		$data = array();
        $BasketManager = new \defaultspace\BasketManager();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();

        \Application::RenderLayout("default", "profile", "default", $data);
    }
	
	public function Navbar()
    {
        $data = array();
		
        \Application::RenderLayout("default", "global/navbar", "default", $data);
    }

    public function About()
    {
        $data = array();
        $BasketManager = new \defaultspace\BasketManager();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();
		
		$IndexManager = new \defaultspace\IndexManager();
        $data['company'] = $IndexManager->Contact();

        \Application::RenderLayout("default", "about", "default", $data);
    }

    public function Contact()
    {
        $data = array();
        $BasketManager = new \defaultspace\BasketManager();
        $IndexManager = new \defaultspace\IndexManager();
        $data['company'] = $IndexManager->Contact();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();

        \Application::RenderLayout("default", "contact", "default", $data);
    }

    public function Delivery()
    {
        $data = array();

        \Application::RenderLayout("default", "delivery", "default", $data);
    }

    public function LegalWarning()
    {
        $data = array();

        \Application::RenderLayout("default", "legal.warning", "default", $data);
    }

    public function EmptyPage()
    {
        $data = array();

        \Application::RenderLayout("default", "empty", "default", $data);
    }

    public function Test($param = null)
    {
       die("prm: ".$param->name);
    }

    public function NotFound()
    {
        // TODO: Implement NotFound() method.
    }

}