<?php
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 09/08/2017
 * Time: 05:27
 */
namespace defaultspace;

Class BasketController extends \BaseModel implements \FrontController {


    public function Index($param = null)
    {

    }

    public function Step1($param = null)
    {

        $data = array();

        $BasketManager = new \defaultspace\BasketManager();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();
        $data['publication_list'] = $BasketManager->GetBasketPublictionList();

        \Application::RenderLayout("default", "basket/step1", "default", $data);

    }

    public function Step2($param = null)
    {

        $data = array();
		    $UserManager = new \defaultspace\UserManager();
        $BasketManager = new \defaultspace\BasketManager();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();
        $data['publication_list'] = $BasketManager->GetBasketPublictionList();
	    	$data['address'] = $UserManager->GetMyAddress();
        $data['country'] = $UserManager->GetCountry();

        \Application::RenderLayout("default", "basket/step2", "default", $data);

    }

    public function Step3($id)
    {

        $data = array();
        $BasketManager = new \defaultspace\BasketManager();

        $data['order'] = $BasketManager->GetCompleteOrder($id);
        $data['order_detail'] = $BasketManager->GetCompleteOrderDetail($id);
        $data['delivery_address'] = $BasketManager->GetAddressId($data['order']['order_address_id']);
        $data['invoice_address'] = $BasketManager->GetAddressId($data['order']['order_invoice_address_id']);

        \Application::RenderLayout("default", "basket/step3", "default", $data);

    }

    public function SaveOrder()
    {
        $OrderManager = new \defaultspace\OrderManager();
        $OrderManager->SaveOrder();
    }

    public function AddToBasket($param = null)
    {
        $data = array();
        $BasketManager = new \defaultspace\BasketManager();
        $data['msg'] = $basket = $BasketManager->AddToBasket();
		
		echo $data['msg'];

    }

    public function ChangeQuantity()
    {
        $BasketManager = new \defaultspace\BasketManager();
        $basket = $BasketManager->ChangeQuantity();
    }

    public function DeleteFromBasket($param = null)
    {
        $data = array();
        $BasketManager = new \defaultspace\BasketManager();
        echo $data['msg'] = $basket = $BasketManager->DeleteFromBasket();

    }

    public function BasketEmpty($param = null)
    {
        $data = array();
        $BasketManager = new \defaultspace\BasketManager();
        $basket = $BasketManager->BasketEmpty();

    }

    public function TotalProduct()
    {
        $data = array();
        $BasketManager = new \defaultspace\BasketManager();
        echo $BasketManager->GetBasketTotalPubliction();

        //\Application::RenderLayout("default", "login", "default", $data);
    }
	
	public function AddAddress()
    {
        $data = array();
        $BasketManager = new \defaultspace\BasketManager();
        echo $data['msg'] = $BasketManager->Address();

        //\Application::RenderLayout("default", "login", "default", $data);
    }

    public function NotFound()
    {
        // TODO: Implement NotFound() method.
    }

}