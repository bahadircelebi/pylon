<?php
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 09/08/2017
 * Time: 05:27
 */
namespace defaultspace;

Class AjaxController extends \BaseModel implements \FrontController {


    public function __construct()
    {
        if(!isset($_SESSION["csr_protection"]) || !isset($_POST["csr_token"])){
            die("Bu işleme izin verilmedi");
        }else{
            if($_SESSION["csr_protection"] != $_POST["csr_token"]){
                die("Bu işleme izin verilmedi");
            }
        }
    }

    public function Index($param = null)
    {
    }


    public function AddToBasket($param = null){

    }

    public function DeleteFromBasket($param = null){

    }

    public function RateUser($param = null){

    }


    public function NotFound()
    {
        // TODO: Implement NotFound() method.
    }

}