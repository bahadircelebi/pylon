<?php
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 09/08/2017
 * Time: 05:27
 */
namespace defaultspace;

Class CategoryController extends \BaseModel implements \FrontController {


    public function Index($param = null)
    {
        $UserManager = new \defaultspace\UserManager();
        $CategoryManager = new \defaultspace\CategoryManager();
		$PublicationManager = new \defaultspace\PublicationManager();

        $categoryId = $_GET["q"];
		
		if(isset($categoryId)):
			$data["breadLink"] = $CategoryManager->GetCategoryListToParent($categoryId);
		    asort($data["breadLink"] );
			$data['categoryInfo'] = $CategoryManager->GetCategory($categoryId);		
			$data['category'] = $CategoryManager->GetCategoryPublicationList();
            $data['subCategory'] = $CategoryManager->GetSubCategory($categoryId);
			$data['categoryOption'] = $CategoryManager->GetCategoryOptionList($categoryId);
		else:
			$data['category'] = $PublicationManager->GetHomePagePublicationList();
		endif;
            $data['country'] = $UserManager->GetCountry();
			$data['categoryMain'] = $CategoryManager->GetCategoryInfo();
			$data["publicationList"] = $PublicationManager->GetPopulerPublicationList();
		


        \Application::RenderLayout("default", "category", "default", $data);
    }

    public function NotFound()
    {
        // TODO: Implement NotFound() method.
    }

}