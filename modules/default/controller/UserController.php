<?php
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 09/08/2017
 * Time: 05:27
 */
namespace defaultspace;

Class UserController extends \BaseModel implements \FrontController {


    public function Index($param = null)
    {

    }

    public function LoginForm($param = null)
    {
        $data = array();
        $BasketManager = new \defaultspace\BasketManager();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();

        \Application::RenderLayout("default", "login", "default", $data);
    }

    public function LoginAction($param = null)
    {

        if(isset($_POST["user_mail"]) && isset($_POST["user_pass"])){

            $UserMngr = new \defaultspace\UserManager();

            $login = $UserMngr->Login($_POST["user_mail"], $_POST["user_pass"]);

            if($login == "OK"){
                \Globalf::redirect("profile");
            }else{

                $data["msg"] = $login;

                \Application::RenderLayout("default", "login", "default", $data);

            }
        }
    }

    public function Logout(){
        session_destroy();
        \Globalf::redirect("user/login");
    }
	
	public function Recovery()
	{
		$data = array();
        $BasketManager = new \defaultspace\BasketManager();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();

        \Application::RenderLayout("default", "profile/profile.recovery", "default", $data);
	}
	
	public function RecoverySend(){

        $UserManager = new \defaultspace\UserManager();
        $pass = $UserManager->SendPassword();
        if($pass == "OK"):
            $data["msg"] = "Şifre sıfırlama linki mail olarak gönderildi.";
        endif;

        \Application::RenderLayout("default", "profile/profile.recovery", "default", $data);

    }

    public function RecoveryChange(){

        $data = array();
        $UserManager = new \defaultspace\UserManager();
        $msg = $UserManager->RecoveryChange();
        $data['m'] = $msg;
        $data["msg"] = $this->caption["messages"][$msg];

        \Application::RenderLayout("default", "profile/profile.recovery.change", "default", $data);

    }

    public function RegisterForm($param = null)
    {

        $UserManager = new \defaultspace\UserManager();
        $BasketManager = new \defaultspace\BasketManager();

        $data = array();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();
        $data['country'] = $UserManager->GetCountry();

        \Application::RenderLayout("default", "register", "default", $data);
    }

    public function RegisterAction($param = null)
    {
        $data = array();

        if(isset($_POST["user_mail"]) && isset($_POST["user_pass"])){

            $UserMngr = new \defaultspace\UserManager();
            $data['country'] = $UserMngr->GetCountry();
            $status = $UserMngr->Register($_POST["user_first_name"],$_POST["user_last_name"],$_POST["user_mail"],$_POST["user_pass"],$_POST["user_repass"],$_POST["user_country"],$_POST["state"],$_POST["city"]);

            if($status == "OK"){
                //\Globalf::redirect("user/login");
                $data['registerMsg'] = "OK";
            }else{

                $data["msg"] = $status;
            }

            \Application::RenderLayout("default", "register", "default", $data);
        }
    }

    public function AccountActivate()
    {
        $data = array();
        $UserManager = new \defaultspace\UserManager();
        $data['m'] = $UserManager->AccountActivate();

        \Application::RenderLayout("default", "profile/profile.activate", "default", $data);
    }

    public function GetState()
    {
        $data = array();

        $UserManager = new \defaultspace\UserManager();

        $data['state'] = $UserManager->GetState($_POST['id']);

        \Application::RenderView("global/get.state", "default", $data);
    }

    public function GetCity()
    {
        $data = array();

        $UserManager = new \defaultspace\UserManager();

        $data['cities'] = $UserManager->GetCity($_POST['id']);

        \Application::RenderView("global/get.city", "default", $data);
    }

    public function MyProfile($param = null)
    {

        $data = array();

        $UserManager = new \defaultspace\UserManager();
        $BasketManager = new \defaultspace\BasketManager();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();
        $data['user'] = $UserManager->GetUserInfo();
        $data['country'] = $UserManager->GetCountry();
        $data['state'] = $UserManager->GetState($data['user']['user_country']);
        $data['city'] = $UserManager->GetCity($data['user']['user_state']);

        \Application::RenderLayout("profile", "profile/profile", "default", $data);

    }

    public function MyProfileAction($param = null)
    {
        $data = array();

        $UserManager = new \defaultspace\UserManager();
        $BasketManager = new \defaultspace\BasketManager();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();
        $data['msg'] = $UserManager->UpdateProfile();

        $data['user'] = $UserManager->GetUserInfo();
        $data['country'] = $UserManager->GetCountry();
        $data['state'] = $UserManager->GetState($data['user']['user_country']);
        $data['city'] = $UserManager->GetCity($data['user']['user_state']);

        \Application::RenderLayout("profile", "profile/profile", "default", $data);
    }
	
	public function PasswordChangeForm()
	{
		$data = array();
		$UserManager = new \defaultspace\UserManager();
		
		$data['user'] = $UserManager->GetUserInfo();
		
		\Application::RenderLayout("profile", "profile/profile.change.password", "default", $data);
	}

    public function SavePasswordChange()
    {
        $data = array();
        $UserManager = new \defaultspace\UserManager();
        $BasketManager = new \defaultspace\BasketManager();
		
		$data['user'] = $UserManager->GetUserInfo();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();
        $data['msg'] = $UserManager->ChangePassword();

        \Application::RenderLayout("profile", "profile/profile.change.password", "default", $data);
    }

    public function BankForm()
    {
        $data = array();
        $UserManager = new \defaultspace\UserManager();
        $BasketManager = new \defaultspace\BasketManager();
		
        $data['bank'] = $UserManager->GetBankInfo();
		$data['user'] = $UserManager->GetUserInfo();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();

        \Application::RenderLayout("profile", "profile/profile.bank", "default", $data);
    }

    public function BankFormAction()
    {
        $data = array();
        $UserManager = new \defaultspace\UserManager();
        $BasketManager = new \defaultspace\BasketManager();

        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();
        $data['msg'] = $UserManager->SaveBank();
        $data['user'] = $UserManager->GetUserInfo();
        $data['bank'] = $UserManager->GetBankInfo();

        \Application::RenderLayout("profile", "profile/profile.bank", "default", $data);
    }

    public function BankDeleteAction()
    {
        $data = array();
        $UserManager = new \defaultspace\UserManager();
        $BasketManager = new \defaultspace\BasketManager();

        $data['user'] = $UserManager->GetUserInfo();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();
        echo $UserManager->DeleteBank();
    }

    public function AddressForm()
    {
        $data = array();
		$UserManager = new \defaultspace\UserManager();
        $BasketManager = new \defaultspace\BasketManager();
		
		$data['user'] = $UserManager->GetUserInfo();
        $data['country'] = $UserManager->GetCountry();
		$data['address'] = $UserManager->GetMyAddress();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();

        \Application::RenderLayout("profile", "profile/profile.address", "default", $data);
    }

    public function AddressFormAction()
    {
        $data = array();
        $UserManager = new \defaultspace\UserManager();
        $BasketManager = new \defaultspace\BasketManager();
		
		$data['user'] = $UserManager->GetUserInfo();
        $data['totalProduct'] = $BasketManager->GetBasketTotalPubliction();
        $data['msg'] = $UserManager->SaveAddress();
        $data['address'] = $UserManager->GetMyAddress();

        \Application::RenderLayout("profile", "profile/profile.address", "default", $data);
    }
	
	public function GetFavorite()
	{
		$data = array();
        $UserManager = new \defaultspace\UserManager();
		$data['favorite'] = $UserManager->GetFavorite();
		
		\Application::RenderLayout("profile", "profile/profile.favorite", "default", $data);
	}

    public function DeleteFavorite()
    {
        $data = array();
        $UserManager = new \defaultspace\UserManager();
        $data['msg'] = $UserManager->DeleteFavorite();
        $data['favorite'] = $UserManager->GetFavorite();

        \Application::RenderLayout("profile", "profile/profile.favorite", "default", $data);
    }

    public function GetBuy()
    {

        $data = array();
        $UserManager = new \defaultspace\UserManager();
        $OrderManager = new \defaultspace\OrderManager();

        if(isset($_POST['oid'])):
            $OrderManager->UpdateOrderStatus("F");
        endif;
        $data['buy'] = $UserManager->GetBuy();

        \Application::RenderLayout("profile", "profile/profile.buy", "default", $data);

    }

    public function GetSell()
    {
        $data = array();
        $UserManager = new \defaultspace\UserManager();
        $OrderManager = new \defaultspace\OrderManager();

        if(isset($_POST['cargo_no'])):
            $OrderManager->UpdateOrderCargoKey();
        endif;
        $data['sell'] = $UserManager->GetSell();

        \Application::RenderLayout("profile", "profile/profile.sell", "default", $data);
    }

    public function UserViewColumn()
    {

        if(isset($_POST['list_column'])):
            $_SESSION["view_column"] = $_POST['list_column'];
        endif;

        if(isset($_POST['box_column'])):
            $_SESSION["view_column"] = $_POST['box_column'];
        endif;

        echo "OK";

    }

    public function GetMessage()
    {
        $data = array();
        $UserManager = new \defaultspace\UserManager();
        $data['message'] = $UserManager->GetMessage();

        \Application::RenderLayout("profile", "profile/profile.message", "default", $data);
    }

    public function GetMessageDetail($id)
    {
        $data = array();
        $UserManager = new \defaultspace\UserManager();
        $data['messages'] = $UserManager->GetMessageDetail($id);
		$data['detail_id'] = $id;

        \Application::RenderLayout("profile", "profile/profile.message.detail", "default", $data);
    }

    public function AddMessage($id)
    {
        $data = array();

        $UserManager = new \defaultspace\UserManager();
        $PublicationManager = new \defaultspace\PublicationManager();

        $data['msg'] = $UserManager->AddMessage();

        $data['publication'] = $PublicationManager->GetPublicationDetail($id);
        $data['publicationOption'] = $PublicationManager->GetPublicationOption($id);
        $data['publication_photo'] = $PublicationManager->GetPublicationPhoto($id);
        $data['from_country'] = $UserManager->GetCountrySelect($data['publication']['publication_from_country']);
        $data['to_country'] = $UserManager->GetCountrySelect($data['publication']['publication_to_country']);
        $data['pid'] = $id;

        \Application::RenderLayout("default", "product/product.detail", "default", $data);
    }
	
	public function AddMessages($id)
    {

		$data = array();
        $UserManager = new \defaultspace\UserManager();
		$UserManager->AddMessages($id);
        $data['messages'] = $UserManager->GetMessageDetail($id);
		$data['detail_id'] = $id;
		
		\Application::RenderLayout("profile", "profile/profile.message.detail", "default", $data);

    }

    public function AddComplaint($id)
    {

        $data = array();

        $UserManager = new \defaultspace\UserManager();
        $PublicationManager = new \defaultspace\PublicationManager();

        $data['msg'] = $UserManager->AddComplaint();

        $data['publication'] = $PublicationManager->GetPublicationDetail($id);
        $data['publicationOption'] = $PublicationManager->GetPublicationOption($id);
        $data['publication_photo'] = $PublicationManager->GetPublicationPhoto($id);
        $data['from_country'] = $UserManager->GetCountrySelect($data['publication']['publication_from_country']);
        $data['to_country'] = $UserManager->GetCountrySelect($data['publication']['publication_to_country']);
        $data['pid'] = $id;

        \Application::RenderLayout("default", "product/product.detail", "default", $data);

    }

    public function GetManage()
    {
        $data = array();
        $PublicationManager = new \defaultspace\PublicationManager();
        $data['pendingPublication'] = $PublicationManager->GetSellerPendingPublications();
        $data['activePublication'] = $PublicationManager->GetSellerActivePublications();

        \Application::RenderLayout("profile", "profile/profile.manage", "default", $data);
    }

    public function GetManageDetail($id)
    {
        $data = array();

        $UserManager = new \defaultspace\UserManager();
        $CategoryManager = new \defaultspace\CategoryManager();
        $PublicationManager = new \defaultspace\PublicationManager();

        $data['publication'] = $PublicationManager->GetPublicationDetail($id);
        $data['publicationOption'] = $PublicationManager->GetPublicationOption($id);
        $data['publication_photo'] = $PublicationManager->GetPublicationPhoto($id);
        $data['category'] = $CategoryManager->GetCategoryInfo();
        $data['country'] = $UserManager->GetCountry();
        $data['from_country'] = $UserManager->GetCountrySelect($data['publication']['publication_from_country']);
        $data['to_country'] = $UserManager->GetCountrySelect($data['publication']['publication_to_country']);
        $data['publication_point'] = $PublicationManager->GetPublicationPoint($id);
        $data['pid'] = $id;

        \Application::RenderLayout("profile", "profile/profile.manage.detail", "default", $data);
    }

    public function OrderPoint($id)
    {
        $PublicationManager = new \defaultspace\PublicationManager();
        echo $PublicationManager->OrderPoint();

    }

    public function Preview($param = null)
    {
        \Application::RenderLayout("default", "preview", $param["module"]);
    }

    public function MyBasket($param = null)
    {
        \Application::RenderLayout("default", "basket", $param["module"]);
    }


    public function NotFound()
    {
        // TODO: Implement NotFound() method.
    }

}